const jwt = require('jwt-simple');
const moment = require('moment');
const secret = 'knex';

function ensureAuth(req, res, next) {
    if(!req.headers.authorization) {
        return res.status(401).json({
            message: 'Token not found'
        });
    }
    const token = req.headers.authorization.split(" ")[1];
    const payload = jwt.decode(token, secret);
    try {    
        if(payload.exp <= moment().unix()){
            return res.status(401).json({
                message: 'Token has expired'
            });
        }
    } catch (ex) {
        return res.status(401).json({
            message: 'Invalid token'
        });
    }
    req.user = payload;
    next();
};

module.exports = {
    ensureAuth,
}
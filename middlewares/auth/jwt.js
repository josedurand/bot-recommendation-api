const jwt = require('jwt-simple');
const moment = require('moment');
const secret = 'knex';

exports.createToken = function(user){
    const payload = {
        id: user.id,
        name: user.name,
        lastName: user.last_name,
        gender: user.gender,
        age: user.age, 
        dni: user.dni,
        iat: moment().unix(),
        exp: moment().add(1,'M').unix
    };
    return jwt.encode(payload, secret);
};

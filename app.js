const express = require('express');
const app = express();
const path = require('path');

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/initial/index.html');
});

app.use('/public', express.static(path.join(__dirname, 'public')));

require('./config/db')(app);
require('./config/express')(app);
require('./config/routes')(app);

module.exports = app;

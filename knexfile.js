require('dotenv').config();

const options = {
  client: process.env.DB_CLIENT,
  connection: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    charset: process.env.DB_CHARSET,
  },
  debug: process.env.DB_DEBUG_MODE,
  pool: {
    afterCreate: (connection, cb) => {
      cb(null, connection);
    },
  },
  migrations: {
    directory: `${__dirname}/db/migrations`,
  },
  seeds: {
    directory: `${__dirname}/db/seeds`,
  },
};

module.exports = options;

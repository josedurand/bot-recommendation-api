# Installation

## global
npm install -g knex
npm install apidoc -g

# Database

## Migration
knex migrate:make 001 
knex migrate:latest 

## Seeder
knex seed:make 001 
knex seed:run 

# Documentation
apidoc -i controllers -o public/apidoc/ 
- **[View Documentation](http://localhost:4000/public/apidoc/index.html )**

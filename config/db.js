const Knex = require('knex');
const Knexfile= require('../knexfile');
const setupPaginator = require('knex-paginator');
setupPaginator(Knex);

module.exports = (app) => {
  const db = Knex(Knexfile);

  app.db = db;
};


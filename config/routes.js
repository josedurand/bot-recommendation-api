const ApiRoutes = require('../expose/routes');

module.exports = (app) => {
  app.use('/', ApiRoutes);
};

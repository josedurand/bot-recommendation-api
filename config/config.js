const config = {
  app: {
    name: 'Chatbot',
    authSecretKey: 'Chatbot',
  },
  appSettings: {
    publicIp: 'http://0.0.0.0:4000',
    timeZone: 'America/Lima',
  },
  appHost: '0.0.0.0',
  appPort: '4003',
  env: 'development'
};

module.exports = config;

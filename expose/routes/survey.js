const express = require('express');
const surveyResultController = require('../controllers/survey');
const middleware = require('../../middlewares/auth/authentication');

const Router = express.Router();

Router.get('/:userId', middleware.ensureAuth, surveyResultController.getSurveyResult);
Router.post('/', middleware.ensureAuth, surveyResultController.createSurveyResult);

module.exports = Router;
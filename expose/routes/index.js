const express = require('express');
const recommendationRoutes = require('../routes/recommendation');
const messageRoutes = require('../routes/message');
const userRoutes = require('../routes/user');
const authRoutes = require('../routes/auth');
const surveyRoutes = require('../routes/survey');


const Router = express.Router();

Router.use('/recommendation', userRoutes);
Router.use('/message', messageRoutes);
Router.use('/user', userRoutes);
Router.use('/auth', authRoutes);
Router.use('/survey', surveyRoutes);


module.exports = Router;

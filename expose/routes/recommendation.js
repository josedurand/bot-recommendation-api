const express = require('express');
const recommendationController = require('../controllers/recommendation');

const Router = express.Router();

Router.post('/', recommendationController.createRecommendation);
Router.put('/:id', recommendationController.updateRecommendation);
Router.delete('/:id', recommendationController.deleteRecommendation);
Router.get('/type', recommendationController.getByTypeRecommendation);
Router.get('/', recommendationController.getAllRecommendation);



module.exports = Router;
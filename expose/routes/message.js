const express = require('express');
const messageController = require('../controllers/message');
const middleware = require('../../middlewares/auth/authentication');

const Router = express.Router();

Router.post('/', middleware.ensureAuth, messageController.getTextAndSendMessage);

module.exports = Router;
const express = require('express');
const userController = require('../controllers/user');
const middleware = require('../../middlewares/auth/authentication');

const Router = express.Router();

Router.post('/', userController.createUser);
Router.put('/:dni', middleware.ensureAuth, userController.updateUser);
Router.get('/:dni', middleware.ensureAuth, userController.getUser);

module.exports = Router;
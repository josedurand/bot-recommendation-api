const express = require('express');
const authController = require('../controllers/auth');

const Router = express.Router();

Router.post('/', authController.login);
Router.get('/department', authController.getDepartment);
Router.get('/province/:departmentId', authController.getProvince);
Router.get('/district/:provinceId', authController.getDistrict);


module.exports = Router;
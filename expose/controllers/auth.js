const authRepository = require('../../business/repository/auth');

const login = async(req, res) => {
    return await authRepository.login(req, res);
}

const getDepartment = async(req, res) => {
    return await authRepository.getDepartment(req, res);
}

const getProvince = async(req, res) => {
    return await authRepository.getProvince(req, res);
}

const getDistrict = async(req, res) => {
    return await authRepository.getDistrict(req, res);
}


module.exports = {
    login,
    getDepartment,
    getProvince,
    getDistrict,
};
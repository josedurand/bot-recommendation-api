const recommendationRepository = require('../../business/repository/recommendation');


const createRecommendation = async (req, res) => {
  return await recommendationRepository.create(req, res);
}

const updateRecommendation = async(req, res) => {
  return await recommendationRepository.update(req, res);
}

const deleteRecommendation = async(req, res) => {
  return await recommendationRepository.destroy(req, res);
}

const getByTypeRecommendation = async(req, res) => {
  return await recommendationRepository.getByType(req, res);
}

const getAllRecommendation = async(req, res) => {
  return await recommendationRepository.get(req, res);
}


module.exports = {
  createRecommendation,
  updateRecommendation,
  deleteRecommendation,
  getByTypeRecommendation,
  getAllRecommendation,
};

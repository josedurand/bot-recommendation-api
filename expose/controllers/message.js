const { handleError } = require('../../utils/helpers/expressHelper');
const ibmWatsonService = require('../../business/service/ibmwatson');

async function getTextAndSendMessage(req, res) {
    const { messageInput } = req.body;

    try {
        const ibmResponse = await ibmWatsonService.sendToIbmWatson(messageInput);
        
        if (!ibmResponse) {
            return res.status(500).json({ message: 'Error al conectarse a ibm watson'});
        }
        
       return res.json(ibmResponse.result.output.text)
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

module.exports = {
    getTextAndSendMessage,
};
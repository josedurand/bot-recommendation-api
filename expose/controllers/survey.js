const surveyResult = require('../../business/repository/survey');

const createSurveyResult = async (req, res) => {
    return await surveyResult.create(req, res);
}

const getSurveyResult = async (req, res) => {
    return await surveyResult.get(req, res);
}

module.exports = {
    createSurveyResult,
    getSurveyResult,
};
const userRepository = require('../../business/repository/user');

/*async function all(page) {
    var configuration = await knex('configuration').select('*').paginate(10, page, true);
    return configuration;
}
} */

const createUser = async (req, res) => {
    return await userRepository.create(req, res);
}


const updateUser = async (req, res) => {
    return await userRepository.update(req, res);
}

const getUser = async (req, res) => {
    return await userRepository.get(req, res);
}
  
module.exports = {
    createUser,
    updateUser,
    getUser,
};
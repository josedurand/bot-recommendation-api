function surveyResult(table) {
  table.increments('id').primary();
  table.integer('userId').notNullable();
  table.text('content').notNullable();
  table.datetime('proccessDate').notNullable();
}

function recommendation(table) {
  table.increments('id').primary();
  table.enu('type', ['low', 'mid', 'high']).notNullable();
  table.string('description', 100).notNullable();
}

function department(table) {
  table.string('id').unique().primary();
  table.string('name').notNullable();
}

function province(table) {
  table.string('id').unique().primary();
  table.string('name').notNullable();
  table.string('departmentId').notNullable();
  table.foreign('departmentId').references('id').inTable('department');
}

function district(table) {
  table.string('id').unique().primary();
  table.string('name').notNullable();
  table.string('provinceId').notNullable();
  table.foreign('provinceId').references('id').inTable('province');
}

function user(table) {
  table.increments('id').primary();
  table.string('name', 100).notNullable();
  table.string('lastName', 100).notNullable();
  table.string('dni', 8).unique().notNullable();
  table.enu('gender', ['M', 'F']).notNullable();
  table.integer('age').notNullable();
  table.string('districtId').notNullable();
  table.foreign('districtId').references('id').inTable('district');
}

function administrator(table) {
  table.increments('id').primary();
  table.string('name', 100).notNullable();
  table.string('lastName', 100).notNullable();
  table.string('email').unique().notNullable();
  table.string('password', 50).notNullable();
}
  exports.up = async (knex) => {
    await Promise.all([
      knex.schema.createTable('administrator', administrator),
      knex.schema.createTable('department', department),
      knex.schema.createTable('province', province),
      knex.schema.createTable('district', district),
      knex.schema.createTable('user', user),
      knex.schema.createTable('surveyResult', surveyResult),
      knex.schema.createTable('recommendation', recommendation),
    ]);
  };
  
  exports.down = async (knex) => {
    await Promise.all([
      knex.raw('SET foreign_key_checks = 0;'),
      knex.raw('SET foreign_key_checks = 1;'),
    ]);
  };
  
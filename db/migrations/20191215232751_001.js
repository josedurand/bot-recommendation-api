function configuration(table){
    table.increments('id').primary();
    table.string('host',100).notNullable();
    table.string('user',100).notNullable();
    table.string('password',100).notNullable();
  }
  
  exports.up = async (knex) => {
    await Promise.all([
      //knex.schema.createTable('configuration', configuration),
    ]);
  };
  
  exports.down = async (knex) => {
    await Promise.all([
      knex.raw('SET foreign_key_checks = 0;'),
      knex.schema.createTable('configuration', configuration),
      knex.raw('SET foreign_key_checks = 1;'),
    ]);
  };
  
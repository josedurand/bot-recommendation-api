
const districts = [
  {
    "A": "010101",
    "B": "CHACHAPOYAS",
    "C": 2339,
    "D": "010100",
    "E": "Chachapoyas"
},
{
    "A": "010102",
    "B": "ASUNCION",
    "C": 2783,
    "D": "010100",
    "E": "Asuncion"
},
{
    "A": "010103",
    "B": "BALSAS",
    "C": 849,
    "D": "010100",
    "E": "Balsas"
},
{
    "A": "010104",
    "B": "CHETO",
    "C": 2129,
    "D": "010100",
    "E": "Cheto"
},
{
    "A": "010105",
    "B": "CHILIQUIN",
    "C": 2617,
    "D": "010100",
    "E": "Chiliquin"
},
{
    "A": "010106",
    "B": "CHUQUIBAMBA",
    "C": 2815,
    "D": "010100",
    "E": "Chuquibamba"
},
{
    "A": "010107",
    "B": "GRANADA",
    "C": 2935,
    "D": "010100",
    "E": "Granada"
},
{
    "A": "010108",
    "B": "HUANCAS",
    "C": 2506,
    "D": "010100",
    "E": "Huancas"
},
{
    "A": "010109",
    "B": "LA JALCA",
    "C": 2795,
    "D": "010100",
    "E": "La Jalca"
},
{
    "A": "010110",
    "B": "LEIMEBAMBA",
    "C": 2158,
    "D": "010100",
    "E": "Leimebamba"
},
{
    "A": "010111",
    "B": "LEVANTO",
    "C": 2651,
    "D": "010100",
    "E": "Levanto"
},
{
    "A": "010112",
    "B": "MAGDALENA",
    "C": 1865,
    "D": "010100",
    "E": "Magdalena"
},
{
    "A": "010113",
    "B": "MARISCAL CASTILLA",
    "C": 2288,
    "D": "010100",
    "E": "Mariscal Castilla"
},
{
    "A": "010114",
    "B": "MOLINOPAMPA",
    "C": 2421,
    "D": "010100",
    "E": "Molinopampa"
},
{
    "A": "010115",
    "B": "MONTEVIDEO",
    "C": 2426,
    "D": "010100",
    "E": "Montevideo"
},
{
    "A": "010116",
    "B": "OLLEROS",
    "C": 3143,
    "D": "010100",
    "E": "Olleros"
},
{
    "A": "010117",
    "B": "QUINJALCA",
    "C": 2800,
    "D": "010100",
    "E": "Quinjalca"
},
{
    "A": "010118",
    "B": "SAN FRANCISCO DE DAGUAS",
    "C": 2280,
    "D": "010100",
    "E": "San Francisco De Daguas"
},
{
    "A": "010119",
    "B": "SAN ISIDRO DE MAINO",
    "C": 2786,
    "D": "010100",
    "E": "San Isidro De Maino"
},
{
    "A": "010120",
    "B": "SOLOCO",
    "C": 2434,
    "D": "010100",
    "E": "Soloco"
},
{
    "A": "010121",
    "B": "SONCHE",
    "C": 2086,
    "D": "010100",
    "E": "Sonche"
},
{
    "A": "010201",
    "B": "BAGUA",
    "C": 421,
    "D": "010200",
    "E": "Bagua"
},
{
    "A": "010202",
    "B": "ARAMANGO",
    "C": 531,
    "D": "010200",
    "E": "Aramango"
},
{
    "A": "010203",
    "B": "COPALLIN",
    "C": 700,
    "D": "010200",
    "E": "Copallin"
},
{
    "A": "010204",
    "B": "EL PARCO",
    "C": 597,
    "D": "010200",
    "E": "El Parco"
},
{
    "A": "010205",
    "B": "IMAZA",
    "C": 347,
    "D": "010200",
    "E": "Imaza"
},
{
    "A": "010206",
    "B": "LA PECA",
    "C": 900,
    "D": "010200",
    "E": "La Peca"
},
{
    "A": "010301",
    "B": "JUMBILLA",
    "C": 1991,
    "D": "010300",
    "E": "Jumbilla"
},
{
    "A": "010302",
    "B": "CHISQUILLA",
    "C": 2013,
    "D": "010300",
    "E": "Chisquilla"
},
{
    "A": "010303",
    "B": "CHURUJA",
    "C": 1383,
    "D": "010300",
    "E": "Churuja"
},
{
    "A": "010304",
    "B": "COROSHA",
    "C": 2180,
    "D": "010300",
    "E": "Corosha"
},
{
    "A": "010305",
    "B": "CUISPES",
    "C": 1937,
    "D": "010300",
    "E": "Cuispes"
},
{
    "A": "010306",
    "B": "FLORIDA",
    "C": 2225,
    "D": "010300",
    "E": "Florida"
},
{
    "A": "010307",
    "B": "JAZAN",
    "C": 1299,
    "D": "010300",
    "E": "Jazan"
},
{
    "A": "010308",
    "B": "RECTA",
    "C": 2140,
    "D": "010300",
    "E": "Recta"
},
{
    "A": "010309",
    "B": "SAN CARLOS",
    "C": 1890,
    "D": "010300",
    "E": "San Carlos"
},
{
    "A": "010310",
    "B": "SHIPASBAMBA",
    "C": 2083,
    "D": "010300",
    "E": "Shipasbamba"
},
{
    "A": "010311",
    "B": "VALERA",
    "C": 1978,
    "D": "010300",
    "E": "Valera"
},
{
    "A": "010312",
    "B": "YAMBRASBAMBA",
    "C": 1995,
    "D": "010300",
    "E": "Yambrasbamba"
},
{
    "A": "010401",
    "B": "NIEVA",
    "C": 222,
    "D": "010400",
    "E": "Nieva"
},
{
    "A": "010402",
    "B": "EL CENEPA",
    "C": 303,
    "D": "010400",
    "E": "El Cenepa"
},
{
    "A": "010403",
    "B": "RIO SANTIAGO",
    "C": 192,
    "D": "010400",
    "E": "Rio Santiago"
},
{
    "A": "010501",
    "B": "LAMUD",
    "C": 2307,
    "D": "010500",
    "E": "Lamud"
},
{
    "A": "010502",
    "B": "CAMPORREDONDO",
    "C": 1767,
    "D": "010500",
    "E": "Camporredondo"
},
{
    "A": "010503",
    "B": "COCABAMBA",
    "C": 2576,
    "D": "010500",
    "E": "Cocabamba"
},
{
    "A": "010504",
    "B": "COLCAMAR",
    "C": 2304,
    "D": "010500",
    "E": "Colcamar"
},
{
    "A": "010505",
    "B": "CONILA",
    "C": 3158,
    "D": "010500",
    "E": "Conila"
},
{
    "A": "010506",
    "B": "INGUILPATA",
    "C": 2395,
    "D": "010500",
    "E": "Inguilpata"
},
{
    "A": "010507",
    "B": "LONGUITA",
    "C": 2800,
    "D": "010500",
    "E": "Longuita"
},
{
    "A": "010508",
    "B": "LONYA CHICO",
    "C": 2306,
    "D": "010500",
    "E": "Lonya Chico"
},
{
    "A": "010509",
    "B": "LUYA",
    "C": 2339,
    "D": "010500",
    "E": "Luya"
},
{
    "A": "010510",
    "B": "LUYA VIEJO",
    "C": 2761,
    "D": "010500",
    "E": "Luya Viejo"
},
{
    "A": "010511",
    "B": "MARIA",
    "C": 3420,
    "D": "010500",
    "E": "Maria"
},
{
    "A": "010512",
    "B": "OCALLI",
    "C": 1774,
    "D": "010500",
    "E": "Ocalli"
},
{
    "A": "010513",
    "B": "OCUMAL",
    "C": 1802,
    "D": "010500",
    "E": "Ocumal"
},
{
    "A": "010514",
    "B": "PISUQUIA",
    "C": 2671,
    "D": "010500",
    "E": "Pisuquia"
},
{
    "A": "010515",
    "B": "PROVIDENCIA",
    "C": 1750,
    "D": "010500",
    "E": "Providencia"
},
{
    "A": "010516",
    "B": "SAN CRISTOBAL",
    "C": 2552,
    "D": "010500",
    "E": "San Cristobal"
},
{
    "A": "010517",
    "B": "SAN FRANCISCO DEL YESO",
    "C": 2379,
    "D": "010500",
    "E": "San Francisco Del Yeso"
},
{
    "A": "010518",
    "B": "SAN JERONIMO",
    "C": 2239,
    "D": "010500",
    "E": "San Jeronimo"
},
{
    "A": "010519",
    "B": "SAN JUAN DE LOPECANCHA",
    "C": 2920,
    "D": "010500",
    "E": "San Juan De Lopecancha"
},
{
    "A": "010520",
    "B": "SANTA CATALINA",
    "C": 2665,
    "D": "010500",
    "E": "Santa Catalina"
},
{
    "A": "010521",
    "B": "SANTO TOMAS",
    "C": 2595,
    "D": "010500",
    "E": "Santo Tomas"
},
{
    "A": "010522",
    "B": "TINGO",
    "C": 1811,
    "D": "010500",
    "E": "Tingo"
},
{
    "A": "010523",
    "B": "TRITA",
    "C": 2728,
    "D": "010500",
    "E": "Trita"
},
{
    "A": "010601",
    "B": "SAN NICOLAS",
    "C": 1584,
    "D": "010600",
    "E": "San Nicolas"
},
{
    "A": "010602",
    "B": "CHIRIMOTO",
    "C": 2040,
    "D": "010600",
    "E": "Chirimoto"
},
{
    "A": "010603",
    "B": "COCHAMAL",
    "C": 1697,
    "D": "010600",
    "E": "Cochamal"
},
{
    "A": "010604",
    "B": "HUAMBO",
    "C": 1684,
    "D": "010600",
    "E": "Huambo"
},
{
    "A": "010605",
    "B": "LIMABAMBA",
    "C": 1656,
    "D": "010600",
    "E": "Limabamba"
},
{
    "A": "010606",
    "B": "LONGAR",
    "C": 1584,
    "D": "010600",
    "E": "Longar"
},
{
    "A": "010607",
    "B": "MARISCAL BENAVIDES",
    "C": 1700,
    "D": "010600",
    "E": "Mariscal Benavides"
},
{
    "A": "010608",
    "B": "MILPUC",
    "C": 1675,
    "D": "010600",
    "E": "Milpuc"
},
{
    "A": "010609",
    "B": "OMIA",
    "C": 1367,
    "D": "010600",
    "E": "Omia"
},
{
    "A": "010610",
    "B": "SANTA ROSA",
    "C": 1756,
    "D": "010600",
    "E": "Santa Rosa"
},
{
    "A": "010611",
    "B": "TOTORA",
    "C": 1672,
    "D": "010600",
    "E": "Totora"
},
{
    "A": "010612",
    "B": "VISTA ALEGRE",
    "C": 1515,
    "D": "010600",
    "E": "Vista Alegre"
},
{
    "A": "010701",
    "B": "BAGUA GRANDE",
    "C": 446,
    "D": "010700",
    "E": "Bagua Grande"
},
{
    "A": "010702",
    "B": "CAJARURO",
    "C": 455,
    "D": "010700",
    "E": "Cajaruro"
},
{
    "A": "010703",
    "B": "CUMBA",
    "C": 456,
    "D": "010700",
    "E": "Cumba"
},
{
    "A": "010704",
    "B": "EL MILAGRO",
    "C": 391,
    "D": "010700",
    "E": "El Milagro"
},
{
    "A": "010705",
    "B": "JAMALCA",
    "C": 1201,
    "D": "010700",
    "E": "Jamalca"
},
{
    "A": "010706",
    "B": "LONYA GRANDE",
    "C": 1222,
    "D": "010700",
    "E": "Lonya Grande"
},
{
    "A": "010707",
    "B": "YAMON",
    "C": 1022,
    "D": "010700",
    "E": "Yamon"
},
{
    "A": "020101",
    "B": "HUARAZ",
    "C": 3038,
    "D": "020100",
    "E": "Huaraz"
},
{
    "A": "020102",
    "B": "COCHABAMBA",
    "C": 2213,
    "D": "020100",
    "E": "Cochabamba"
},
{
    "A": "020103",
    "B": "COLCABAMBA",
    "C": 3150,
    "D": "020100",
    "E": "Colcabamba"
},
{
    "A": "020104",
    "B": "HUANCHAY",
    "C": 2592,
    "D": "020100",
    "E": "Huanchay"
},
{
    "A": "020105",
    "B": "INDEPENDENCIA",
    "C": 3023,
    "D": "020100",
    "E": "Independencia"
},
{
    "A": "020106",
    "B": "JANGAS",
    "C": 2809,
    "D": "020100",
    "E": "Jangas"
},
{
    "A": "020107",
    "B": "LA LIBERTAD",
    "C": 3350,
    "D": "020100",
    "E": "La Libertad"
},
{
    "A": "020108",
    "B": "OLLEROS",
    "C": 3457,
    "D": "020100",
    "E": "Olleros"
},
{
    "A": "020109",
    "B": "PAMPAS GRANDE",
    "C": 3642,
    "D": "020100",
    "E": "Pampas Grande"
},
{
    "A": "020110",
    "B": "PARIACOTO",
    "C": 1221,
    "D": "020100",
    "E": "Pariacoto"
},
{
    "A": "020111",
    "B": "PIRA",
    "C": 3580,
    "D": "020100",
    "E": "Pira"
},
{
    "A": "020112",
    "B": "TARICA",
    "C": 2799,
    "D": "020100",
    "E": "Tarica"
},
{
    "A": "020201",
    "B": "AIJA",
    "C": 3427,
    "D": "020200",
    "E": "Aija"
},
{
    "A": "020202",
    "B": "CORIS",
    "C": 2763,
    "D": "020200",
    "E": "Coris"
},
{
    "A": "020203",
    "B": "HUACLLAN",
    "C": 3019,
    "D": "020200",
    "E": "Huacllan"
},
{
    "A": "020204",
    "B": "LA MERCED",
    "C": 3326,
    "D": "020200",
    "E": "La Merced"
},
{
    "A": "020205",
    "B": "SUCCHA",
    "C": 3147,
    "D": "020200",
    "E": "Succha"
},
{
    "A": "020301",
    "B": "LLAMELLIN",
    "C": 3457,
    "D": "020300",
    "E": "Llamellin"
},
{
    "A": "020302",
    "B": "ACZO",
    "C": 2657,
    "D": "020300",
    "E": "Aczo"
},
{
    "A": "020303",
    "B": "CHACCHO",
    "C": 3323,
    "D": "020300",
    "E": "Chaccho"
},
{
    "A": "020304",
    "B": "CHINGAS",
    "C": 2911,
    "D": "020300",
    "E": "Chingas"
},
{
    "A": "020305",
    "B": "MIRGAS",
    "C": 3105,
    "D": "020300",
    "E": "Mirgas"
},
{
    "A": "020306",
    "B": "SAN JUAN DE RONTOY",
    "C": 3500,
    "D": "020300",
    "E": "San Juan De Rontoy"
},
{
    "A": "020401",
    "B": "CHACAS",
    "C": 3336,
    "D": "020400",
    "E": "Chacas"
},
{
    "A": "020402",
    "B": "ACOCHACA",
    "C": 2980,
    "D": "020400",
    "E": "Acochaca"
},
{
    "A": "020501",
    "B": "CHIQUIAN",
    "C": 3401,
    "D": "020500",
    "E": "Chiquian"
},
{
    "A": "020502",
    "B": "ABELARDO PARDO LEZAMETA",
    "C": 2204,
    "D": "020500",
    "E": "Abelardo Pardo Lezameta"
},
{
    "A": "020503",
    "B": "ANTONIO RAYMONDI",
    "C": 2114,
    "D": "020500",
    "E": "Antonio Raymondi"
},
{
    "A": "020504",
    "B": "AQUIA",
    "C": 3382,
    "D": "020500",
    "E": "Aquia"
},
{
    "A": "020505",
    "B": "CAJACAY",
    "C": 2562,
    "D": "020500",
    "E": "Cajacay"
},
{
    "A": "020506",
    "B": "CANIS",
    "C": 2502,
    "D": "020500",
    "E": "Canis"
},
{
    "A": "020507",
    "B": "COLQUIOC",
    "C": 744,
    "D": "020500",
    "E": "Colquioc"
},
{
    "A": "020508",
    "B": "HUALLANCA",
    "C": 3641,
    "D": "020500",
    "E": "Huallanca"
},
{
    "A": "020509",
    "B": "HUASTA",
    "C": 3365,
    "D": "020500",
    "E": "Huasta"
},
{
    "A": "020510",
    "B": "HUAYLLACAYAN",
    "C": 3259,
    "D": "020500",
    "E": "Huayllacayan"
},
{
    "A": "020511",
    "B": "LA PRIMAVERA",
    "C": 2624,
    "D": "020500",
    "E": "La Primavera"
},
{
    "A": "020512",
    "B": "MANGAS",
    "C": 3449,
    "D": "020500",
    "E": "Mangas"
},
{
    "A": "020513",
    "B": "PACLLON",
    "C": 3244,
    "D": "020500",
    "E": "Pacllon"
},
{
    "A": "020514",
    "B": "SAN MIGUEL DE CORPANQUI",
    "C": 3395,
    "D": "020500",
    "E": "San Miguel De Corpanqui"
},
{
    "A": "020515",
    "B": "TICLLOS",
    "C": 3650,
    "D": "020500",
    "E": "Ticllos"
},
{
    "A": "020601",
    "B": "CARHUAZ",
    "C": 2632,
    "D": "020600",
    "E": "Carhuaz"
},
{
    "A": "020602",
    "B": "ACOPAMPA",
    "C": 2691,
    "D": "020600",
    "E": "Acopampa"
},
{
    "A": "020603",
    "B": "AMASHCA",
    "C": 2878,
    "D": "020600",
    "E": "Amashca"
},
{
    "A": "020604",
    "B": "ANTA",
    "C": 2777,
    "D": "020600",
    "E": "Anta"
},
{
    "A": "020605",
    "B": "ATAQUERO",
    "C": 2739,
    "D": "020600",
    "E": "Ataquero"
},
{
    "A": "020606",
    "B": "MARCARA",
    "C": 2757,
    "D": "020600",
    "E": "Marcara"
},
{
    "A": "020607",
    "B": "PARIAHUANCA",
    "C": 2785,
    "D": "020600",
    "E": "Pariahuanca"
},
{
    "A": "020608",
    "B": "SAN MIGUEL DE ACO",
    "C": 2940,
    "D": "020600",
    "E": "San Miguel De Aco"
},
{
    "A": "020609",
    "B": "SHILLA",
    "C": 3014,
    "D": "020600",
    "E": "Shilla"
},
{
    "A": "020610",
    "B": "TINCO",
    "C": 2581,
    "D": "020600",
    "E": "Tinco"
},
{
    "A": "020611",
    "B": "YUNGAR",
    "C": 2836,
    "D": "020600",
    "E": "Yungar"
},
{
    "A": "020701",
    "B": "SAN LUIS",
    "C": 3079,
    "D": "020700",
    "E": "San Luis"
},
{
    "A": "020702",
    "B": "SAN NICOLAS",
    "C": 2758,
    "D": "020700",
    "E": "San Nicolas"
},
{
    "A": "020703",
    "B": "YAUYA",
    "C": 3164,
    "D": "020700",
    "E": "Yauya"
},
{
    "A": "020801",
    "B": "CASMA",
    "C": 45,
    "D": "020800",
    "E": "Casma"
},
{
    "A": "020802",
    "B": "BUENA VISTA ALTA",
    "C": 216,
    "D": "020800",
    "E": "Buena Vista Alta"
},
{
    "A": "020803",
    "B": "COMANDANTE NOEL",
    "C": 4,
    "D": "020800",
    "E": "Comandante Noel"
},
{
    "A": "020804",
    "B": "YAUTAN",
    "C": 806,
    "D": "020800",
    "E": "Yautan"
},
{
    "A": "020901",
    "B": "CORONGO",
    "C": 3173,
    "D": "020900",
    "E": "Corongo"
},
{
    "A": "020902",
    "B": "ACO",
    "C": 3106,
    "D": "020900",
    "E": "Aco"
},
{
    "A": "020903",
    "B": "BAMBAS",
    "C": 3002,
    "D": "020900",
    "E": "Bambas"
},
{
    "A": "020904",
    "B": "CUSCA",
    "C": 3241,
    "D": "020900",
    "E": "Cusca"
},
{
    "A": "020905",
    "B": "LA PAMPA",
    "C": 1784,
    "D": "020900",
    "E": "La Pampa"
},
{
    "A": "020906",
    "B": "YANAC",
    "C": 2885,
    "D": "020900",
    "E": "Yanac"
},
{
    "A": "020907",
    "B": "YUPAN",
    "C": 2728,
    "D": "020900",
    "E": "Yupan"
},
{
    "A": "021001",
    "B": "HUARI",
    "C": 3110,
    "D": "021000",
    "E": "Huari"
},
{
    "A": "021002",
    "B": "ANRA",
    "C": 3203,
    "D": "021000",
    "E": "Anra"
},
{
    "A": "021003",
    "B": "CAJAY",
    "C": 3175,
    "D": "021000",
    "E": "Cajay"
},
{
    "A": "021004",
    "B": "CHAVIN DE HUANTAR",
    "C": 3141,
    "D": "021000",
    "E": "Chavin De Huantar"
},
{
    "A": "021005",
    "B": "HUACACHI",
    "C": 3432,
    "D": "021000",
    "E": "Huacachi"
},
{
    "A": "021006",
    "B": "HUACCHIS",
    "C": 3465,
    "D": "021000",
    "E": "Huacchis"
},
{
    "A": "021007",
    "B": "HUACHIS",
    "C": 3243,
    "D": "021000",
    "E": "Huachis"
},
{
    "A": "021008",
    "B": "HUANTAR",
    "C": 3353,
    "D": "021000",
    "E": "Huantar"
},
{
    "A": "021009",
    "B": "MASIN",
    "C": 2552,
    "D": "021000",
    "E": "Masin"
},
{
    "A": "021010",
    "B": "PAUCAS",
    "C": 3424,
    "D": "021000",
    "E": "Paucas"
},
{
    "A": "021011",
    "B": "PONTO",
    "C": 3115,
    "D": "021000",
    "E": "Ponto"
},
{
    "A": "021012",
    "B": "RAHUAPAMPA",
    "C": 2510,
    "D": "021000",
    "E": "Rahuapampa"
},
{
    "A": "021013",
    "B": "RAPAYAN",
    "C": 3239,
    "D": "021000",
    "E": "Rapayan"
},
{
    "A": "021014",
    "B": "SAN MARCOS",
    "C": 2956,
    "D": "021000",
    "E": "San Marcos"
},
{
    "A": "021015",
    "B": "SAN PEDRO DE CHANA",
    "C": 3385,
    "D": "021000",
    "E": "San Pedro De Chana"
},
{
    "A": "021016",
    "B": "UCO",
    "C": 3348,
    "D": "021000",
    "E": "Uco"
},
{
    "A": "021101",
    "B": "HUARMEY",
    "C": 12,
    "D": "021100",
    "E": "Huarmey"
},
{
    "A": "021102",
    "B": "COCHAPETI",
    "C": 3494,
    "D": "021100",
    "E": "Cochapeti"
},
{
    "A": "021103",
    "B": "CULEBRAS",
    "C": 21,
    "D": "021100",
    "E": "Culebras"
},
{
    "A": "021104",
    "B": "HUAYAN",
    "C": 2693,
    "D": "021100",
    "E": "Huayan"
},
{
    "A": "021105",
    "B": "MALVAS",
    "C": 3126,
    "D": "021100",
    "E": "Malvas"
},
{
    "A": "021201",
    "B": "CARAZ",
    "C": 2278,
    "D": "021200",
    "E": "Caraz"
},
{
    "A": "021202",
    "B": "HUALLANCA",
    "C": 1358,
    "D": "021200",
    "E": "Huallanca"
},
{
    "A": "021203",
    "B": "HUATA",
    "C": 2723,
    "D": "021200",
    "E": "Huata"
},
{
    "A": "021204",
    "B": "HUAYLAS",
    "C": 2691,
    "D": "021200",
    "E": "Huaylas"
},
{
    "A": "021205",
    "B": "MATO",
    "C": 2234,
    "D": "021200",
    "E": "Mato"
},
{
    "A": "021206",
    "B": "PAMPAROMAS",
    "C": 2742,
    "D": "021200",
    "E": "Pamparomas"
},
{
    "A": "021207",
    "B": "PUEBLO LIBRE",
    "C": 2488,
    "D": "021200",
    "E": "Pueblo Libre"
},
{
    "A": "021208",
    "B": "SANTA CRUZ",
    "C": 2868,
    "D": "021200",
    "E": "Santa Cruz"
},
{
    "A": "021209",
    "B": "SANTO TORIBIO",
    "C": 2919,
    "D": "021200",
    "E": "Santo Toribio"
},
{
    "A": "021210",
    "B": "YURACMARCA",
    "C": 1483,
    "D": "021200",
    "E": "Yuracmarca"
},
{
    "A": "021301",
    "B": "PISCOBAMBA",
    "C": 3371,
    "D": "021300",
    "E": "Piscobamba"
},
{
    "A": "021302",
    "B": "CASCA",
    "C": 3139,
    "D": "021300",
    "E": "Casca"
},
{
    "A": "021303",
    "B": "ELEAZAR GUZMAN BARRON",
    "C": 2922,
    "D": "021300",
    "E": "Eleazar Guzman Barron"
},
{
    "A": "021304",
    "B": "FIDEL OLIVAS ESCUDERO",
    "C": 2905,
    "D": "021300",
    "E": "Fidel Olivas Escudero"
},
{
    "A": "021305",
    "B": "LLAMA",
    "C": 2829,
    "D": "021300",
    "E": "Llama"
},
{
    "A": "021306",
    "B": "LLUMPA",
    "C": 3160,
    "D": "021300",
    "E": "Llumpa"
},
{
    "A": "021307",
    "B": "LUCMA",
    "C": 3053,
    "D": "021300",
    "E": "Lucma"
},
{
    "A": "021308",
    "B": "MUSGA",
    "C": 3028,
    "D": "021300",
    "E": "Musga"
},
{
    "A": "021401",
    "B": "OCROS",
    "C": 3311,
    "D": "021400",
    "E": "Ocros"
},
{
    "A": "021402",
    "B": "ACAS",
    "C": 3702,
    "D": "021400",
    "E": "Acas"
},
{
    "A": "021403",
    "B": "CAJAMARQUILLA",
    "C": 3536,
    "D": "021400",
    "E": "Cajamarquilla"
},
{
    "A": "021404",
    "B": "CARHUAPAMPA",
    "C": 2276,
    "D": "021400",
    "E": "Carhuapampa"
},
{
    "A": "021405",
    "B": "COCHAS",
    "C": 1365,
    "D": "021400",
    "E": "Cochas"
},
{
    "A": "021406",
    "B": "CONGAS",
    "C": 3137,
    "D": "021400",
    "E": "Congas"
},
{
    "A": "021407",
    "B": "LLIPA",
    "C": 3029,
    "D": "021400",
    "E": "Llipa"
},
{
    "A": "021408",
    "B": "SAN CRISTOBAL DE RAJAN",
    "C": 3621,
    "D": "021400",
    "E": "San Cristobal De Rajan"
},
{
    "A": "021409",
    "B": "SAN PEDRO",
    "C": 2270,
    "D": "021400",
    "E": "San Pedro"
},
{
    "A": "021410",
    "B": "SANTIAGO DE CHILCAS",
    "C": 3654,
    "D": "021400",
    "E": "Santiago De Chilcas"
},
{
    "A": "021501",
    "B": "CABANA",
    "C": 3231,
    "D": "021500",
    "E": "Cabana"
},
{
    "A": "021502",
    "B": "BOLOGNESI",
    "C": 2876,
    "D": "021500",
    "E": "Bolognesi"
},
{
    "A": "021503",
    "B": "CONCHUCOS",
    "C": 3251,
    "D": "021500",
    "E": "Conchucos"
},
{
    "A": "021504",
    "B": "HUACASCHUQUE",
    "C": 3105,
    "D": "021500",
    "E": "Huacaschuque"
},
{
    "A": "021505",
    "B": "HUANDOVAL",
    "C": 2957,
    "D": "021500",
    "E": "Huandoval"
},
{
    "A": "021506",
    "B": "LACABAMBA",
    "C": 3277,
    "D": "021500",
    "E": "Lacabamba"
},
{
    "A": "021507",
    "B": "LLAPO",
    "C": 3308,
    "D": "021500",
    "E": "Llapo"
},
{
    "A": "021508",
    "B": "PALLASCA",
    "C": 3043,
    "D": "021500",
    "E": "Pallasca"
},
{
    "A": "021509",
    "B": "PAMPAS",
    "C": 3142,
    "D": "021500",
    "E": "Pampas"
},
{
    "A": "021510",
    "B": "SANTA ROSA",
    "C": 2324,
    "D": "021500",
    "E": "Santa Rosa"
},
{
    "A": "021511",
    "B": "TAUCA",
    "C": 3341,
    "D": "021500",
    "E": "Tauca"
},
{
    "A": "021601",
    "B": "POMABAMBA",
    "C": 3057,
    "D": "021600",
    "E": "Pomabamba"
},
{
    "A": "021602",
    "B": "HUAYLLAN",
    "C": 2855,
    "D": "021600",
    "E": "Huayllan"
},
{
    "A": "021603",
    "B": "PAROBAMBA",
    "C": 3251,
    "D": "021600",
    "E": "Parobamba"
},
{
    "A": "021604",
    "B": "QUINUABAMBA",
    "C": 3062,
    "D": "021600",
    "E": "Quinuabamba"
},
{
    "A": "021701",
    "B": "RECUAY",
    "C": 3398,
    "D": "021700",
    "E": "Recuay"
},
{
    "A": "021702",
    "B": "CATAC",
    "C": 3557,
    "D": "021700",
    "E": "Catac"
},
{
    "A": "021703",
    "B": "COTAPARACO",
    "C": 3023,
    "D": "021700",
    "E": "Cotaparaco"
},
{
    "A": "021704",
    "B": "HUAYLLAPAMPA",
    "C": 2907,
    "D": "021700",
    "E": "Huayllapampa"
},
{
    "A": "021705",
    "B": "LLACLLIN",
    "C": 2993,
    "D": "021700",
    "E": "Llacllin"
},
{
    "A": "021706",
    "B": "MARCA",
    "C": 2644,
    "D": "021700",
    "E": "Marca"
},
{
    "A": "021707",
    "B": "PAMPAS CHICO",
    "C": 3520,
    "D": "021700",
    "E": "Pampas Chico"
},
{
    "A": "021708",
    "B": "PARARIN",
    "C": 3388,
    "D": "021700",
    "E": "Pararin"
},
{
    "A": "021709",
    "B": "TAPACOCHA",
    "C": 3617,
    "D": "021700",
    "E": "Tapacocha"
},
{
    "A": "021710",
    "B": "TICAPAMPA",
    "C": 3465,
    "D": "021700",
    "E": "Ticapampa"
},
{
    "A": "021801",
    "B": "CHIMBOTE",
    "C": 13,
    "D": "021800",
    "E": "Chimbote"
},
{
    "A": "021802",
    "B": "CACERES DEL PERU",
    "C": 1166,
    "D": "021800",
    "E": "Caceres Del Peru"
},
{
    "A": "021803",
    "B": "COISHCO",
    "C": 39,
    "D": "021800",
    "E": "Coishco"
},
{
    "A": "021804",
    "B": "MACATE",
    "C": 2717,
    "D": "021800",
    "E": "Macate"
},
{
    "A": "021805",
    "B": "MORO",
    "C": 480,
    "D": "021800",
    "E": "Moro"
},
{
    "A": "021806",
    "B": "NEPEÑA",
    "C": 141,
    "D": "021800",
    "E": "Nepeña"
},
{
    "A": "021807",
    "B": "SAMANCO",
    "C": 128,
    "D": "021800",
    "E": "Samanco"
},
{
    "A": "021808",
    "B": "SANTA",
    "C": 21,
    "D": "021800",
    "E": "Santa"
},
{
    "A": "021809",
    "B": "NUEVO CHIMBOTE",
    "C": 22,
    "D": "021800",
    "E": "Nuevo Chimbote"
},
{
    "A": "021901",
    "B": "SIHUAS",
    "C": 2784,
    "D": "021900",
    "E": "Sihuas"
},
{
    "A": "021902",
    "B": "ACOBAMBA",
    "C": 3129,
    "D": "021900",
    "E": "Acobamba"
},
{
    "A": "021903",
    "B": "ALFONSO UGARTE",
    "C": 3227,
    "D": "021900",
    "E": "Alfonso Ugarte"
},
{
    "A": "021904",
    "B": "CASHAPAMPA",
    "C": 3577,
    "D": "021900",
    "E": "Cashapampa"
},
{
    "A": "021905",
    "B": "CHINGALPO",
    "C": 3187,
    "D": "021900",
    "E": "Chingalpo"
},
{
    "A": "021906",
    "B": "HUAYLLABAMBA",
    "C": 3377,
    "D": "021900",
    "E": "Huayllabamba"
},
{
    "A": "021907",
    "B": "QUICHES",
    "C": 2998,
    "D": "021900",
    "E": "Quiches"
},
{
    "A": "021908",
    "B": "RAGASH",
    "C": 3528,
    "D": "021900",
    "E": "Ragash"
},
{
    "A": "021909",
    "B": "SAN JUAN",
    "C": 2729,
    "D": "021900",
    "E": "San Juan"
},
{
    "A": "021910",
    "B": "SICSIBAMBA",
    "C": 3115,
    "D": "021900",
    "E": "Sicsibamba"
},
{
    "A": "022001",
    "B": "YUNGAY",
    "C": 2463,
    "D": "022000",
    "E": "Yungay"
},
{
    "A": "022002",
    "B": "CASCAPARA",
    "C": 2801,
    "D": "022000",
    "E": "Cascapara"
},
{
    "A": "022003",
    "B": "MANCOS",
    "C": 2482,
    "D": "022000",
    "E": "Mancos"
},
{
    "A": "022004",
    "B": "MATACOTO",
    "C": 2528,
    "D": "022000",
    "E": "Matacoto"
},
{
    "A": "022005",
    "B": "QUILLO",
    "C": 1238,
    "D": "022000",
    "E": "Quillo"
},
{
    "A": "022006",
    "B": "RANRAHIRCA",
    "C": 2470,
    "D": "022000",
    "E": "Ranrahirca"
},
{
    "A": "022007",
    "B": "SHUPLUY",
    "C": 2555,
    "D": "022000",
    "E": "Shupluy"
},
{
    "A": "022008",
    "B": "YANAMA",
    "C": 3358,
    "D": "022000",
    "E": "Yanama"
},
{
    "A": "030101",
    "B": "ABANCAY",
    "C": 2392,
    "D": "030100",
    "E": "Abancay"
},
{
    "A": "030102",
    "B": "CHACOCHE",
    "C": 3468,
    "D": "030100",
    "E": "Chacoche"
},
{
    "A": "030103",
    "B": "CIRCA",
    "C": 3192,
    "D": "030100",
    "E": "Circa"
},
{
    "A": "030104",
    "B": "CURAHUASI",
    "C": 2684,
    "D": "030100",
    "E": "Curahuasi"
},
{
    "A": "030105",
    "B": "HUANIPACA",
    "C": 3196,
    "D": "030100",
    "E": "Huanipaca"
},
{
    "A": "030106",
    "B": "LAMBRAMA",
    "C": 3271,
    "D": "030100",
    "E": "Lambrama"
},
{
    "A": "030107",
    "B": "PICHIRHUA",
    "C": 2738,
    "D": "030100",
    "E": "Pichirhua"
},
{
    "A": "030108",
    "B": "SAN PEDRO DE CACHORA",
    "C": 2902,
    "D": "030100",
    "E": "San Pedro De Cachora"
},
{
    "A": "030109",
    "B": "TAMBURCO",
    "C": 2619,
    "D": "030100",
    "E": "Tamburco"
},
{
    "A": "030201",
    "B": "ANDAHUAYLAS",
    "C": 2901,
    "D": "030200",
    "E": "Andahuaylas"
},
{
    "A": "030202",
    "B": "ANDARAPA",
    "C": 2907,
    "D": "030200",
    "E": "Andarapa"
},
{
    "A": "030203",
    "B": "CHIARA",
    "C": 3278,
    "D": "030200",
    "E": "Chiara"
},
{
    "A": "030204",
    "B": "HUANCARAMA",
    "C": 2980,
    "D": "030200",
    "E": "Huancarama"
},
{
    "A": "030205",
    "B": "HUANCARAY",
    "C": 2905,
    "D": "030200",
    "E": "Huancaray"
},
{
    "A": "030206",
    "B": "HUAYANA",
    "C": 3170,
    "D": "030200",
    "E": "Huayana"
},
{
    "A": "030207",
    "B": "KISHUARA",
    "C": 3643,
    "D": "030200",
    "E": "Kishuara"
},
{
    "A": "030208",
    "B": "PACOBAMBA",
    "C": 2722,
    "D": "030200",
    "E": "Pacobamba"
},
{
    "A": "030209",
    "B": "PACUCHA",
    "C": 3147,
    "D": "030200",
    "E": "Pacucha"
},
{
    "A": "030210",
    "B": "PAMPACHIRI",
    "C": 3393,
    "D": "030200",
    "E": "Pampachiri"
},
{
    "A": "030211",
    "B": "POMACOCHA",
    "C": 3670,
    "D": "030200",
    "E": "Pomacocha"
},
{
    "A": "030212",
    "B": "SAN ANTONIO DE CACHI",
    "C": 3225,
    "D": "030200",
    "E": "San Antonio De Cachi"
},
{
    "A": "030213",
    "B": "SAN JERONIMO",
    "C": 2956,
    "D": "030200",
    "E": "San Jeronimo"
},
{
    "A": "030214",
    "B": "SAN MIGUEL DE CHACCRAMPA",
    "C": 3647,
    "D": "030200",
    "E": "San Miguel De Chaccrampa"
},
{
    "A": "030215",
    "B": "SANTA MARIA DE CHICMO",
    "C": 3272,
    "D": "030200",
    "E": "Santa Maria De Chicmo"
},
{
    "A": "030216",
    "B": "TALAVERA",
    "C": 2830,
    "D": "030200",
    "E": "Talavera"
},
{
    "A": "030217",
    "B": "TUMAY HUARACA",
    "C": 3307,
    "D": "030200",
    "E": "Tumay Huaraca"
},
{
    "A": "030218",
    "B": "TURPO",
    "C": 3297,
    "D": "030200",
    "E": "Turpo"
},
{
    "A": "030219",
    "B": "KAQUIABAMBA",
    "C": 3184,
    "D": "030200",
    "E": "Kaquiabamba"
},
{
    "A": "030220",
    "B": "JOSE MARIA ARGUEDAS",
    "C": 2926,
    "D": "030200",
    "E": "Jose Maria Arguedas"
},
{
    "A": "030301",
    "B": "ANTABAMBA",
    "C": 3640,
    "D": "030300",
    "E": "Antabamba"
},
{
    "A": "030302",
    "B": "EL ORO",
    "C": 3272,
    "D": "030300",
    "E": "El Oro"
},
{
    "A": "030303",
    "B": "HUAQUIRCA",
    "C": 3575,
    "D": "030300",
    "E": "Huaquirca"
},
{
    "A": "030304",
    "B": "JUAN ESPINOZA MEDRANO",
    "C": 3296,
    "D": "030300",
    "E": "Juan Espinoza Medrano"
},
{
    "A": "030305",
    "B": "OROPESA",
    "C": 3388,
    "D": "030300",
    "E": "Oropesa"
},
{
    "A": "030306",
    "B": "PACHACONAS",
    "C": 3449,
    "D": "030300",
    "E": "Pachaconas"
},
{
    "A": "030307",
    "B": "SABAINO",
    "C": 3457,
    "D": "030300",
    "E": "Sabaino"
},
{
    "A": "030401",
    "B": "CHALHUANCA",
    "C": 2911,
    "D": "030400",
    "E": "Chalhuanca"
},
{
    "A": "030402",
    "B": "CAPAYA",
    "C": 3292,
    "D": "030400",
    "E": "Capaya"
},
{
    "A": "030403",
    "B": "CARAYBAMBA",
    "C": 3338,
    "D": "030400",
    "E": "Caraybamba"
},
{
    "A": "030404",
    "B": "CHAPIMARCA",
    "C": 3399,
    "D": "030400",
    "E": "Chapimarca"
},
{
    "A": "030405",
    "B": "COLCABAMBA",
    "C": 3152,
    "D": "030400",
    "E": "Colcabamba"
},
{
    "A": "030406",
    "B": "COTARUSE",
    "C": 3257,
    "D": "030400",
    "E": "Cotaruse"
},
{
    "A": "030407",
    "B": "HUAYLLO",
    "C": 3115,
    "D": "030400",
    "E": "Huayllo"
},
{
    "A": "030408",
    "B": "JUSTO APU SAHUARAURA",
    "C": 3146,
    "D": "030400",
    "E": "Justo Apu Sahuaraura"
},
{
    "A": "030409",
    "B": "LUCRE",
    "C": 2820,
    "D": "030400",
    "E": "Lucre"
},
{
    "A": "030410",
    "B": "POCOHUANCA",
    "C": 3376,
    "D": "030400",
    "E": "Pocohuanca"
},
{
    "A": "030411",
    "B": "SAN JUAN DE CHACÑA",
    "C": 2859,
    "D": "030400",
    "E": "San Juan De Chacña"
},
{
    "A": "030412",
    "B": "SAÑAYCA",
    "C": 3398,
    "D": "030400",
    "E": "Sañayca"
},
{
    "A": "030413",
    "B": "SORAYA",
    "C": 2881,
    "D": "030400",
    "E": "Soraya"
},
{
    "A": "030414",
    "B": "TAPAIRIHUA",
    "C": 2712,
    "D": "030400",
    "E": "Tapairihua"
},
{
    "A": "030415",
    "B": "TINTAY",
    "C": 2796,
    "D": "030400",
    "E": "Tintay"
},
{
    "A": "030416",
    "B": "TORAYA",
    "C": 3159,
    "D": "030400",
    "E": "Toraya"
},
{
    "A": "030417",
    "B": "YANACA",
    "C": 3315,
    "D": "030400",
    "E": "Yanaca"
},
{
    "A": "030501",
    "B": "TAMBOBAMBA",
    "C": 3292,
    "D": "030500",
    "E": "Tambobamba"
},
{
    "A": "030502",
    "B": "COTABAMBAS",
    "C": 3487,
    "D": "030500",
    "E": "Cotabambas"
},
{
    "A": "030503",
    "B": "COYLLURQUI",
    "C": 3127,
    "D": "030500",
    "E": "Coyllurqui"
},
{
    "A": "030504",
    "B": "HAQUIRA",
    "C": 3712,
    "D": "030500",
    "E": "Haquira"
},
{
    "A": "030505",
    "B": "MARA",
    "C": 3766,
    "D": "030500",
    "E": "Mara"
},
{
    "A": "030506",
    "B": "CHALLHUAHUACHO",
    "C": 3698,
    "D": "030500",
    "E": "Challhuahuacho"
},
{
    "A": "030601",
    "B": "CHINCHEROS",
    "C": 2795,
    "D": "030600",
    "E": "Chincheros"
},
{
    "A": "030602",
    "B": "ANCO-HUALLO",
    "C": 3209,
    "D": "030600",
    "E": "Anco-Huallo"
},
{
    "A": "030603",
    "B": "COCHARCAS",
    "C": 3032,
    "D": "030600",
    "E": "Cocharcas"
},
{
    "A": "030604",
    "B": "HUACCANA",
    "C": 3078,
    "D": "030600",
    "E": "Huaccana"
},
{
    "A": "030605",
    "B": "OCOBAMBA",
    "C": 3036,
    "D": "030600",
    "E": "Ocobamba"
},
{
    "A": "030606",
    "B": "ONGOY",
    "C": 2825,
    "D": "030600",
    "E": "Ongoy"
},
{
    "A": "030607",
    "B": "URANMARCA",
    "C": 3090,
    "D": "030600",
    "E": "Uranmarca"
},
{
    "A": "030608",
    "B": "RANRACANCHA",
    "C": 3413,
    "D": "030600",
    "E": "Ranracancha"
},
{
    "A": "030701",
    "B": "CHUQUIBAMBILLA",
    "C": 3376,
    "D": "030700",
    "E": "Chuquibambilla"
},
{
    "A": "030702",
    "B": "CURPAHUASI",
    "C": 3500,
    "D": "030700",
    "E": "Curpahuasi"
},
{
    "A": "030703",
    "B": "GAMARRA",
    "C": 3400,
    "D": "030700",
    "E": "Gamarra"
},
{
    "A": "030704",
    "B": "HUAYLLATI",
    "C": 3469,
    "D": "030700",
    "E": "Huayllati"
},
{
    "A": "030705",
    "B": "MAMARA",
    "C": 3588,
    "D": "030700",
    "E": "Mamara"
},
{
    "A": "030706",
    "B": "MICAELA BASTIDAS",
    "C": 3510,
    "D": "030700",
    "E": "Micaela Bastidas"
},
{
    "A": "030707",
    "B": "PATAYPAMPA",
    "C": 3777,
    "D": "030700",
    "E": "Pataypampa"
},
{
    "A": "030708",
    "B": "PROGRESO",
    "C": 3863,
    "D": "030700",
    "E": "Progreso"
},
{
    "A": "030709",
    "B": "SAN ANTONIO",
    "C": 3452,
    "D": "030700",
    "E": "San Antonio"
},
{
    "A": "030710",
    "B": "SANTA ROSA",
    "C": 3572,
    "D": "030700",
    "E": "Santa Rosa"
},
{
    "A": "030711",
    "B": "TURPAY",
    "C": 3531,
    "D": "030700",
    "E": "Turpay"
},
{
    "A": "030712",
    "B": "VILCABAMBA",
    "C": 2784,
    "D": "030700",
    "E": "Vilcabamba"
},
{
    "A": "030713",
    "B": "VIRUNDO",
    "C": 3865,
    "D": "030700",
    "E": "Virundo"
},
{
    "A": "030714",
    "B": "CURASCO",
    "C": 3537,
    "D": "030700",
    "E": "Curasco"
},
{
    "A": "040101",
    "B": "AREQUIPA",
    "C": 2337,
    "D": "040100",
    "E": "Arequipa"
},
{
    "A": "040102",
    "B": "ALTO SELVA ALEGRE",
    "C": 2460,
    "D": "040100",
    "E": "Alto Selva Alegre"
},
{
    "A": "040103",
    "B": "CAYMA",
    "C": 2368,
    "D": "040100",
    "E": "Cayma"
},
{
    "A": "040104",
    "B": "CERRO COLORADO",
    "C": 2419,
    "D": "040100",
    "E": "Cerro Colorado"
},
{
    "A": "040105",
    "B": "CHARACATO",
    "C": 2459,
    "D": "040100",
    "E": "Characato"
},
{
    "A": "040106",
    "B": "CHIGUATA",
    "C": 2946,
    "D": "040100",
    "E": "Chiguata"
},
{
    "A": "040107",
    "B": "JACOBO HUNTER",
    "C": 2250,
    "D": "040100",
    "E": "Jacobo Hunter"
},
{
    "A": "040108",
    "B": "LA JOYA",
    "C": 1617,
    "D": "040100",
    "E": "La Joya"
},
{
    "A": "040109",
    "B": "MARIANO MELGAR",
    "C": 2409,
    "D": "040100",
    "E": "Mariano Melgar"
},
{
    "A": "040110",
    "B": "MIRAFLORES",
    "C": 2415,
    "D": "040100",
    "E": "Miraflores"
},
{
    "A": "040111",
    "B": "MOLLEBAYA",
    "C": 2505,
    "D": "040100",
    "E": "Mollebaya"
},
{
    "A": "040112",
    "B": "PAUCARPATA",
    "C": 2410,
    "D": "040100",
    "E": "Paucarpata"
},
{
    "A": "040113",
    "B": "POCSI",
    "C": 3045,
    "D": "040100",
    "E": "Pocsi"
},
{
    "A": "040114",
    "B": "POLOBAYA",
    "C": 3075,
    "D": "040100",
    "E": "Polobaya"
},
{
    "A": "040115",
    "B": "QUEQUEÑA",
    "C": 2536,
    "D": "040100",
    "E": "Quequeña"
},
{
    "A": "040116",
    "B": "SABANDIA",
    "C": 2399,
    "D": "040100",
    "E": "Sabandia"
},
{
    "A": "040117",
    "B": "SACHACA",
    "C": 2236,
    "D": "040100",
    "E": "Sachaca"
},
{
    "A": "040118",
    "B": "SAN JUAN DE SIGUAS",
    "C": 1262,
    "D": "040100",
    "E": "San Juan De Siguas"
},
{
    "A": "040119",
    "B": "SAN JUAN DE TARUCANI",
    "C": 4248,
    "D": "040100",
    "E": "San Juan De Tarucani"
},
{
    "A": "040120",
    "B": "SANTA ISABEL DE SIGUAS",
    "C": 1344,
    "D": "040100",
    "E": "Santa Isabel De Siguas"
},
{
    "A": "040121",
    "B": "SANTA RITA DE SIGUAS",
    "C": 1277,
    "D": "040100",
    "E": "Santa Rita De Siguas"
},
{
    "A": "040122",
    "B": "SOCABAYA",
    "C": 2287,
    "D": "040100",
    "E": "Socabaya"
},
{
    "A": "040123",
    "B": "TIABAYA",
    "C": 2173,
    "D": "040100",
    "E": "Tiabaya"
},
{
    "A": "040124",
    "B": "UCHUMAYO",
    "C": 1973,
    "D": "040100",
    "E": "Uchumayo"
},
{
    "A": "040125",
    "B": "VITOR",
    "C": 1244,
    "D": "040100",
    "E": "Vitor"
},
{
    "A": "040126",
    "B": "YANAHUARA",
    "C": 2343,
    "D": "040100",
    "E": "Yanahuara"
},
{
    "A": "040127",
    "B": "YARABAMBA",
    "C": 2474,
    "D": "040100",
    "E": "Yarabamba"
},
{
    "A": "040128",
    "B": "YURA",
    "C": 2529,
    "D": "040100",
    "E": "Yura"
},
{
    "A": "040129",
    "B": "JOSE LUIS BUSTAMANTE Y RIVERO",
    "C": 2363,
    "D": "040100",
    "E": "Jose Luis Bustamante Y Rivero"
},
{
    "A": "040201",
    "B": "CAMANA",
    "C": 15,
    "D": "040200",
    "E": "Camana"
},
{
    "A": "040202",
    "B": "JOSE MARIA QUIMPER",
    "C": 25,
    "D": "040200",
    "E": "Jose Maria Quimper"
},
{
    "A": "040203",
    "B": "MARIANO NICOLAS VALCARCEL",
    "C": 348,
    "D": "040200",
    "E": "Mariano Nicolas Valcarcel"
},
{
    "A": "040204",
    "B": "MARISCAL CACERES",
    "C": 16,
    "D": "040200",
    "E": "Mariscal Caceres"
},
{
    "A": "040205",
    "B": "NICOLAS DE PIEROLA",
    "C": 72,
    "D": "040200",
    "E": "Nicolas De Pierola"
},
{
    "A": "040206",
    "B": "OCOÑA",
    "C": 12,
    "D": "040200",
    "E": "Ocoña"
},
{
    "A": "040207",
    "B": "QUILCA",
    "C": 81,
    "D": "040200",
    "E": "Quilca"
},
{
    "A": "040208",
    "B": "SAMUEL PASTOR",
    "C": 22,
    "D": "040200",
    "E": "Samuel Pastor"
},
{
    "A": "040301",
    "B": "CARAVELI",
    "C": 1776,
    "D": "040300",
    "E": "Caraveli"
},
{
    "A": "040302",
    "B": "ACARI",
    "C": 162,
    "D": "040300",
    "E": "Acari"
},
{
    "A": "040303",
    "B": "ATICO",
    "C": 137,
    "D": "040300",
    "E": "Atico"
},
{
    "A": "040304",
    "B": "ATIQUIPA",
    "C": 345,
    "D": "040300",
    "E": "Atiquipa"
},
{
    "A": "040305",
    "B": "BELLA UNION",
    "C": 217,
    "D": "040300",
    "E": "Bella Union"
},
{
    "A": "040306",
    "B": "CAHUACHO",
    "C": 3396,
    "D": "040300",
    "E": "Cahuacho"
},
{
    "A": "040307",
    "B": "CHALA",
    "C": 12,
    "D": "040300",
    "E": "Chala"
},
{
    "A": "040308",
    "B": "CHAPARRA",
    "C": 596,
    "D": "040300",
    "E": "Chaparra"
},
{
    "A": "040309",
    "B": "HUANUHUANU",
    "C": 941,
    "D": "040300",
    "E": "Huanuhuanu"
},
{
    "A": "040310",
    "B": "JAQUI",
    "C": 272,
    "D": "040300",
    "E": "Jaqui"
},
{
    "A": "040311",
    "B": "LOMAS",
    "C": 8,
    "D": "040300",
    "E": "Lomas"
},
{
    "A": "040312",
    "B": "QUICACHA",
    "C": 1815,
    "D": "040300",
    "E": "Quicacha"
},
{
    "A": "040313",
    "B": "YAUCA",
    "C": 34,
    "D": "040300",
    "E": "Yauca"
},
{
    "A": "040401",
    "B": "APLAO",
    "C": 631,
    "D": "040400",
    "E": "Aplao"
},
{
    "A": "040402",
    "B": "ANDAGUA",
    "C": 3574,
    "D": "040400",
    "E": "Andagua"
},
{
    "A": "040403",
    "B": "AYO",
    "C": 1982,
    "D": "040400",
    "E": "Ayo"
},
{
    "A": "040404",
    "B": "CHACHAS",
    "C": 3059,
    "D": "040400",
    "E": "Chachas"
},
{
    "A": "040405",
    "B": "CHILCAYMARCA",
    "C": 3892,
    "D": "040400",
    "E": "Chilcaymarca"
},
{
    "A": "040406",
    "B": "CHOCO",
    "C": 2500,
    "D": "040400",
    "E": "Choco"
},
{
    "A": "040407",
    "B": "HUANCARQUI",
    "C": 599,
    "D": "040400",
    "E": "Huancarqui"
},
{
    "A": "040408",
    "B": "MACHAGUAY",
    "C": 3143,
    "D": "040400",
    "E": "Machaguay"
},
{
    "A": "040409",
    "B": "ORCOPAMPA",
    "C": 3796,
    "D": "040400",
    "E": "Orcopampa"
},
{
    "A": "040410",
    "B": "PAMPACOLCA",
    "C": 2916,
    "D": "040400",
    "E": "Pampacolca"
},
{
    "A": "040411",
    "B": "TIPAN",
    "C": 2086,
    "D": "040400",
    "E": "Tipan"
},
{
    "A": "040412",
    "B": "UÑON",
    "C": 2730,
    "D": "040400",
    "E": "Uñon"
},
{
    "A": "040413",
    "B": "URACA",
    "C": 433,
    "D": "040400",
    "E": "Uraca"
},
{
    "A": "040414",
    "B": "VIRACO",
    "C": 3220,
    "D": "040400",
    "E": "Viraco"
},
{
    "A": "040501",
    "B": "CHIVAY",
    "C": 3632,
    "D": "040500",
    "E": "Chivay"
},
{
    "A": "040502",
    "B": "ACHOMA",
    "C": 3487,
    "D": "040500",
    "E": "Achoma"
},
{
    "A": "040503",
    "B": "CABANACONDE",
    "C": 3296,
    "D": "040500",
    "E": "Cabanaconde"
},
{
    "A": "040504",
    "B": "CALLALLI",
    "C": 3862,
    "D": "040500",
    "E": "Callalli"
},
{
    "A": "040505",
    "B": "CAYLLOMA",
    "C": 4332,
    "D": "040500",
    "E": "Caylloma"
},
{
    "A": "040506",
    "B": "COPORAQUE",
    "C": 3583,
    "D": "040500",
    "E": "Coporaque"
},
{
    "A": "040507",
    "B": "HUAMBO",
    "C": 3308,
    "D": "040500",
    "E": "Huambo"
},
{
    "A": "040508",
    "B": "HUANCA",
    "C": 3078,
    "D": "040500",
    "E": "Huanca"
},
{
    "A": "040509",
    "B": "ICHUPAMPA",
    "C": 3397,
    "D": "040500",
    "E": "Ichupampa"
},
{
    "A": "040510",
    "B": "LARI",
    "C": 3358,
    "D": "040500",
    "E": "Lari"
},
{
    "A": "040511",
    "B": "LLUTA",
    "C": 2999,
    "D": "040500",
    "E": "Lluta"
},
{
    "A": "040512",
    "B": "MACA",
    "C": 3279,
    "D": "040500",
    "E": "Maca"
},
{
    "A": "040513",
    "B": "MADRIGAL",
    "C": 3271,
    "D": "040500",
    "E": "Madrigal"
},
{
    "A": "040514",
    "B": "SAN ANTONIO DE CHUCA",
    "C": 4457,
    "D": "040500",
    "E": "San Antonio De Chuca"
},
{
    "A": "040515",
    "B": "SIBAYO",
    "C": 3855,
    "D": "040500",
    "E": "Sibayo"
},
{
    "A": "040516",
    "B": "TAPAY",
    "C": 2984,
    "D": "040500",
    "E": "Tapay"
},
{
    "A": "040517",
    "B": "TISCO",
    "C": 4211,
    "D": "040500",
    "E": "Tisco"
},
{
    "A": "040518",
    "B": "TUTI",
    "C": 3837,
    "D": "040500",
    "E": "Tuti"
},
{
    "A": "040519",
    "B": "YANQUE",
    "C": 3420,
    "D": "040500",
    "E": "Yanque"
},
{
    "A": "040520",
    "B": "MAJES",
    "C": 1410,
    "D": "040500",
    "E": "Majes"
},
{
    "A": "040601",
    "B": "CHUQUIBAMBA",
    "C": 2935,
    "D": "040600",
    "E": "Chuquibamba"
},
{
    "A": "040602",
    "B": "ANDARAY",
    "C": 3028,
    "D": "040600",
    "E": "Andaray"
},
{
    "A": "040603",
    "B": "CAYARANI",
    "C": 3924,
    "D": "040600",
    "E": "Cayarani"
},
{
    "A": "040604",
    "B": "CHICHAS",
    "C": 2146,
    "D": "040600",
    "E": "Chichas"
},
{
    "A": "040605",
    "B": "IRAY",
    "C": 2396,
    "D": "040600",
    "E": "Iray"
},
{
    "A": "040606",
    "B": "RIO GRANDE",
    "C": 470,
    "D": "040600",
    "E": "Rio Grande"
},
{
    "A": "040607",
    "B": "SALAMANCA",
    "C": 3207,
    "D": "040600",
    "E": "Salamanca"
},
{
    "A": "040608",
    "B": "YANAQUIHUA",
    "C": 3008,
    "D": "040600",
    "E": "Yanaquihua"
},
{
    "A": "040701",
    "B": "MOLLENDO",
    "C": 52,
    "D": "040700",
    "E": "Mollendo"
},
{
    "A": "040702",
    "B": "COCACHACRA",
    "C": 84,
    "D": "040700",
    "E": "Cocachacra"
},
{
    "A": "040703",
    "B": "DEAN VALDIVIA",
    "C": 23,
    "D": "040700",
    "E": "Dean Valdivia"
},
{
    "A": "040704",
    "B": "ISLAY",
    "C": 85,
    "D": "040700",
    "E": "Islay"
},
{
    "A": "040705",
    "B": "MEJIA",
    "C": 13,
    "D": "040700",
    "E": "Mejia"
},
{
    "A": "040706",
    "B": "PUNTA DE BOMBON",
    "C": 23,
    "D": "040700",
    "E": "Punta De Bombon"
},
{
    "A": "040801",
    "B": "COTAHUASI",
    "C": 2675,
    "D": "040800",
    "E": "Cotahuasi"
},
{
    "A": "040802",
    "B": "ALCA",
    "C": 2726,
    "D": "040800",
    "E": "Alca"
},
{
    "A": "040803",
    "B": "CHARCANA",
    "C": 3402,
    "D": "040800",
    "E": "Charcana"
},
{
    "A": "040804",
    "B": "HUAYNACOTAS",
    "C": 2597,
    "D": "040800",
    "E": "Huaynacotas"
},
{
    "A": "040805",
    "B": "PAMPAMARCA",
    "C": 2567,
    "D": "040800",
    "E": "Pampamarca"
},
{
    "A": "040806",
    "B": "PUYCA",
    "C": 3674,
    "D": "040800",
    "E": "Puyca"
},
{
    "A": "040807",
    "B": "QUECHUALLA",
    "C": 1909,
    "D": "040800",
    "E": "Quechualla"
},
{
    "A": "040808",
    "B": "SAYLA",
    "C": 3531,
    "D": "040800",
    "E": "Sayla"
},
{
    "A": "040809",
    "B": "TAURIA",
    "C": 2857,
    "D": "040800",
    "E": "Tauria"
},
{
    "A": "040810",
    "B": "TOMEPAMPA",
    "C": 2621,
    "D": "040800",
    "E": "Tomepampa"
},
{
    "A": "040811",
    "B": "TORO",
    "C": 2957,
    "D": "040800",
    "E": "Toro"
},
{
    "A": "050101",
    "B": "AYACUCHO",
    "C": 2760,
    "D": "050100",
    "E": "Ayacucho"
},
{
    "A": "050102",
    "B": "ACOCRO",
    "C": 3246,
    "D": "050100",
    "E": "Acocro"
},
{
    "A": "050103",
    "B": "ACOS VINCHOS",
    "C": 2839,
    "D": "050100",
    "E": "Acos Vinchos"
},
{
    "A": "050104",
    "B": "CARMEN ALTO",
    "C": 2800,
    "D": "050100",
    "E": "Carmen Alto"
},
{
    "A": "050105",
    "B": "CHIARA",
    "C": 3515,
    "D": "050100",
    "E": "Chiara"
},
{
    "A": "050106",
    "B": "OCROS",
    "C": 3146,
    "D": "050100",
    "E": "Ocros"
},
{
    "A": "050107",
    "B": "PACAYCASA",
    "C": 2557,
    "D": "050100",
    "E": "Pacaycasa"
},
{
    "A": "050108",
    "B": "QUINUA",
    "C": 3275,
    "D": "050100",
    "E": "Quinua"
},
{
    "A": "050109",
    "B": "SAN JOSE DE TICLLAS",
    "C": 3267,
    "D": "050100",
    "E": "San Jose De Ticllas"
},
{
    "A": "050110",
    "B": "SAN JUAN BAUTISTA",
    "C": 2734,
    "D": "050100",
    "E": "San Juan Bautista"
},
{
    "A": "050111",
    "B": "SANTIAGO DE PISCHA",
    "C": 3176,
    "D": "050100",
    "E": "Santiago De Pischa"
},
{
    "A": "050112",
    "B": "SOCOS",
    "C": 3394,
    "D": "050100",
    "E": "Socos"
},
{
    "A": "050113",
    "B": "TAMBILLO",
    "C": 3064,
    "D": "050100",
    "E": "Tambillo"
},
{
    "A": "050114",
    "B": "VINCHOS",
    "C": 3129,
    "D": "050100",
    "E": "Vinchos"
},
{
    "A": "050115",
    "B": "JESUS NAZARENO",
    "C": 2734,
    "D": "050100",
    "E": "Jesus Nazareno"
},
{
    "A": "050116",
    "B": "ANDRES AVELINO CACERES DORREGARAY",
    "C": 2734,
    "D": "050100",
    "E": "Andres Avelino Caceres Dorregaray"
},
{
    "A": "050201",
    "B": "CANGALLO",
    "C": 2570,
    "D": "050200",
    "E": "Cangallo"
},
{
    "A": "050202",
    "B": "CHUSCHI",
    "C": 3148,
    "D": "050200",
    "E": "Chuschi"
},
{
    "A": "050203",
    "B": "LOS MOROCHUCOS",
    "C": 3327,
    "D": "050200",
    "E": "Los Morochucos"
},
{
    "A": "050204",
    "B": "MARIA PARADO DE BELLIDO",
    "C": 3246,
    "D": "050200",
    "E": "Maria Parado De Bellido"
},
{
    "A": "050205",
    "B": "PARAS",
    "C": 3342,
    "D": "050200",
    "E": "Paras"
},
{
    "A": "050206",
    "B": "TOTOS",
    "C": 3318,
    "D": "050200",
    "E": "Totos"
},
{
    "A": "050301",
    "B": "SANCOS",
    "C": 3422,
    "D": "050300",
    "E": "Sancos"
},
{
    "A": "050302",
    "B": "CARAPO",
    "C": 3230,
    "D": "050300",
    "E": "Carapo"
},
{
    "A": "050303",
    "B": "SACSAMARCA",
    "C": 3361,
    "D": "050300",
    "E": "Sacsamarca"
},
{
    "A": "050304",
    "B": "SANTIAGO DE LUCANAMARCA",
    "C": 3460,
    "D": "050300",
    "E": "Santiago De Lucanamarca"
},
{
    "A": "050401",
    "B": "HUANTA",
    "C": 2642,
    "D": "050400",
    "E": "Huanta"
},
{
    "A": "050402",
    "B": "AYAHUANCO",
    "C": 2363,
    "D": "050400",
    "E": "Ayahuanco"
},
{
    "A": "050403",
    "B": "HUAMANGUILLA",
    "C": 3277,
    "D": "050400",
    "E": "Huamanguilla"
},
{
    "A": "050404",
    "B": "IGUAIN",
    "C": 3026,
    "D": "050400",
    "E": "Iguain"
},
{
    "A": "050405",
    "B": "LURICOCHA",
    "C": 2611,
    "D": "050400",
    "E": "Luricocha"
},
{
    "A": "050406",
    "B": "SANTILLANA",
    "C": 3239,
    "D": "050400",
    "E": "Santillana"
},
{
    "A": "050407",
    "B": "SIVIA",
    "C": 551,
    "D": "050400",
    "E": "Sivia"
},
{
    "A": "050408",
    "B": "LLOCHEGUA",
    "C": 525,
    "D": "050400",
    "E": "Llochegua"
},
{
    "A": "050409",
    "B": "CANAYRE",
    "C": 507,
    "D": "050400",
    "E": "Canayre"
},
{
    "A": "050410",
    "B": "UCHURACCAY",
    "C": 4010,
    "D": "050400",
    "E": "Uchuraccay"
},
{
    "A": "050501",
    "B": "SAN MIGUEL",
    "C": 2647,
    "D": "050500",
    "E": "San Miguel"
},
{
    "A": "050502",
    "B": "ANCO",
    "C": 3155,
    "D": "050500",
    "E": "Anco"
},
{
    "A": "050503",
    "B": "AYNA",
    "C": 620,
    "D": "050500",
    "E": "Ayna"
},
{
    "A": "050504",
    "B": "CHILCAS",
    "C": 3248,
    "D": "050500",
    "E": "Chilcas"
},
{
    "A": "050505",
    "B": "CHUNGUI",
    "C": 3572,
    "D": "050500",
    "E": "Chungui"
},
{
    "A": "050506",
    "B": "LUIS CARRANZA",
    "C": 2926,
    "D": "050500",
    "E": "Luis Carranza"
},
{
    "A": "050507",
    "B": "SANTA ROSA",
    "C": 708,
    "D": "050500",
    "E": "Santa Rosa"
},
{
    "A": "050508",
    "B": "TAMBO",
    "C": 3219,
    "D": "050500",
    "E": "Tambo"
},
{
    "A": "050509",
    "B": "SAMUGARI",
    "C": 810,
    "D": "050500",
    "E": "Samugari"
},
{
    "A": "050510",
    "B": "ANCHIHUAY",
    "C": 752,
    "D": "050500",
    "E": "Anchihuay"
},
{
    "A": "050601",
    "B": "PUQUIO",
    "C": 3221,
    "D": "050600",
    "E": "Puquio"
},
{
    "A": "050602",
    "B": "AUCARA",
    "C": 3228,
    "D": "050600",
    "E": "Aucara"
},
{
    "A": "050603",
    "B": "CABANA",
    "C": 3288,
    "D": "050600",
    "E": "Cabana"
},
{
    "A": "050604",
    "B": "CARMEN SALCEDO",
    "C": 3459,
    "D": "050600",
    "E": "Carmen Salcedo"
},
{
    "A": "050605",
    "B": "CHAVIÑA",
    "C": 3316,
    "D": "050600",
    "E": "Chaviña"
},
{
    "A": "050606",
    "B": "CHIPAO",
    "C": 3425,
    "D": "050600",
    "E": "Chipao"
},
{
    "A": "050607",
    "B": "HUAC-HUAS",
    "C": 3170,
    "D": "050600",
    "E": "Huac-Huas"
},
{
    "A": "050608",
    "B": "LARAMATE",
    "C": 3040,
    "D": "050600",
    "E": "Laramate"
},
{
    "A": "050609",
    "B": "LEONCIO PRADO",
    "C": 2697,
    "D": "050600",
    "E": "Leoncio Prado"
},
{
    "A": "050610",
    "B": "LLAUTA",
    "C": 2669,
    "D": "050600",
    "E": "Llauta"
},
{
    "A": "050611",
    "B": "LUCANAS",
    "C": 3363,
    "D": "050600",
    "E": "Lucanas"
},
{
    "A": "050612",
    "B": "OCAÑA",
    "C": 2629,
    "D": "050600",
    "E": "Ocaña"
},
{
    "A": "050613",
    "B": "OTOCA",
    "C": 1950,
    "D": "050600",
    "E": "Otoca"
},
{
    "A": "050614",
    "B": "SAISA",
    "C": 3022,
    "D": "050600",
    "E": "Saisa"
},
{
    "A": "050615",
    "B": "SAN CRISTOBAL",
    "C": 3555,
    "D": "050600",
    "E": "San Cristobal"
},
{
    "A": "050616",
    "B": "SAN JUAN",
    "C": 2898,
    "D": "050600",
    "E": "San Juan"
},
{
    "A": "050617",
    "B": "SAN PEDRO",
    "C": 3139,
    "D": "050600",
    "E": "San Pedro"
},
{
    "A": "050618",
    "B": "SAN PEDRO DE PALCO",
    "C": 2464,
    "D": "050600",
    "E": "San Pedro De Palco"
},
{
    "A": "050619",
    "B": "SANCOS",
    "C": 2810,
    "D": "050600",
    "E": "Sancos"
},
{
    "A": "050620",
    "B": "SANTA ANA DE HUAYCAHUACHO",
    "C": 2962,
    "D": "050600",
    "E": "Santa Ana De Huaycahuacho"
},
{
    "A": "050621",
    "B": "SANTA LUCIA",
    "C": 2247,
    "D": "050600",
    "E": "Santa Lucia"
},
{
    "A": "050701",
    "B": "CORACORA",
    "C": 3178,
    "D": "050700",
    "E": "Coracora"
},
{
    "A": "050702",
    "B": "CHUMPI",
    "C": 3207,
    "D": "050700",
    "E": "Chumpi"
},
{
    "A": "050703",
    "B": "CORONEL CASTAÑEDA",
    "C": 3578,
    "D": "050700",
    "E": "Coronel Castañeda"
},
{
    "A": "050704",
    "B": "PACAPAUSA",
    "C": 2789,
    "D": "050700",
    "E": "Pacapausa"
},
{
    "A": "050705",
    "B": "PULLO",
    "C": 2996,
    "D": "050700",
    "E": "Pullo"
},
{
    "A": "050706",
    "B": "PUYUSCA",
    "C": 3294,
    "D": "050700",
    "E": "Puyusca"
},
{
    "A": "050707",
    "B": "SAN FRANCISCO DE RAVACAYCO",
    "C": 2904,
    "D": "050700",
    "E": "San Francisco De Ravacayco"
},
{
    "A": "050708",
    "B": "UPAHUACHO",
    "C": 3325,
    "D": "050700",
    "E": "Upahuacho"
},
{
    "A": "050801",
    "B": "PAUSA",
    "C": 2518,
    "D": "050800",
    "E": "Pausa"
},
{
    "A": "050802",
    "B": "COLTA",
    "C": 3237,
    "D": "050800",
    "E": "Colta"
},
{
    "A": "050803",
    "B": "CORCULLA",
    "C": 3467,
    "D": "050800",
    "E": "Corculla"
},
{
    "A": "050804",
    "B": "LAMPA",
    "C": 2793,
    "D": "050800",
    "E": "Lampa"
},
{
    "A": "050805",
    "B": "MARCABAMBA",
    "C": 2648,
    "D": "050800",
    "E": "Marcabamba"
},
{
    "A": "050806",
    "B": "OYOLO",
    "C": 3423,
    "D": "050800",
    "E": "Oyolo"
},
{
    "A": "050807",
    "B": "PARARCA",
    "C": 3030,
    "D": "050800",
    "E": "Pararca"
},
{
    "A": "050808",
    "B": "SAN JAVIER DE ALPABAMBA",
    "C": 2630,
    "D": "050800",
    "E": "San Javier De Alpabamba"
},
{
    "A": "050809",
    "B": "SAN JOSE DE USHUA",
    "C": 3003,
    "D": "050800",
    "E": "San Jose De Ushua"
},
{
    "A": "050810",
    "B": "SARA SARA",
    "C": 3284,
    "D": "050800",
    "E": "Sara Sara"
},
{
    "A": "050901",
    "B": "QUEROBAMBA",
    "C": 3508,
    "D": "050900",
    "E": "Querobamba"
},
{
    "A": "050902",
    "B": "BELEN",
    "C": 3239,
    "D": "050900",
    "E": "Belen"
},
{
    "A": "050903",
    "B": "CHALCOS",
    "C": 3649,
    "D": "050900",
    "E": "Chalcos"
},
{
    "A": "050904",
    "B": "CHILCAYOC",
    "C": 3410,
    "D": "050900",
    "E": "Chilcayoc"
},
{
    "A": "050905",
    "B": "HUACAÑA",
    "C": 3187,
    "D": "050900",
    "E": "Huacaña"
},
{
    "A": "050906",
    "B": "MORCOLLA",
    "C": 3452,
    "D": "050900",
    "E": "Morcolla"
},
{
    "A": "050907",
    "B": "PAICO",
    "C": 3117,
    "D": "050900",
    "E": "Paico"
},
{
    "A": "050908",
    "B": "SAN PEDRO DE LARCAY",
    "C": 3493,
    "D": "050900",
    "E": "San Pedro De Larcay"
},
{
    "A": "050909",
    "B": "SAN SALVADOR DE QUIJE",
    "C": 3122,
    "D": "050900",
    "E": "San Salvador De Quije"
},
{
    "A": "050910",
    "B": "SANTIAGO DE PAUCARAY",
    "C": 3259,
    "D": "050900",
    "E": "Santiago De Paucaray"
},
{
    "A": "050911",
    "B": "SORAS",
    "C": 3411,
    "D": "050900",
    "E": "Soras"
},
{
    "A": "051001",
    "B": "HUANCAPI",
    "C": 3102,
    "D": "051000",
    "E": "Huancapi"
},
{
    "A": "051002",
    "B": "ALCAMENCA",
    "C": 3219,
    "D": "051000",
    "E": "Alcamenca"
},
{
    "A": "051003",
    "B": "APONGO",
    "C": 3166,
    "D": "051000",
    "E": "Apongo"
},
{
    "A": "051004",
    "B": "ASQUIPATA",
    "C": 3370,
    "D": "051000",
    "E": "Asquipata"
},
{
    "A": "051005",
    "B": "CANARIA",
    "C": 3031,
    "D": "051000",
    "E": "Canaria"
},
{
    "A": "051006",
    "B": "CAYARA",
    "C": 3224,
    "D": "051000",
    "E": "Cayara"
},
{
    "A": "051007",
    "B": "COLCA",
    "C": 2992,
    "D": "051000",
    "E": "Colca"
},
{
    "A": "051008",
    "B": "HUAMANQUIQUIA",
    "C": 3349,
    "D": "051000",
    "E": "Huamanquiquia"
},
{
    "A": "051009",
    "B": "HUANCARAYLLA",
    "C": 3211,
    "D": "051000",
    "E": "Huancaraylla"
},
{
    "A": "051010",
    "B": "HUAYA",
    "C": 3431,
    "D": "051000",
    "E": "Huaya"
},
{
    "A": "051011",
    "B": "SARHUA",
    "C": 3183,
    "D": "051000",
    "E": "Sarhua"
},
{
    "A": "051012",
    "B": "VILCANCHOS",
    "C": 3064,
    "D": "051000",
    "E": "Vilcanchos"
},
{
    "A": "051101",
    "B": "VILCAS HUAMAN",
    "C": 3482,
    "D": "051100",
    "E": "Vilcas Huaman"
},
{
    "A": "051102",
    "B": "ACCOMARCA",
    "C": 3380,
    "D": "051100",
    "E": "Accomarca"
},
{
    "A": "051103",
    "B": "CARHUANCA",
    "C": 2975,
    "D": "051100",
    "E": "Carhuanca"
},
{
    "A": "051104",
    "B": "CONCEPCION",
    "C": 3067,
    "D": "051100",
    "E": "Concepcion"
},
{
    "A": "051105",
    "B": "HUAMBALPA",
    "C": 3217,
    "D": "051100",
    "E": "Huambalpa"
},
{
    "A": "051106",
    "B": "INDEPENDENCIA",
    "C": 3208,
    "D": "051100",
    "E": "Independencia"
},
{
    "A": "051107",
    "B": "SAURAMA",
    "C": 3601,
    "D": "051100",
    "E": "Saurama"
},
{
    "A": "051108",
    "B": "VISCHONGO",
    "C": 3113,
    "D": "051100",
    "E": "Vischongo"
},
{
    "A": "060101",
    "B": "CAJAMARCA",
    "C": 2719,
    "D": "060100",
    "E": "Cajamarca"
},
{
    "A": "060102",
    "B": "ASUNCION",
    "C": 2206,
    "D": "060100",
    "E": "Asuncion"
},
{
    "A": "060103",
    "B": "CHETILLA",
    "C": 2776,
    "D": "060100",
    "E": "Chetilla"
},
{
    "A": "060104",
    "B": "COSPAN",
    "C": 2463,
    "D": "060100",
    "E": "Cospan"
},
{
    "A": "060105",
    "B": "ENCAÑADA",
    "C": 3123,
    "D": "060100",
    "E": "Encañada"
},
{
    "A": "060106",
    "B": "JESUS",
    "C": 2531,
    "D": "060100",
    "E": "Jesus"
},
{
    "A": "060107",
    "B": "LLACANORA",
    "C": 2644,
    "D": "060100",
    "E": "Llacanora"
},
{
    "A": "060108",
    "B": "LOS BAÑOS DEL INCA",
    "C": 2665,
    "D": "060100",
    "E": "Los Baños Del Inca"
},
{
    "A": "060109",
    "B": "MAGDALENA",
    "C": 1308,
    "D": "060100",
    "E": "Magdalena"
},
{
    "A": "060110",
    "B": "MATARA",
    "C": 2895,
    "D": "060100",
    "E": "Matara"
},
{
    "A": "060111",
    "B": "NAMORA",
    "C": 2743,
    "D": "060100",
    "E": "Namora"
},
{
    "A": "060112",
    "B": "SAN JUAN",
    "C": 2297,
    "D": "060100",
    "E": "San Juan"
},
{
    "A": "060201",
    "B": "CAJABAMBA",
    "C": 2651,
    "D": "060200",
    "E": "Cajabamba"
},
{
    "A": "060202",
    "B": "CACHACHI",
    "C": 3275,
    "D": "060200",
    "E": "Cachachi"
},
{
    "A": "060203",
    "B": "CONDEBAMBA",
    "C": 2807,
    "D": "060200",
    "E": "Condebamba"
},
{
    "A": "060204",
    "B": "SITACOCHA",
    "C": 3218,
    "D": "060200",
    "E": "Sitacocha"
},
{
    "A": "060301",
    "B": "CELENDIN",
    "C": 2645,
    "D": "060300",
    "E": "Celendin"
},
{
    "A": "060302",
    "B": "CHUMUCH",
    "C": 2281,
    "D": "060300",
    "E": "Chumuch"
},
{
    "A": "060303",
    "B": "CORTEGANA",
    "C": 1667,
    "D": "060300",
    "E": "Cortegana"
},
{
    "A": "060304",
    "B": "HUASMIN",
    "C": 2560,
    "D": "060300",
    "E": "Huasmin"
},
{
    "A": "060305",
    "B": "JORGE CHAVEZ",
    "C": 2630,
    "D": "060300",
    "E": "Jorge Chavez"
},
{
    "A": "060306",
    "B": "JOSE GALVEZ",
    "C": 2601,
    "D": "060300",
    "E": "Jose Galvez"
},
{
    "A": "060307",
    "B": "MIGUEL IGLESIAS",
    "C": 3025,
    "D": "060300",
    "E": "Miguel Iglesias"
},
{
    "A": "060308",
    "B": "OXAMARCA",
    "C": 2903,
    "D": "060300",
    "E": "Oxamarca"
},
{
    "A": "060309",
    "B": "SOROCHUCO",
    "C": 2674,
    "D": "060300",
    "E": "Sorochuco"
},
{
    "A": "060310",
    "B": "SUCRE",
    "C": 2611,
    "D": "060300",
    "E": "Sucre"
},
{
    "A": "060311",
    "B": "UTCO",
    "C": 2196,
    "D": "060300",
    "E": "Utco"
},
{
    "A": "060312",
    "B": "LA LIBERTAD DE PALLAN",
    "C": 2985,
    "D": "060300",
    "E": "La Libertad De Pallan"
},
{
    "A": "060401",
    "B": "CHOTA",
    "C": 2387,
    "D": "060400",
    "E": "Chota"
},
{
    "A": "060402",
    "B": "ANGUIA",
    "C": 2450,
    "D": "060400",
    "E": "Anguia"
},
{
    "A": "060403",
    "B": "CHADIN",
    "C": 2385,
    "D": "060400",
    "E": "Chadin"
},
{
    "A": "060404",
    "B": "CHIGUIRIP",
    "C": 2612,
    "D": "060400",
    "E": "Chiguirip"
},
{
    "A": "060405",
    "B": "CHIMBAN",
    "C": 1611,
    "D": "060400",
    "E": "Chimban"
},
{
    "A": "060406",
    "B": "CHOROPAMPA",
    "C": 2820,
    "D": "060400",
    "E": "Choropampa"
},
{
    "A": "060407",
    "B": "COCHABAMBA",
    "C": 1694,
    "D": "060400",
    "E": "Cochabamba"
},
{
    "A": "060408",
    "B": "CONCHAN",
    "C": 2315,
    "D": "060400",
    "E": "Conchan"
},
{
    "A": "060409",
    "B": "HUAMBOS",
    "C": 2273,
    "D": "060400",
    "E": "Huambos"
},
{
    "A": "060410",
    "B": "LAJAS",
    "C": 2137,
    "D": "060400",
    "E": "Lajas"
},
{
    "A": "060411",
    "B": "LLAMA",
    "C": 2096,
    "D": "060400",
    "E": "Llama"
},
{
    "A": "060412",
    "B": "MIRACOSTA",
    "C": 2976,
    "D": "060400",
    "E": "Miracosta"
},
{
    "A": "060413",
    "B": "PACCHA",
    "C": 2120,
    "D": "060400",
    "E": "Paccha"
},
{
    "A": "060414",
    "B": "PION",
    "C": 1820,
    "D": "060400",
    "E": "Pion"
},
{
    "A": "060415",
    "B": "QUEROCOTO",
    "C": 2426,
    "D": "060400",
    "E": "Querocoto"
},
{
    "A": "060416",
    "B": "SAN JUAN DE LICUPIS",
    "C": 3012,
    "D": "060400",
    "E": "San Juan De Licupis"
},
{
    "A": "060417",
    "B": "TACABAMBA",
    "C": 2075,
    "D": "060400",
    "E": "Tacabamba"
},
{
    "A": "060418",
    "B": "TOCMOCHE",
    "C": 1286,
    "D": "060400",
    "E": "Tocmoche"
},
{
    "A": "060419",
    "B": "CHALAMARCA",
    "C": 2319,
    "D": "060400",
    "E": "Chalamarca"
},
{
    "A": "060501",
    "B": "CONTUMAZA",
    "C": 2647,
    "D": "060500",
    "E": "Contumaza"
},
{
    "A": "060502",
    "B": "CHILETE",
    "C": 847,
    "D": "060500",
    "E": "Chilete"
},
{
    "A": "060503",
    "B": "CUPISNIQUE",
    "C": 1881,
    "D": "060500",
    "E": "Cupisnique"
},
{
    "A": "060504",
    "B": "GUZMANGO",
    "C": 2502,
    "D": "060500",
    "E": "Guzmango"
},
{
    "A": "060505",
    "B": "SAN BENITO",
    "C": 1457,
    "D": "060500",
    "E": "San Benito"
},
{
    "A": "060506",
    "B": "SANTA CRUZ DE TOLED",
    "C": 2328,
    "D": "060500",
    "E": "Santa Cruz De Toled"
},
{
    "A": "060507",
    "B": "TANTARICA",
    "C": 2842,
    "D": "060500",
    "E": "Tantarica"
},
{
    "A": "060508",
    "B": "YONAN",
    "C": 436,
    "D": "060500",
    "E": "Yonan"
},
{
    "A": "060601",
    "B": "CUTERVO",
    "C": 2628,
    "D": "060600",
    "E": "Cutervo"
},
{
    "A": "060602",
    "B": "CALLAYUC",
    "C": 1330,
    "D": "060600",
    "E": "Callayuc"
},
{
    "A": "060603",
    "B": "CHOROS",
    "C": 481,
    "D": "060600",
    "E": "Choros"
},
{
    "A": "060604",
    "B": "CUJILLO",
    "C": 1585,
    "D": "060600",
    "E": "Cujillo"
},
{
    "A": "060605",
    "B": "LA RAMADA",
    "C": 1484,
    "D": "060600",
    "E": "La Ramada"
},
{
    "A": "060606",
    "B": "PIMPINGOS",
    "C": 1748,
    "D": "060600",
    "E": "Pimpingos"
},
{
    "A": "060607",
    "B": "QUEROCOTILLO",
    "C": 1982,
    "D": "060600",
    "E": "Querocotillo"
},
{
    "A": "060608",
    "B": "SAN ANDRES DE CUTERVO",
    "C": 2058,
    "D": "060600",
    "E": "San Andres De Cutervo"
},
{
    "A": "060609",
    "B": "SAN JUAN DE CUTERVO",
    "C": 1975,
    "D": "060600",
    "E": "San Juan De Cutervo"
},
{
    "A": "060610",
    "B": "SAN LUIS DE LUCMA",
    "C": 1786,
    "D": "060600",
    "E": "San Luis De Lucma"
},
{
    "A": "060611",
    "B": "SANTA CRUZ",
    "C": 1580,
    "D": "060600",
    "E": "Santa Cruz"
},
{
    "A": "060612",
    "B": "SANTO DOMINGO DE LA CAPILLA",
    "C": 1663,
    "D": "060600",
    "E": "Santo Domingo De La Capilla"
},
{
    "A": "060613",
    "B": "SANTO TOMAS",
    "C": 2061,
    "D": "060600",
    "E": "Santo Tomas"
},
{
    "A": "060614",
    "B": "SOCOTA",
    "C": 1822,
    "D": "060600",
    "E": "Socota"
},
{
    "A": "060615",
    "B": "TORIBIO CASANOVA",
    "C": 1497,
    "D": "060600",
    "E": "Toribio Casanova"
},
{
    "A": "060701",
    "B": "BAMBAMARCA",
    "C": 2580,
    "D": "060700",
    "E": "Bambamarca"
},
{
    "A": "060702",
    "B": "CHUGUR",
    "C": 2739,
    "D": "060700",
    "E": "Chugur"
},
{
    "A": "060703",
    "B": "HUALGAYOC",
    "C": 3716,
    "D": "060700",
    "E": "Hualgayoc"
},
{
    "A": "060801",
    "B": "JAEN",
    "C": 753,
    "D": "060800",
    "E": "Jaen"
},
{
    "A": "060802",
    "B": "BELLAVISTA",
    "C": 434,
    "D": "060800",
    "E": "Bellavista"
},
{
    "A": "060803",
    "B": "CHONTALI",
    "C": 1621,
    "D": "060800",
    "E": "Chontali"
},
{
    "A": "060804",
    "B": "COLASAY",
    "C": 1783,
    "D": "060800",
    "E": "Colasay"
},
{
    "A": "060805",
    "B": "HUABAL",
    "C": 1628,
    "D": "060800",
    "E": "Huabal"
},
{
    "A": "060806",
    "B": "LAS PIRIAS",
    "C": 1555,
    "D": "060800",
    "E": "Las Pirias"
},
{
    "A": "060807",
    "B": "POMAHUACA",
    "C": 1082,
    "D": "060800",
    "E": "Pomahuaca"
},
{
    "A": "060808",
    "B": "PUCARA",
    "C": 897,
    "D": "060800",
    "E": "Pucara"
},
{
    "A": "060809",
    "B": "SALLIQUE",
    "C": 1658,
    "D": "060800",
    "E": "Sallique"
},
{
    "A": "060810",
    "B": "SAN FELIPE",
    "C": 1852,
    "D": "060800",
    "E": "San Felipe"
},
{
    "A": "060811",
    "B": "SAN JOSE DEL ALTO",
    "C": 1872,
    "D": "060800",
    "E": "San Jose Del Alto"
},
{
    "A": "060812",
    "B": "SANTA ROSA",
    "C": 1199,
    "D": "060800",
    "E": "Santa Rosa"
},
{
    "A": "060901",
    "B": "SAN IGNACIO",
    "C": 1303,
    "D": "060900",
    "E": "San Ignacio"
},
{
    "A": "060902",
    "B": "CHIRINOS",
    "C": 1887,
    "D": "060900",
    "E": "Chirinos"
},
{
    "A": "060903",
    "B": "HUARANGO",
    "C": 785,
    "D": "060900",
    "E": "Huarango"
},
{
    "A": "060904",
    "B": "LA COIPA",
    "C": 1466,
    "D": "060900",
    "E": "La Coipa"
},
{
    "A": "060905",
    "B": "NAMBALLE",
    "C": 732,
    "D": "060900",
    "E": "Namballe"
},
{
    "A": "060906",
    "B": "SAN JOSE DE LOURDES",
    "C": 1092,
    "D": "060900",
    "E": "San Jose De Lourdes"
},
{
    "A": "060907",
    "B": "TABACONAS",
    "C": 1888,
    "D": "060900",
    "E": "Tabaconas"
},
{
    "A": "061001",
    "B": "PEDRO GALVEZ",
    "C": 2252,
    "D": "061000",
    "E": "Pedro Galvez"
},
{
    "A": "061002",
    "B": "CHANCAY",
    "C": 2668,
    "D": "061000",
    "E": "Chancay"
},
{
    "A": "061003",
    "B": "EDUARDO VILLANUEVA",
    "C": 2009,
    "D": "061000",
    "E": "Eduardo Villanueva"
},
{
    "A": "061004",
    "B": "GREGORIO PITA",
    "C": 2712,
    "D": "061000",
    "E": "Gregorio Pita"
},
{
    "A": "061005",
    "B": "ICHOCAN",
    "C": 2602,
    "D": "061000",
    "E": "Ichocan"
},
{
    "A": "061006",
    "B": "JOSE MANUEL QUIROZ",
    "C": 2823,
    "D": "061000",
    "E": "Jose Manuel Quiroz"
},
{
    "A": "061007",
    "B": "JOSE SABOGAL",
    "C": 2989,
    "D": "061000",
    "E": "Jose Sabogal"
},
{
    "A": "061101",
    "B": "SAN MIGUEL",
    "C": 2659,
    "D": "061100",
    "E": "San Miguel"
},
{
    "A": "061102",
    "B": "BOLIVAR",
    "C": 912,
    "D": "061100",
    "E": "Bolivar"
},
{
    "A": "061103",
    "B": "CALQUIS",
    "C": 2883,
    "D": "061100",
    "E": "Calquis"
},
{
    "A": "061104",
    "B": "CATILLUC",
    "C": 2793,
    "D": "061100",
    "E": "Catilluc"
},
{
    "A": "061105",
    "B": "EL PRADO",
    "C": 2839,
    "D": "061100",
    "E": "El Prado"
},
{
    "A": "061106",
    "B": "LA FLORIDA",
    "C": 987,
    "D": "061100",
    "E": "La Florida"
},
{
    "A": "061107",
    "B": "LLAPA",
    "C": 2956,
    "D": "061100",
    "E": "Llapa"
},
{
    "A": "061108",
    "B": "NANCHOC",
    "C": 399,
    "D": "061100",
    "E": "Nanchoc"
},
{
    "A": "061109",
    "B": "NIEPOS",
    "C": 2451,
    "D": "061100",
    "E": "Niepos"
},
{
    "A": "061110",
    "B": "SAN GREGORIO",
    "C": 1821,
    "D": "061100",
    "E": "San Gregorio"
},
{
    "A": "061111",
    "B": "SAN SILVESTRE DE COCHAN",
    "C": 2912,
    "D": "061100",
    "E": "San Silvestre De Cochan"
},
{
    "A": "061112",
    "B": "TONGOD",
    "C": 2683,
    "D": "061100",
    "E": "Tongod"
},
{
    "A": "061113",
    "B": "UNION AGUA BLANCA",
    "C": 2911,
    "D": "061100",
    "E": "Union Agua Blanca"
},
{
    "A": "061201",
    "B": "SAN PABLO",
    "C": 2381,
    "D": "061200",
    "E": "San Pablo"
},
{
    "A": "061202",
    "B": "SAN BERNARDINO",
    "C": 1376,
    "D": "061200",
    "E": "San Bernardino"
},
{
    "A": "061203",
    "B": "SAN LUIS",
    "C": 1334,
    "D": "061200",
    "E": "San Luis"
},
{
    "A": "061204",
    "B": "TUMBADEN",
    "C": 2940,
    "D": "061200",
    "E": "Tumbaden"
},
{
    "A": "061301",
    "B": "SANTA CRUZ",
    "C": 2034,
    "D": "061300",
    "E": "Santa Cruz"
},
{
    "A": "061302",
    "B": "ANDABAMBA",
    "C": 2538,
    "D": "061300",
    "E": "Andabamba"
},
{
    "A": "061303",
    "B": "CATACHE",
    "C": 1343,
    "D": "061300",
    "E": "Catache"
},
{
    "A": "061304",
    "B": "CHANCAYBAÑOS",
    "C": 1603,
    "D": "061300",
    "E": "Chancaybaños"
},
{
    "A": "061305",
    "B": "LA ESPERANZA",
    "C": 1713,
    "D": "061300",
    "E": "La Esperanza"
},
{
    "A": "061306",
    "B": "NINABAMBA",
    "C": 2159,
    "D": "061300",
    "E": "Ninabamba"
},
{
    "A": "061307",
    "B": "PULAN",
    "C": 2137,
    "D": "061300",
    "E": "Pulan"
},
{
    "A": "061308",
    "B": "SAUCEPAMPA",
    "C": 1881,
    "D": "061300",
    "E": "Saucepampa"
},
{
    "A": "061309",
    "B": "SEXI",
    "C": 2475,
    "D": "061300",
    "E": "Sexi"
},
{
    "A": "061310",
    "B": "UTICYACU",
    "C": 2283,
    "D": "061300",
    "E": "Uticyacu"
},
{
    "A": "061311",
    "B": "YAUYUCAN",
    "C": 2499,
    "D": "061300",
    "E": "Yauyucan"
},
{
    "A": "070101",
    "B": "CALLAO",
    "C": 3,
    "D": "070100",
    "E": "Callao"
},
{
    "A": "070102",
    "B": "BELLAVISTA",
    "C": 14,
    "D": "070100",
    "E": "Bellavista"
},
{
    "A": "070103",
    "B": "CARMEN DE LA LEGUA REYNOSO",
    "C": 51,
    "D": "070100",
    "E": "Carmen De La Legua Reynoso"
},
{
    "A": "070104",
    "B": "LA PERLA",
    "C": 24,
    "D": "070100",
    "E": "La Perla"
},
{
    "A": "070105",
    "B": "LA PUNTA",
    "C": 3,
    "D": "070100",
    "E": "La Punta"
},
{
    "A": "070106",
    "B": "VENTANILLA",
    "C": 55,
    "D": "070100",
    "E": "Ventanilla"
},
{
    "A": "070107",
    "B": "MI PERU",
    "C": 135,
    "D": "070100",
    "E": "Mi Peru"
},
{
    "A": "080101",
    "B": "CUSCO",
    "C": 3414,
    "D": "080100",
    "E": "Cusco"
},
{
    "A": "080102",
    "B": "CCORCA",
    "C": 3625,
    "D": "080100",
    "E": "Ccorca"
},
{
    "A": "080103",
    "B": "POROY",
    "C": 3499,
    "D": "080100",
    "E": "Poroy"
},
{
    "A": "080104",
    "B": "SAN JERONIMO",
    "C": 3245,
    "D": "080100",
    "E": "San Jeronimo"
},
{
    "A": "080105",
    "B": "SAN SEBASTIAN",
    "C": 3295,
    "D": "080100",
    "E": "San Sebastian"
},
{
    "A": "080106",
    "B": "SANTIAGO",
    "C": 3427,
    "D": "080100",
    "E": "Santiago"
},
{
    "A": "080107",
    "B": "SAYLLA",
    "C": 3150,
    "D": "080100",
    "E": "Saylla"
},
{
    "A": "080108",
    "B": "WANCHAQ",
    "C": 3363,
    "D": "080100",
    "E": "Wanchaq"
},
{
    "A": "080201",
    "B": "ACOMAYO",
    "C": 3221,
    "D": "080200",
    "E": "Acomayo"
},
{
    "A": "080202",
    "B": "ACOPIA",
    "C": 3713,
    "D": "080200",
    "E": "Acopia"
},
{
    "A": "080203",
    "B": "ACOS",
    "C": 3096,
    "D": "080200",
    "E": "Acos"
},
{
    "A": "080204",
    "B": "MOSOC LLACTA",
    "C": 3810,
    "D": "080200",
    "E": "Mosoc Llacta"
},
{
    "A": "080205",
    "B": "POMACANCHI",
    "C": 3693,
    "D": "080200",
    "E": "Pomacanchi"
},
{
    "A": "080206",
    "B": "RONDOCAN",
    "C": 3359,
    "D": "080200",
    "E": "Rondocan"
},
{
    "A": "080207",
    "B": "SANGARARA",
    "C": 3769,
    "D": "080200",
    "E": "Sangarara"
},
{
    "A": "080301",
    "B": "ANTA",
    "C": 3345,
    "D": "080300",
    "E": "Anta"
},
{
    "A": "080302",
    "B": "ANCAHUASI",
    "C": 3457,
    "D": "080300",
    "E": "Ancahuasi"
},
{
    "A": "080303",
    "B": "CACHIMAYO",
    "C": 3423,
    "D": "080300",
    "E": "Cachimayo"
},
{
    "A": "080304",
    "B": "CHINCHAYPUJIO",
    "C": 3075,
    "D": "080300",
    "E": "Chinchaypujio"
},
{
    "A": "080305",
    "B": "HUAROCONDO",
    "C": 3353,
    "D": "080300",
    "E": "Huarocondo"
},
{
    "A": "080306",
    "B": "LIMATAMBO",
    "C": 2577,
    "D": "080300",
    "E": "Limatambo"
},
{
    "A": "080307",
    "B": "MOLLEPATA",
    "C": 2976,
    "D": "080300",
    "E": "Mollepata"
},
{
    "A": "080308",
    "B": "PUCYURA",
    "C": 3383,
    "D": "080300",
    "E": "Pucyura"
},
{
    "A": "080309",
    "B": "ZURITE",
    "C": 3405,
    "D": "080300",
    "E": "Zurite"
},
{
    "A": "080401",
    "B": "CALCA",
    "C": 2925,
    "D": "080400",
    "E": "Calca"
},
{
    "A": "080402",
    "B": "COYA",
    "C": 2944,
    "D": "080400",
    "E": "Coya"
},
{
    "A": "080403",
    "B": "LAMAY",
    "C": 2934,
    "D": "080400",
    "E": "Lamay"
},
{
    "A": "080404",
    "B": "LARES",
    "C": 3171,
    "D": "080400",
    "E": "Lares"
},
{
    "A": "080405",
    "B": "PISAC",
    "C": 2974,
    "D": "080400",
    "E": "Pisac"
},
{
    "A": "080406",
    "B": "SAN SALVADOR",
    "C": 3020,
    "D": "080400",
    "E": "San Salvador"
},
{
    "A": "080407",
    "B": "TARAY",
    "C": 3024,
    "D": "080400",
    "E": "Taray"
},
{
    "A": "080408",
    "B": "YANATILE",
    "C": 1357,
    "D": "080400",
    "E": "Yanatile"
},
{
    "A": "080501",
    "B": "YANAOCA",
    "C": 3910,
    "D": "080500",
    "E": "Yanaoca"
},
{
    "A": "080502",
    "B": "CHECCA",
    "C": 3833,
    "D": "080500",
    "E": "Checca"
},
{
    "A": "080503",
    "B": "KUNTURKANKI",
    "C": 3941,
    "D": "080500",
    "E": "Kunturkanki"
},
{
    "A": "080504",
    "B": "LANGUI",
    "C": 3964,
    "D": "080500",
    "E": "Langui"
},
{
    "A": "080505",
    "B": "LAYO",
    "C": 3980,
    "D": "080500",
    "E": "Layo"
},
{
    "A": "080506",
    "B": "PAMPAMARCA",
    "C": 3792,
    "D": "080500",
    "E": "Pampamarca"
},
{
    "A": "080507",
    "B": "QUEHUE",
    "C": 3799,
    "D": "080500",
    "E": "Quehue"
},
{
    "A": "080508",
    "B": "TUPAC AMARU",
    "C": 3849,
    "D": "080500",
    "E": "Tupac Amaru"
},
{
    "A": "080601",
    "B": "SICUANI",
    "C": 3546,
    "D": "080600",
    "E": "Sicuani"
},
{
    "A": "080602",
    "B": "CHECACUPE",
    "C": 3441,
    "D": "080600",
    "E": "Checacupe"
},
{
    "A": "080603",
    "B": "COMBAPATA",
    "C": 3481,
    "D": "080600",
    "E": "Combapata"
},
{
    "A": "080604",
    "B": "MARANGANI",
    "C": 3698,
    "D": "080600",
    "E": "Marangani"
},
{
    "A": "080605",
    "B": "PITUMARCA",
    "C": 3570,
    "D": "080600",
    "E": "Pitumarca"
},
{
    "A": "080606",
    "B": "SAN PABLO",
    "C": 3486,
    "D": "080600",
    "E": "San Pablo"
},
{
    "A": "080607",
    "B": "SAN PEDRO",
    "C": 3489,
    "D": "080600",
    "E": "San Pedro"
},
{
    "A": "080608",
    "B": "TINTA",
    "C": 3484,
    "D": "080600",
    "E": "Tinta"
},
{
    "A": "080701",
    "B": "SANTO TOMAS",
    "C": 3678,
    "D": "080700",
    "E": "Santo Tomas"
},
{
    "A": "080702",
    "B": "CAPACMARCA",
    "C": 3584,
    "D": "080700",
    "E": "Capacmarca"
},
{
    "A": "080703",
    "B": "CHAMACA",
    "C": 3739,
    "D": "080700",
    "E": "Chamaca"
},
{
    "A": "080704",
    "B": "COLQUEMARCA",
    "C": 3592,
    "D": "080700",
    "E": "Colquemarca"
},
{
    "A": "080705",
    "B": "LIVITACA",
    "C": 3752,
    "D": "080700",
    "E": "Livitaca"
},
{
    "A": "080706",
    "B": "LLUSCO",
    "C": 3496,
    "D": "080700",
    "E": "Llusco"
},
{
    "A": "080707",
    "B": "QUIÑOTA",
    "C": 3593,
    "D": "080700",
    "E": "Quiñota"
},
{
    "A": "080708",
    "B": "VELILLE",
    "C": 3738,
    "D": "080700",
    "E": "Velille"
},
{
    "A": "080801",
    "B": "ESPINAR",
    "C": 3924,
    "D": "080800",
    "E": "Espinar"
},
{
    "A": "080802",
    "B": "CONDOROMA",
    "C": 4675,
    "D": "080800",
    "E": "Condoroma"
},
{
    "A": "080803",
    "B": "COPORAQUE",
    "C": 3941,
    "D": "080800",
    "E": "Coporaque"
},
{
    "A": "080804",
    "B": "OCORURO",
    "C": 4102,
    "D": "080800",
    "E": "Ocoruro"
},
{
    "A": "080805",
    "B": "PALLPATA",
    "C": 3998,
    "D": "080800",
    "E": "Pallpata"
},
{
    "A": "080806",
    "B": "PICHIGUA",
    "C": 3909,
    "D": "080800",
    "E": "Pichigua"
},
{
    "A": "080807",
    "B": "SUYCKUTAMBO",
    "C": 4801,
    "D": "080800",
    "E": "Suyckutambo"
},
{
    "A": "080808",
    "B": "ALTO PICHIGUA",
    "C": 4032,
    "D": "080800",
    "E": "Alto Pichigua"
},
{
    "A": "080901",
    "B": "SANTA ANA",
    "C": 1063,
    "D": "080900",
    "E": "Santa Ana"
},
{
    "A": "080902",
    "B": "ECHARATE",
    "C": 1162,
    "D": "080900",
    "E": "Echarate"
},
{
    "A": "080903",
    "B": "HUAYOPATA",
    "C": 1524,
    "D": "080900",
    "E": "Huayopata"
},
{
    "A": "080904",
    "B": "MARANURA",
    "C": 1110,
    "D": "080900",
    "E": "Maranura"
},
{
    "A": "080905",
    "B": "OCOBAMBA",
    "C": 1543,
    "D": "080900",
    "E": "Ocobamba"
},
{
    "A": "080906",
    "B": "QUELLOUNO",
    "C": 800,
    "D": "080900",
    "E": "Quellouno"
},
{
    "A": "080907",
    "B": "KIMBIRI",
    "C": 739,
    "D": "080900",
    "E": "Kimbiri"
},
{
    "A": "080908",
    "B": "SANTA TERESA",
    "C": 1811,
    "D": "080900",
    "E": "Santa Teresa"
},
{
    "A": "080909",
    "B": "VILCABAMBA",
    "C": 2764,
    "D": "080900",
    "E": "Vilcabamba"
},
{
    "A": "080910",
    "B": "PICHARI",
    "C": 614,
    "D": "080900",
    "E": "Pichari"
},
{
    "A": "080911",
    "B": "INKAWASI",
    "C": 3078,
    "D": "080900",
    "E": "Inkawasi"
},
{
    "A": "080912",
    "B": "VILLA VIRGEN",
    "C": 650,
    "D": "080900",
    "E": "Villa Virgen"
},
{
    "A": "081001",
    "B": "PARURO",
    "C": 3068,
    "D": "081000",
    "E": "Paruro"
},
{
    "A": "081002",
    "B": "ACCHA",
    "C": 3591,
    "D": "081000",
    "E": "Accha"
},
{
    "A": "081003",
    "B": "CCAPI",
    "C": 3196,
    "D": "081000",
    "E": "Ccapi"
},
{
    "A": "081004",
    "B": "COLCHA",
    "C": 2800,
    "D": "081000",
    "E": "Colcha"
},
{
    "A": "081005",
    "B": "HUANOQUITE",
    "C": 3391,
    "D": "081000",
    "E": "Huanoquite"
},
{
    "A": "081006",
    "B": "OMACHA",
    "C": 3910,
    "D": "081000",
    "E": "Omacha"
},
{
    "A": "081007",
    "B": "PACCARITAMBO",
    "C": 3581,
    "D": "081000",
    "E": "Paccaritambo"
},
{
    "A": "081008",
    "B": "PILLPINTO",
    "C": 2866,
    "D": "081000",
    "E": "Pillpinto"
},
{
    "A": "081009",
    "B": "YAURISQUE",
    "C": 3328,
    "D": "081000",
    "E": "Yaurisque"
},
{
    "A": "081101",
    "B": "PAUCARTAMBO",
    "C": 3005,
    "D": "081100",
    "E": "Paucartambo"
},
{
    "A": "081102",
    "B": "CAICAY",
    "C": 3110,
    "D": "081100",
    "E": "Caicay"
},
{
    "A": "081103",
    "B": "CHALLABAMBA",
    "C": 2767,
    "D": "081100",
    "E": "Challabamba"
},
{
    "A": "081104",
    "B": "COLQUEPATA",
    "C": 3650,
    "D": "081100",
    "E": "Colquepata"
},
{
    "A": "081105",
    "B": "HUANCARANI",
    "C": 3850,
    "D": "081100",
    "E": "Huancarani"
},
{
    "A": "081106",
    "B": "KOSÑIPATA",
    "C": 689,
    "D": "081100",
    "E": "Kosñipata"
},
{
    "A": "081201",
    "B": "URCOS",
    "C": 3158,
    "D": "081200",
    "E": "Urcos"
},
{
    "A": "081202",
    "B": "ANDAHUAYLILLAS",
    "C": 3121,
    "D": "081200",
    "E": "Andahuaylillas"
},
{
    "A": "081203",
    "B": "CAMANTI",
    "C": 873,
    "D": "081200",
    "E": "Camanti"
},
{
    "A": "081204",
    "B": "CCARHUAYO",
    "C": 3449,
    "D": "081200",
    "E": "Ccarhuayo"
},
{
    "A": "081205",
    "B": "CCATCA",
    "C": 3700,
    "D": "081200",
    "E": "Ccatca"
},
{
    "A": "081206",
    "B": "CUSIPATA",
    "C": 3319,
    "D": "081200",
    "E": "Cusipata"
},
{
    "A": "081207",
    "B": "HUARO",
    "C": 3162,
    "D": "081200",
    "E": "Huaro"
},
{
    "A": "081208",
    "B": "LUCRE",
    "C": 3111,
    "D": "081200",
    "E": "Lucre"
},
{
    "A": "081209",
    "B": "MARCAPATA",
    "C": 3150,
    "D": "081200",
    "E": "Marcapata"
},
{
    "A": "081210",
    "B": "OCONGATE",
    "C": 3540,
    "D": "081200",
    "E": "Ocongate"
},
{
    "A": "081211",
    "B": "OROPESA",
    "C": 3110,
    "D": "081200",
    "E": "Oropesa"
},
{
    "A": "081212",
    "B": "QUIQUIJANA",
    "C": 3553,
    "D": "081200",
    "E": "Quiquijana"
},
{
    "A": "081301",
    "B": "URUBAMBA",
    "C": 2869,
    "D": "081300",
    "E": "Urubamba"
},
{
    "A": "081302",
    "B": "CHINCHERO",
    "C": 3754,
    "D": "081300",
    "E": "Chinchero"
},
{
    "A": "081303",
    "B": "HUAYLLABAMBA",
    "C": 2868,
    "D": "081300",
    "E": "Huayllabamba"
},
{
    "A": "081304",
    "B": "MACHUPICCHU",
    "C": 2162,
    "D": "081300",
    "E": "Machupicchu"
},
{
    "A": "081305",
    "B": "MARAS",
    "C": 3384,
    "D": "081300",
    "E": "Maras"
},
{
    "A": "081306",
    "B": "OLLANTAYTAMBO",
    "C": 2848,
    "D": "081300",
    "E": "Ollantaytambo"
},
{
    "A": "081307",
    "B": "YUCAY",
    "C": 2858,
    "D": "081300",
    "E": "Yucay"
},
{
    "A": "090101",
    "B": "HUANCAVELICA",
    "C": 3679,
    "D": "090100",
    "E": "Huancavelica"
},
{
    "A": "090102",
    "B": "ACOBAMBILLA",
    "C": 3820,
    "D": "090100",
    "E": "Acobambilla"
},
{
    "A": "090103",
    "B": "ACORIA",
    "C": 3301,
    "D": "090100",
    "E": "Acoria"
},
{
    "A": "090104",
    "B": "CONAYCA",
    "C": 3708,
    "D": "090100",
    "E": "Conayca"
},
{
    "A": "090105",
    "B": "CUENCA",
    "C": 3190,
    "D": "090100",
    "E": "Cuenca"
},
{
    "A": "090106",
    "B": "HUACHOCOLPA",
    "C": 4102,
    "D": "090100",
    "E": "Huachocolpa"
},
{
    "A": "090107",
    "B": "HUAYLLAHUARA",
    "C": 3872,
    "D": "090100",
    "E": "Huayllahuara"
},
{
    "A": "090108",
    "B": "IZCUCHACA",
    "C": 2900,
    "D": "090100",
    "E": "Izcuchaca"
},
{
    "A": "090109",
    "B": "LARIA",
    "C": 3906,
    "D": "090100",
    "E": "Laria"
},
{
    "A": "090110",
    "B": "MANTA",
    "C": 3675,
    "D": "090100",
    "E": "Manta"
},
{
    "A": "090111",
    "B": "MARISCAL CACERES",
    "C": 2881,
    "D": "090100",
    "E": "Mariscal Caceres"
},
{
    "A": "090112",
    "B": "MOYA",
    "C": 3118,
    "D": "090100",
    "E": "Moya"
},
{
    "A": "090113",
    "B": "NUEVO OCCORO",
    "C": 3857,
    "D": "090100",
    "E": "Nuevo Occoro"
},
{
    "A": "090114",
    "B": "PALCA",
    "C": 3791,
    "D": "090100",
    "E": "Palca"
},
{
    "A": "090115",
    "B": "PILCHACA",
    "C": 3586,
    "D": "090100",
    "E": "Pilchaca"
},
{
    "A": "090116",
    "B": "VILCA",
    "C": 3390,
    "D": "090100",
    "E": "Vilca"
},
{
    "A": "090117",
    "B": "YAULI",
    "C": 3402,
    "D": "090100",
    "E": "Yauli"
},
{
    "A": "090118",
    "B": "ASCENSION",
    "C": 3693,
    "D": "090100",
    "E": "Ascension"
},
{
    "A": "090119",
    "B": "HUANDO",
    "C": 3586,
    "D": "090100",
    "E": "Huando"
},
{
    "A": "090201",
    "B": "ACOBAMBA",
    "C": 3431,
    "D": "090200",
    "E": "Acobamba"
},
{
    "A": "090202",
    "B": "ANDABAMBA",
    "C": 3526,
    "D": "090200",
    "E": "Andabamba"
},
{
    "A": "090203",
    "B": "ANTA",
    "C": 3588,
    "D": "090200",
    "E": "Anta"
},
{
    "A": "090204",
    "B": "CAJA",
    "C": 3380,
    "D": "090200",
    "E": "Caja"
},
{
    "A": "090205",
    "B": "MARCAS",
    "C": 3382,
    "D": "090200",
    "E": "Marcas"
},
{
    "A": "090206",
    "B": "PAUCARA",
    "C": 3795,
    "D": "090200",
    "E": "Paucara"
},
{
    "A": "090207",
    "B": "POMACOCHA",
    "C": 3145,
    "D": "090200",
    "E": "Pomacocha"
},
{
    "A": "090208",
    "B": "ROSARIO",
    "C": 3625,
    "D": "090200",
    "E": "Rosario"
},
{
    "A": "090301",
    "B": "LIRCAY",
    "C": 3271,
    "D": "090300",
    "E": "Lircay"
},
{
    "A": "090302",
    "B": "ANCHONGA",
    "C": 3358,
    "D": "090300",
    "E": "Anchonga"
},
{
    "A": "090303",
    "B": "CALLANMARCA",
    "C": 3532,
    "D": "090300",
    "E": "Callanmarca"
},
{
    "A": "090304",
    "B": "CCOCHACCASA",
    "C": 4179,
    "D": "090300",
    "E": "Ccochaccasa"
},
{
    "A": "090305",
    "B": "CHINCHO",
    "C": 3215,
    "D": "090300",
    "E": "Chincho"
},
{
    "A": "090306",
    "B": "CONGALLA",
    "C": 3535,
    "D": "090300",
    "E": "Congalla"
},
{
    "A": "090307",
    "B": "HUANCA-HUANCA",
    "C": 3569,
    "D": "090300",
    "E": "Huanca-Huanca"
},
{
    "A": "090308",
    "B": "HUAYLLAY GRANDE",
    "C": 3588,
    "D": "090300",
    "E": "Huayllay Grande"
},
{
    "A": "090309",
    "B": "JULCAMARCA",
    "C": 3404,
    "D": "090300",
    "E": "Julcamarca"
},
{
    "A": "090310",
    "B": "SAN ANTONIO DE ANTAPARCO",
    "C": 2771,
    "D": "090300",
    "E": "San Antonio De Antaparco"
},
{
    "A": "090311",
    "B": "SANTO TOMAS DE PATA",
    "C": 3165,
    "D": "090300",
    "E": "Santo Tomas De Pata"
},
{
    "A": "090312",
    "B": "SECCLLA",
    "C": 3375,
    "D": "090300",
    "E": "Secclla"
},
{
    "A": "090401",
    "B": "CASTROVIRREYNA",
    "C": 3958,
    "D": "090400",
    "E": "Castrovirreyna"
},
{
    "A": "090402",
    "B": "ARMA",
    "C": 3332,
    "D": "090400",
    "E": "Arma"
},
{
    "A": "090403",
    "B": "AURAHUA",
    "C": 3488,
    "D": "090400",
    "E": "Aurahua"
},
{
    "A": "090404",
    "B": "CAPILLAS",
    "C": 3188,
    "D": "090400",
    "E": "Capillas"
},
{
    "A": "090405",
    "B": "CHUPAMARCA",
    "C": 3329,
    "D": "090400",
    "E": "Chupamarca"
},
{
    "A": "090406",
    "B": "COCAS",
    "C": 3210,
    "D": "090400",
    "E": "Cocas"
},
{
    "A": "090407",
    "B": "HUACHOS",
    "C": 2751,
    "D": "090400",
    "E": "Huachos"
},
{
    "A": "090408",
    "B": "HUAMATAMBO",
    "C": 3261,
    "D": "090400",
    "E": "Huamatambo"
},
{
    "A": "090409",
    "B": "MOLLEPAMPA",
    "C": 2515,
    "D": "090400",
    "E": "Mollepampa"
},
{
    "A": "090410",
    "B": "SAN JUAN",
    "C": 1913,
    "D": "090400",
    "E": "San Juan"
},
{
    "A": "090411",
    "B": "SANTA ANA",
    "C": 4497,
    "D": "090400",
    "E": "Santa Ana"
},
{
    "A": "090412",
    "B": "TANTARA",
    "C": 2874,
    "D": "090400",
    "E": "Tantara"
},
{
    "A": "090413",
    "B": "TICRAPO",
    "C": 2178,
    "D": "090400",
    "E": "Ticrapo"
},
{
    "A": "090501",
    "B": "CHURCAMPA",
    "C": 3275,
    "D": "090500",
    "E": "Churcampa"
},
{
    "A": "090502",
    "B": "ANCO",
    "C": 2413,
    "D": "090500",
    "E": "Anco"
},
{
    "A": "090503",
    "B": "CHINCHIHUASI",
    "C": 2776,
    "D": "090500",
    "E": "Chinchihuasi"
},
{
    "A": "090504",
    "B": "EL CARMEN",
    "C": 3050,
    "D": "090500",
    "E": "El Carmen"
},
{
    "A": "090505",
    "B": "LA MERCED",
    "C": 2628,
    "D": "090500",
    "E": "La Merced"
},
{
    "A": "090506",
    "B": "LOCROJA",
    "C": 3387,
    "D": "090500",
    "E": "Locroja"
},
{
    "A": "090507",
    "B": "PAUCARBAMBA",
    "C": 3373,
    "D": "090500",
    "E": "Paucarbamba"
},
{
    "A": "090508",
    "B": "SAN MIGUEL DE MAYOCC",
    "C": 2237,
    "D": "090500",
    "E": "San Miguel De Mayocc"
},
{
    "A": "090509",
    "B": "SAN PEDRO DE CORIS",
    "C": 3564,
    "D": "090500",
    "E": "San Pedro De Coris"
},
{
    "A": "090510",
    "B": "PACHAMARCA",
    "C": 2768,
    "D": "090500",
    "E": "Pachamarca"
},
{
    "A": "090511",
    "B": "COSME",
    "C": 3496,
    "D": "090500",
    "E": "Cosme"
},
{
    "A": "090601",
    "B": "HUAYTARA",
    "C": 2712,
    "D": "090600",
    "E": "Huaytara"
},
{
    "A": "090602",
    "B": "AYAVI",
    "C": 3801,
    "D": "090600",
    "E": "Ayavi"
},
{
    "A": "090603",
    "B": "CORDOVA",
    "C": 3216,
    "D": "090600",
    "E": "Cordova"
},
{
    "A": "090604",
    "B": "HUAYACUNDO ARMA",
    "C": 3175,
    "D": "090600",
    "E": "Huayacundo Arma"
},
{
    "A": "090605",
    "B": "LARAMARCA",
    "C": 3278,
    "D": "090600",
    "E": "Laramarca"
},
{
    "A": "090606",
    "B": "OCOYO",
    "C": 1951,
    "D": "090600",
    "E": "Ocoyo"
},
{
    "A": "090607",
    "B": "PILPICHACA",
    "C": 4109,
    "D": "090600",
    "E": "Pilpichaca"
},
{
    "A": "090608",
    "B": "QUERCO",
    "C": 2873,
    "D": "090600",
    "E": "Querco"
},
{
    "A": "090609",
    "B": "QUITO-ARMA",
    "C": 2994,
    "D": "090600",
    "E": "Quito-Arma"
},
{
    "A": "090610",
    "B": "SAN ANTONIO DE CUSICANCHA",
    "C": 3379,
    "D": "090600",
    "E": "San Antonio De Cusicancha"
},
{
    "A": "090611",
    "B": "SAN FRANCISCO DE SANGAYAICO",
    "C": 3375,
    "D": "090600",
    "E": "San Francisco De Sangayaico"
},
{
    "A": "090612",
    "B": "SAN ISIDRO",
    "C": 3606,
    "D": "090600",
    "E": "San Isidro"
},
{
    "A": "090613",
    "B": "SANTIAGO DE CHOCORVOS",
    "C": 2669,
    "D": "090600",
    "E": "Santiago De Chocorvos"
},
{
    "A": "090614",
    "B": "SANTIAGO DE QUIRAHUARA",
    "C": 2828,
    "D": "090600",
    "E": "Santiago De Quirahuara"
},
{
    "A": "090615",
    "B": "SANTO DOMINGO DE CAPILLAS",
    "C": 3379,
    "D": "090600",
    "E": "Santo Domingo De Capillas"
},
{
    "A": "090616",
    "B": "TAMBO",
    "C": 3200,
    "D": "090600",
    "E": "Tambo"
},
{
    "A": "090701",
    "B": "PAMPAS",
    "C": 3251,
    "D": "090700",
    "E": "Pampas"
},
{
    "A": "090702",
    "B": "ACOSTAMBO",
    "C": 3610,
    "D": "090700",
    "E": "Acostambo"
},
{
    "A": "090703",
    "B": "ACRAQUIA",
    "C": 3277,
    "D": "090700",
    "E": "Acraquia"
},
{
    "A": "090704",
    "B": "AHUAYCHA",
    "C": 3264,
    "D": "090700",
    "E": "Ahuaycha"
},
{
    "A": "090705",
    "B": "COLCABAMBA",
    "C": 2967,
    "D": "090700",
    "E": "Colcabamba"
},
{
    "A": "090706",
    "B": "DANIEL HERNANDEZ",
    "C": 3249,
    "D": "090700",
    "E": "Daniel Hernandez"
},
{
    "A": "090707",
    "B": "HUACHOCOLPA",
    "C": 2914,
    "D": "090700",
    "E": "Huachocolpa"
},
{
    "A": "090709",
    "B": "HUARIBAMBA",
    "C": 3170,
    "D": "090700",
    "E": "Huaribamba"
},
{
    "A": "090710",
    "B": "ÑAHUIMPUQUIO",
    "C": 3712,
    "D": "090700",
    "E": "Ñahuimpuquio"
},
{
    "A": "090711",
    "B": "PAZOS",
    "C": 3804,
    "D": "090700",
    "E": "Pazos"
},
{
    "A": "090713",
    "B": "QUISHUAR",
    "C": 3101,
    "D": "090700",
    "E": "Quishuar"
},
{
    "A": "090714",
    "B": "SALCABAMBA",
    "C": 3138,
    "D": "090700",
    "E": "Salcabamba"
},
{
    "A": "090715",
    "B": "SALCAHUASI",
    "C": 3145,
    "D": "090700",
    "E": "Salcahuasi"
},
{
    "A": "090716",
    "B": "SAN MARCOS DE ROCCHAC",
    "C": 3211,
    "D": "090700",
    "E": "San Marcos De Rocchac"
},
{
    "A": "090717",
    "B": "SURCUBAMBA",
    "C": 2581,
    "D": "090700",
    "E": "Surcubamba"
},
{
    "A": "090718",
    "B": "TINTAY PUNCU",
    "C": 2313,
    "D": "090700",
    "E": "Tintay Puncu"
},
{
    "A": "090719",
    "B": "QUICHUAS",
    "C": 2600,
    "D": "090700",
    "E": "Quichuas"
},
{
    "A": "090720",
    "B": "ANDAYMARCA",
    "C": 2800,
    "D": "090700",
    "E": "Andaymarca"
},
{
    "A": 100101,
    "B": "HUANUCO",
    "C": 1898,
    "D": "100100",
    "E": "Huanuco"
},
{
    "A": 100102,
    "B": "AMARILIS",
    "C": 1921,
    "D": "100100",
    "E": "Amarilis"
},
{
    "A": 100103,
    "B": "CHINCHAO",
    "C": 2074,
    "D": "100100",
    "E": "Chinchao"
},
{
    "A": 100104,
    "B": "CHURUBAMBA",
    "C": 1983,
    "D": "100100",
    "E": "Churubamba"
},
{
    "A": 100105,
    "B": "MARGOS",
    "C": 3547,
    "D": "100100",
    "E": "Margos"
},
{
    "A": 100106,
    "B": "QUISQUI",
    "C": 2131,
    "D": "100100",
    "E": "Quisqui"
},
{
    "A": 100107,
    "B": "SAN FRANCISCO DE CAYRAN",
    "C": 2227,
    "D": "100100",
    "E": "San Francisco De Cayran"
},
{
    "A": 100108,
    "B": "SAN PEDRO DE CHAULAN",
    "C": 3612,
    "D": "100100",
    "E": "San Pedro De Chaulan"
},
{
    "A": 100109,
    "B": "SANTA MARIA DEL VALLE",
    "C": 1906,
    "D": "100100",
    "E": "Santa Maria Del Valle"
},
{
    "A": 100110,
    "B": "YARUMAYO",
    "C": 3086,
    "D": "100100",
    "E": "Yarumayo"
},
{
    "A": 100111,
    "B": "PILLCO MARCA",
    "C": 1934,
    "D": "100100",
    "E": "Pillco Marca"
},
{
    "A": 100112,
    "B": "YACUS",
    "C": 3226,
    "D": "100100",
    "E": "Yacus"
},
{
    "A": 100201,
    "B": "AMBO",
    "C": 2076,
    "D": "100200",
    "E": "Ambo"
},
{
    "A": 100202,
    "B": "CAYNA",
    "C": 3296,
    "D": "100200",
    "E": "Cayna"
},
{
    "A": 100203,
    "B": "COLPAS",
    "C": 2770,
    "D": "100200",
    "E": "Colpas"
},
{
    "A": 100204,
    "B": "CONCHAMARCA",
    "C": 2162,
    "D": "100200",
    "E": "Conchamarca"
},
{
    "A": 100205,
    "B": "HUACAR",
    "C": 2116,
    "D": "100200",
    "E": "Huacar"
},
{
    "A": 100206,
    "B": "SAN FRANCISCO",
    "C": 3450,
    "D": "100200",
    "E": "San Francisco"
},
{
    "A": 100207,
    "B": "SAN RAFAEL",
    "C": 2703,
    "D": "100200",
    "E": "San Rafael"
},
{
    "A": 100208,
    "B": "TOMAY KICHWA",
    "C": 2020,
    "D": "100200",
    "E": "Tomay Kichwa"
},
{
    "A": 100301,
    "B": "LA UNION",
    "C": 3210,
    "D": "100300",
    "E": "La Union"
},
{
    "A": 100307,
    "B": "CHUQUIS",
    "C": 3377,
    "D": "100300",
    "E": "Chuquis"
},
{
    "A": 100311,
    "B": "MARIAS",
    "C": 3530,
    "D": "100300",
    "E": "Marias"
},
{
    "A": 100313,
    "B": "PACHAS",
    "C": 3499,
    "D": "100300",
    "E": "Pachas"
},
{
    "A": 100316,
    "B": "QUIVILLA",
    "C": 2981,
    "D": "100300",
    "E": "Quivilla"
},
{
    "A": 100317,
    "B": "RIPAN",
    "C": 3211,
    "D": "100300",
    "E": "Ripan"
},
{
    "A": 100321,
    "B": "SHUNQUI",
    "C": 3552,
    "D": "100300",
    "E": "Shunqui"
},
{
    "A": 100322,
    "B": "SILLAPATA",
    "C": 3446,
    "D": "100300",
    "E": "Sillapata"
},
{
    "A": 100323,
    "B": "YANAS",
    "C": 3461,
    "D": "100300",
    "E": "Yanas"
},
{
    "A": 100401,
    "B": "HUACAYBAMBA",
    "C": 3191,
    "D": "100400",
    "E": "Huacaybamba"
},
{
    "A": 100402,
    "B": "CANCHABAMBA",
    "C": 3154,
    "D": "100400",
    "E": "Canchabamba"
},
{
    "A": 100403,
    "B": "COCHABAMBA",
    "C": 3307,
    "D": "100400",
    "E": "Cochabamba"
},
{
    "A": 100404,
    "B": "PINRA",
    "C": 2895,
    "D": "100400",
    "E": "Pinra"
},
{
    "A": 100501,
    "B": "LLATA",
    "C": 3436,
    "D": "100500",
    "E": "Llata"
},
{
    "A": 100502,
    "B": "ARANCAY",
    "C": 3137,
    "D": "100500",
    "E": "Arancay"
},
{
    "A": 100503,
    "B": "CHAVIN DE PARIARCA",
    "C": 3386,
    "D": "100500",
    "E": "Chavin De Pariarca"
},
{
    "A": 100504,
    "B": "JACAS GRANDE",
    "C": 3632,
    "D": "100500",
    "E": "Jacas Grande"
},
{
    "A": 100505,
    "B": "JIRCAN",
    "C": 3292,
    "D": "100500",
    "E": "Jircan"
},
{
    "A": 100506,
    "B": "MIRAFLORES",
    "C": 3691,
    "D": "100500",
    "E": "Miraflores"
},
{
    "A": 100507,
    "B": "MONZON",
    "C": 1108,
    "D": "100500",
    "E": "Monzon"
},
{
    "A": 100508,
    "B": "PUNCHAO",
    "C": 3547,
    "D": "100500",
    "E": "Punchao"
},
{
    "A": 100509,
    "B": "PUÑOS",
    "C": 3733,
    "D": "100500",
    "E": "Puños"
},
{
    "A": 100510,
    "B": "SINGA",
    "C": 3648,
    "D": "100500",
    "E": "Singa"
},
{
    "A": 100511,
    "B": "TANTAMAYO",
    "C": 3484,
    "D": "100500",
    "E": "Tantamayo"
},
{
    "A": 100601,
    "B": "RUPA-RUPA",
    "C": 648,
    "D": "100600",
    "E": "Rupa-Rupa"
},
{
    "A": 100602,
    "B": "DANIEL ALOMIA ROBLES",
    "C": 768,
    "D": "100600",
    "E": "Daniel Alomia Robles"
},
{
    "A": 100603,
    "B": "HERMILIO VALDIZAN",
    "C": 1250,
    "D": "100600",
    "E": "Hermilio Valdizan"
},
{
    "A": 100604,
    "B": "JOSE CRESPO Y CASTILLO",
    "C": 569,
    "D": "100600",
    "E": "Jose Crespo Y Castillo"
},
{
    "A": 100605,
    "B": "LUYANDO",
    "C": 631,
    "D": "100600",
    "E": "Luyando"
},
{
    "A": 100606,
    "B": "MARIANO DAMASO BERAUN",
    "C": 982,
    "D": "100600",
    "E": "Mariano Damaso Beraun"
},
{
    "A": 100701,
    "B": "HUACRACHUCO",
    "C": 2893,
    "D": "100700",
    "E": "Huacrachuco"
},
{
    "A": 100702,
    "B": "CHOLON",
    "C": 2127,
    "D": "100700",
    "E": "Cholon"
},
{
    "A": 100703,
    "B": "SAN BUENAVENTURA",
    "C": 3160,
    "D": "100700",
    "E": "San Buenaventura"
},
{
    "A": 100801,
    "B": "PANAO",
    "C": 2772,
    "D": "100800",
    "E": "Panao"
},
{
    "A": 100802,
    "B": "CHAGLLA",
    "C": 2568,
    "D": "100800",
    "E": "Chaglla"
},
{
    "A": 100803,
    "B": "MOLINO",
    "C": 2361,
    "D": "100800",
    "E": "Molino"
},
{
    "A": 100804,
    "B": "UMARI",
    "C": 2424,
    "D": "100800",
    "E": "Umari"
},
{
    "A": 100901,
    "B": "PUERTO INCA",
    "C": 210,
    "D": "100900",
    "E": "Puerto Inca"
},
{
    "A": 100902,
    "B": "CODO DEL POZUZO",
    "C": 367,
    "D": "100900",
    "E": "Codo Del Pozuzo"
},
{
    "A": 100903,
    "B": "HONORIA",
    "C": 168,
    "D": "100900",
    "E": "Honoria"
},
{
    "A": 100904,
    "B": "TOURNAVISTA",
    "C": 188,
    "D": "100900",
    "E": "Tournavista"
},
{
    "A": 100905,
    "B": "YUYAPICHIS",
    "C": 207,
    "D": "100900",
    "E": "Yuyapichis"
},
{
    "A": 101001,
    "B": "JESUS",
    "C": 3485,
    "D": "101000",
    "E": "Jesus"
},
{
    "A": 101002,
    "B": "BAÑOS",
    "C": 3423,
    "D": "101000",
    "E": "Baños"
},
{
    "A": 101003,
    "B": "JIVIA",
    "C": 3388,
    "D": "101000",
    "E": "Jivia"
},
{
    "A": 101004,
    "B": "QUEROPALCA",
    "C": 3831,
    "D": "101000",
    "E": "Queropalca"
},
{
    "A": 101005,
    "B": "RONDOS",
    "C": 3568,
    "D": "101000",
    "E": "Rondos"
},
{
    "A": 101006,
    "B": "SAN FRANCISCO DE ASIS",
    "C": 3439,
    "D": "101000",
    "E": "San Francisco De Asis"
},
{
    "A": 101007,
    "B": "SAN MIGUEL DE CAURI",
    "C": 3594,
    "D": "101000",
    "E": "San Miguel De Cauri"
},
{
    "A": 101101,
    "B": "CHAVINILLO",
    "C": 3254,
    "D": "101100",
    "E": "Chavinillo"
},
{
    "A": 101102,
    "B": "CAHUAC",
    "C": 3332,
    "D": "101100",
    "E": "Cahuac"
},
{
    "A": 101103,
    "B": "CHACABAMBA",
    "C": 3171,
    "D": "101100",
    "E": "Chacabamba"
},
{
    "A": 101104,
    "B": "APARICIO POMARES",
    "C": 3423,
    "D": "101100",
    "E": "Aparicio Pomares"
},
{
    "A": 101105,
    "B": "JACAS CHICO",
    "C": 3761,
    "D": "101100",
    "E": "Jacas Chico"
},
{
    "A": 101106,
    "B": "OBAS",
    "C": 3528,
    "D": "101100",
    "E": "Obas"
},
{
    "A": 101107,
    "B": "PAMPAMARCA",
    "C": 3445,
    "D": "101100",
    "E": "Pampamarca"
},
{
    "A": 101108,
    "B": "CHORAS",
    "C": 3585,
    "D": "101100",
    "E": "Choras"
},
{
    "A": 110101,
    "B": "ICA",
    "C": 409,
    "D": "110100",
    "E": "Ica"
},
{
    "A": 110102,
    "B": "LA TINGUIÑA",
    "C": 440,
    "D": "110100",
    "E": "La Tinguiña"
},
{
    "A": 110103,
    "B": "LOS AQUIJES",
    "C": 417,
    "D": "110100",
    "E": "Los Aquijes"
},
{
    "A": 110104,
    "B": "OCUCAJE",
    "C": 334,
    "D": "110100",
    "E": "Ocucaje"
},
{
    "A": 110105,
    "B": "PACHACUTEC",
    "C": 409,
    "D": "110100",
    "E": "Pachacutec"
},
{
    "A": 110106,
    "B": "PARCONA",
    "C": 436,
    "D": "110100",
    "E": "Parcona"
},
{
    "A": 110107,
    "B": "PUEBLO NUEVO",
    "C": 404,
    "D": "110100",
    "E": "Pueblo Nuevo"
},
{
    "A": 110108,
    "B": "SALAS",
    "C": 430,
    "D": "110100",
    "E": "Salas"
},
{
    "A": 110109,
    "B": "SAN JOSE DE LOS MOLINOS",
    "C": 532,
    "D": "110100",
    "E": "San Jose De Los Molinos"
},
{
    "A": 110110,
    "B": "SAN JUAN BAUTISTA",
    "C": 426,
    "D": "110100",
    "E": "San Juan Bautista"
},
{
    "A": 110111,
    "B": "SANTIAGO",
    "C": 378,
    "D": "110100",
    "E": "Santiago"
},
{
    "A": 110112,
    "B": "SUBTANJALLA",
    "C": 424,
    "D": "110100",
    "E": "Subtanjalla"
},
{
    "A": 110113,
    "B": "TATE",
    "C": 395,
    "D": "110100",
    "E": "Tate"
},
{
    "A": 110114,
    "B": "YAUCA DEL ROSARIO",
    "C": 833,
    "D": "110100",
    "E": "Yauca Del Rosario"
},
{
    "A": 110201,
    "B": "CHINCHA ALTA",
    "C": 94,
    "D": "110200",
    "E": "Chincha Alta"
},
{
    "A": 110202,
    "B": "ALTO LARAN",
    "C": 91,
    "D": "110200",
    "E": "Alto Laran"
},
{
    "A": 110203,
    "B": "CHAVIN",
    "C": 3170,
    "D": "110200",
    "E": "Chavin"
},
{
    "A": 110204,
    "B": "CHINCHA BAJA",
    "C": 33,
    "D": "110200",
    "E": "Chincha Baja"
},
{
    "A": 110205,
    "B": "EL CARMEN",
    "C": 153,
    "D": "110200",
    "E": "El Carmen"
},
{
    "A": 110206,
    "B": "GROCIO PRADO",
    "C": 93,
    "D": "110200",
    "E": "Grocio Prado"
},
{
    "A": 110207,
    "B": "PUEBLO NUEVO",
    "C": 126,
    "D": "110200",
    "E": "Pueblo Nuevo"
},
{
    "A": 110208,
    "B": "SAN JUAN DE YANAC",
    "C": 2533,
    "D": "110200",
    "E": "San Juan De Yanac"
},
{
    "A": 110209,
    "B": "SAN PEDRO DE HUACARPANA",
    "C": 3776,
    "D": "110200",
    "E": "San Pedro De Huacarpana"
},
{
    "A": 110210,
    "B": "SUNAMPE",
    "C": 64,
    "D": "110200",
    "E": "Sunampe"
},
{
    "A": 110211,
    "B": "TAMBO DE MORA",
    "C": 5,
    "D": "110200",
    "E": "Tambo De Mora"
},
{
    "A": 110301,
    "B": "NASCA",
    "C": 585,
    "D": "110300",
    "E": "Nasca"
},
{
    "A": 110302,
    "B": "CHANGUILLO",
    "C": 244,
    "D": "110300",
    "E": "Changuillo"
},
{
    "A": 110303,
    "B": "EL INGENIO",
    "C": 447,
    "D": "110300",
    "E": "El Ingenio"
},
{
    "A": 110304,
    "B": "MARCONA",
    "C": 45,
    "D": "110300",
    "E": "Marcona"
},
{
    "A": 110305,
    "B": "VISTA ALEGRE",
    "C": 565,
    "D": "110300",
    "E": "Vista Alegre"
},
{
    "A": 110401,
    "B": "PALPA",
    "C": 351,
    "D": "110400",
    "E": "Palpa"
},
{
    "A": 110402,
    "B": "LLIPATA",
    "C": 301,
    "D": "110400",
    "E": "Llipata"
},
{
    "A": 110403,
    "B": "RIO GRANDE",
    "C": 357,
    "D": "110400",
    "E": "Rio Grande"
},
{
    "A": 110404,
    "B": "SANTA CRUZ",
    "C": 526,
    "D": "110400",
    "E": "Santa Cruz"
},
{
    "A": 110405,
    "B": "TIBILLO",
    "C": 2180,
    "D": "110400",
    "E": "Tibillo"
},
{
    "A": 110501,
    "B": "PISCO",
    "C": 15,
    "D": "110500",
    "E": "Pisco"
},
{
    "A": 110502,
    "B": "HUANCANO",
    "C": 994,
    "D": "110500",
    "E": "Huancano"
},
{
    "A": 110503,
    "B": "HUMAY",
    "C": 410,
    "D": "110500",
    "E": "Humay"
},
{
    "A": 110504,
    "B": "INDEPENDENCIA",
    "C": 211,
    "D": "110500",
    "E": "Independencia"
},
{
    "A": 110505,
    "B": "PARACAS",
    "C": 7,
    "D": "110500",
    "E": "Paracas"
},
{
    "A": 110506,
    "B": "SAN ANDRES",
    "C": 8,
    "D": "110500",
    "E": "San Andres"
},
{
    "A": 110507,
    "B": "SAN CLEMENTE",
    "C": 70,
    "D": "110500",
    "E": "San Clemente"
},
{
    "A": 110508,
    "B": "TUPAC AMARU INCA",
    "C": 84,
    "D": "110500",
    "E": "Tupac Amaru Inca"
},
{
    "A": 120101,
    "B": "HUANCAYO",
    "C": 3245,
    "D": "120100",
    "E": "Huancayo"
},
{
    "A": 120104,
    "B": "CARHUACALLANGA",
    "C": 3824,
    "D": "120100",
    "E": "Carhuacallanga"
},
{
    "A": 120105,
    "B": "CHACAPAMPA",
    "C": 3358,
    "D": "120100",
    "E": "Chacapampa"
},
{
    "A": 120106,
    "B": "CHICCHE",
    "C": 3560,
    "D": "120100",
    "E": "Chicche"
},
{
    "A": 120107,
    "B": "CHILCA",
    "C": 3245,
    "D": "120100",
    "E": "Chilca"
},
{
    "A": 120108,
    "B": "CHONGOS ALTO",
    "C": 3625,
    "D": "120100",
    "E": "Chongos Alto"
},
{
    "A": 120111,
    "B": "CHUPURO",
    "C": 3180,
    "D": "120100",
    "E": "Chupuro"
},
{
    "A": 120112,
    "B": "COLCA",
    "C": 3544,
    "D": "120100",
    "E": "Colca"
},
{
    "A": 120113,
    "B": "CULLHUAS",
    "C": 3732,
    "D": "120100",
    "E": "Cullhuas"
},
{
    "A": 120114,
    "B": "EL TAMBO",
    "C": 3262,
    "D": "120100",
    "E": "El Tambo"
},
{
    "A": 120116,
    "B": "HUACRAPUQUIO",
    "C": 3219,
    "D": "120100",
    "E": "Huacrapuquio"
},
{
    "A": 120117,
    "B": "HUALHUAS",
    "C": 3263,
    "D": "120100",
    "E": "Hualhuas"
},
{
    "A": 120119,
    "B": "HUANCAN",
    "C": 3214,
    "D": "120100",
    "E": "Huancan"
},
{
    "A": 120120,
    "B": "HUASICANCHA",
    "C": 3746,
    "D": "120100",
    "E": "Huasicancha"
},
{
    "A": 120121,
    "B": "HUAYUCACHI",
    "C": 3188,
    "D": "120100",
    "E": "Huayucachi"
},
{
    "A": 120122,
    "B": "INGENIO",
    "C": 3451,
    "D": "120100",
    "E": "Ingenio"
},
{
    "A": 120124,
    "B": "PARIAHUANCA",
    "C": 2589,
    "D": "120100",
    "E": "Pariahuanca"
},
{
    "A": 120125,
    "B": "PILCOMAYO",
    "C": 3212,
    "D": "120100",
    "E": "Pilcomayo"
},
{
    "A": 120126,
    "B": "PUCARA",
    "C": 3340,
    "D": "120100",
    "E": "Pucara"
},
{
    "A": 120127,
    "B": "QUICHUAY",
    "C": 3401,
    "D": "120100",
    "E": "Quichuay"
},
{
    "A": 120128,
    "B": "QUILCAS",
    "C": 3303,
    "D": "120100",
    "E": "Quilcas"
},
{
    "A": 120129,
    "B": "SAN AGUSTIN",
    "C": 3250,
    "D": "120100",
    "E": "San Agustin"
},
{
    "A": 120130,
    "B": "SAN JERONIMO DE TUNAN",
    "C": 3254,
    "D": "120100",
    "E": "San Jeronimo De Tunan"
},
{
    "A": 120132,
    "B": "SAÑO",
    "C": 3268,
    "D": "120100",
    "E": "Saño"
},
{
    "A": 120133,
    "B": "SAPALLANGA",
    "C": 3299,
    "D": "120100",
    "E": "Sapallanga"
},
{
    "A": 120134,
    "B": "SICAYA",
    "C": 3273,
    "D": "120100",
    "E": "Sicaya"
},
{
    "A": 120135,
    "B": "SANTO DOMINGO DE ACOBAMBA",
    "C": 2197,
    "D": "120100",
    "E": "Santo Domingo De Acobamba"
},
{
    "A": 120136,
    "B": "VIQUES",
    "C": 3179,
    "D": "120100",
    "E": "Viques"
},
{
    "A": 120201,
    "B": "CONCEPCION",
    "C": 3286,
    "D": "120200",
    "E": "Concepcion"
},
{
    "A": 120202,
    "B": "ACO",
    "C": 3447,
    "D": "120200",
    "E": "Aco"
},
{
    "A": 120203,
    "B": "ANDAMARCA",
    "C": 2688,
    "D": "120200",
    "E": "Andamarca"
},
{
    "A": 120204,
    "B": "CHAMBARA",
    "C": 3521,
    "D": "120200",
    "E": "Chambara"
},
{
    "A": 120205,
    "B": "COCHAS",
    "C": 3272,
    "D": "120200",
    "E": "Cochas"
},
{
    "A": 120206,
    "B": "COMAS",
    "C": 3155,
    "D": "120200",
    "E": "Comas"
},
{
    "A": 120207,
    "B": "HEROINAS TOLEDO",
    "C": 3838,
    "D": "120200",
    "E": "Heroinas Toledo"
},
{
    "A": 120208,
    "B": "MANZANARES",
    "C": 3377,
    "D": "120200",
    "E": "Manzanares"
},
{
    "A": 120209,
    "B": "MARISCAL CASTILLA",
    "C": 2619,
    "D": "120200",
    "E": "Mariscal Castilla"
},
{
    "A": 120210,
    "B": "MATAHUASI",
    "C": 3279,
    "D": "120200",
    "E": "Matahuasi"
},
{
    "A": 120211,
    "B": "MITO",
    "C": 3321,
    "D": "120200",
    "E": "Mito"
},
{
    "A": 120212,
    "B": "NUEVE DE JULIO",
    "C": 3314,
    "D": "120200",
    "E": "Nueve De Julio"
},
{
    "A": 120213,
    "B": "ORCOTUNA",
    "C": 3304,
    "D": "120200",
    "E": "Orcotuna"
},
{
    "A": 120214,
    "B": "SAN JOSE DE QUERO",
    "C": 3875,
    "D": "120200",
    "E": "San Jose De Quero"
},
{
    "A": 120215,
    "B": "SANTA ROSA DE OCOPA",
    "C": 3373,
    "D": "120200",
    "E": "Santa Rosa De Ocopa"
},
{
    "A": 120301,
    "B": "CHANCHAMAYO",
    "C": 775,
    "D": "120300",
    "E": "Chanchamayo"
},
{
    "A": 120302,
    "B": "PERENE",
    "C": 621,
    "D": "120300",
    "E": "Perene"
},
{
    "A": 120303,
    "B": "PICHANAQUI",
    "C": 510,
    "D": "120300",
    "E": "Pichanaqui"
},
{
    "A": 120304,
    "B": "SAN LUIS DE SHUARO",
    "C": 744,
    "D": "120300",
    "E": "San Luis De Shuaro"
},
{
    "A": 120305,
    "B": "SAN RAMON",
    "C": 814,
    "D": "120300",
    "E": "San Ramon"
},
{
    "A": 120306,
    "B": "VITOC",
    "C": 1077,
    "D": "120300",
    "E": "Vitoc"
},
{
    "A": 120401,
    "B": "JAUJA",
    "C": 3389,
    "D": "120400",
    "E": "Jauja"
},
{
    "A": 120402,
    "B": "ACOLLA",
    "C": 3465,
    "D": "120400",
    "E": "Acolla"
},
{
    "A": 120403,
    "B": "APATA",
    "C": 3332,
    "D": "120400",
    "E": "Apata"
},
{
    "A": 120404,
    "B": "ATAURA",
    "C": 3340,
    "D": "120400",
    "E": "Ataura"
},
{
    "A": 120405,
    "B": "CANCHAYLLO",
    "C": 3671,
    "D": "120400",
    "E": "Canchayllo"
},
{
    "A": 120406,
    "B": "CURICACA",
    "C": 3620,
    "D": "120400",
    "E": "Curicaca"
},
{
    "A": 120407,
    "B": "EL MANTARO",
    "C": 3350,
    "D": "120400",
    "E": "El Mantaro"
},
{
    "A": 120408,
    "B": "HUAMALI",
    "C": 3336,
    "D": "120400",
    "E": "Huamali"
},
{
    "A": 120409,
    "B": "HUARIPAMPA",
    "C": 3358,
    "D": "120400",
    "E": "Huaripampa"
},
{
    "A": 120410,
    "B": "HUERTAS",
    "C": 3365,
    "D": "120400",
    "E": "Huertas"
},
{
    "A": 120411,
    "B": "JANJAILLO",
    "C": 3966,
    "D": "120400",
    "E": "Janjaillo"
},
{
    "A": 120412,
    "B": "JULCAN",
    "C": 3433,
    "D": "120400",
    "E": "Julcan"
},
{
    "A": 120413,
    "B": "LEONOR ORDOÑEZ",
    "C": 3317,
    "D": "120400",
    "E": "Leonor Ordoñez"
},
{
    "A": 120414,
    "B": "LLOCLLAPAMPA",
    "C": 3527,
    "D": "120400",
    "E": "Llocllapampa"
},
{
    "A": 120415,
    "B": "MARCO",
    "C": 3492,
    "D": "120400",
    "E": "Marco"
},
{
    "A": 120416,
    "B": "MASMA",
    "C": 3478,
    "D": "120400",
    "E": "Masma"
},
{
    "A": 120417,
    "B": "MASMA CHICCHE",
    "C": 3650,
    "D": "120400",
    "E": "Masma Chicche"
},
{
    "A": 120418,
    "B": "MOLINOS",
    "C": 3434,
    "D": "120400",
    "E": "Molinos"
},
{
    "A": 120419,
    "B": "MONOBAMBA",
    "C": 1449,
    "D": "120400",
    "E": "Monobamba"
},
{
    "A": 120420,
    "B": "MUQUI",
    "C": 3331,
    "D": "120400",
    "E": "Muqui"
},
{
    "A": 120421,
    "B": "MUQUIYAUYO",
    "C": 3347,
    "D": "120400",
    "E": "Muquiyauyo"
},
{
    "A": 120422,
    "B": "PACA",
    "C": 3404,
    "D": "120400",
    "E": "Paca"
},
{
    "A": 120423,
    "B": "PACCHA",
    "C": 3835,
    "D": "120400",
    "E": "Paccha"
},
{
    "A": 120424,
    "B": "PANCAN",
    "C": 3374,
    "D": "120400",
    "E": "Pancan"
},
{
    "A": 120425,
    "B": "PARCO",
    "C": 3556,
    "D": "120400",
    "E": "Parco"
},
{
    "A": 120426,
    "B": "POMACANCHA",
    "C": 3819,
    "D": "120400",
    "E": "Pomacancha"
},
{
    "A": 120427,
    "B": "RICRAN",
    "C": 3777,
    "D": "120400",
    "E": "Ricran"
},
{
    "A": 120428,
    "B": "SAN LORENZO",
    "C": 3308,
    "D": "120400",
    "E": "San Lorenzo"
},
{
    "A": 120429,
    "B": "SAN PEDRO DE CHUNAN",
    "C": 3401,
    "D": "120400",
    "E": "San Pedro De Chunan"
},
{
    "A": 120430,
    "B": "SAUSA",
    "C": 3376,
    "D": "120400",
    "E": "Sausa"
},
{
    "A": 120431,
    "B": "SINCOS",
    "C": 3311,
    "D": "120400",
    "E": "Sincos"
},
{
    "A": 120432,
    "B": "TUNAN MARCA",
    "C": 3567,
    "D": "120400",
    "E": "Tunan Marca"
},
{
    "A": 120433,
    "B": "YAULI",
    "C": 3425,
    "D": "120400",
    "E": "Yauli"
},
{
    "A": 120434,
    "B": "YAUYOS",
    "C": 3384,
    "D": "120400",
    "E": "Yauyos"
},
{
    "A": 120501,
    "B": "JUNIN",
    "C": 4113,
    "D": "120500",
    "E": "Junin"
},
{
    "A": 120502,
    "B": "CARHUAMAYO",
    "C": 4126,
    "D": "120500",
    "E": "Carhuamayo"
},
{
    "A": 120503,
    "B": "ONDORES",
    "C": 4105,
    "D": "120500",
    "E": "Ondores"
},
{
    "A": 120504,
    "B": "ULCUMAYO",
    "C": 3611,
    "D": "120500",
    "E": "Ulcumayo"
},
{
    "A": 120601,
    "B": "SATIPO",
    "C": 628,
    "D": "120600",
    "E": "Satipo"
},
{
    "A": 120602,
    "B": "COVIRIALI",
    "C": 690,
    "D": "120600",
    "E": "Coviriali"
},
{
    "A": 120603,
    "B": "LLAYLLA",
    "C": 1100,
    "D": "120600",
    "E": "Llaylla"
},
{
    "A": 120604,
    "B": "MAZAMARI",
    "C": 700,
    "D": "120600",
    "E": "Mazamari"
},
{
    "A": 120605,
    "B": "PAMPA HERMOSA",
    "C": 1253,
    "D": "120600",
    "E": "Pampa Hermosa"
},
{
    "A": 120606,
    "B": "PANGOA",
    "C": 700,
    "D": "120600",
    "E": "Pangoa"
},
{
    "A": 120607,
    "B": "RIO NEGRO",
    "C": 644,
    "D": "120600",
    "E": "Rio Negro"
},
{
    "A": 120608,
    "B": "RIO TAMBO",
    "C": 362,
    "D": "120600",
    "E": "Rio Tambo"
},
{
    "A": 120701,
    "B": "TARMA",
    "C": 3059,
    "D": "120700",
    "E": "Tarma"
},
{
    "A": 120702,
    "B": "ACOBAMBA",
    "C": 2950,
    "D": "120700",
    "E": "Acobamba"
},
{
    "A": 120703,
    "B": "HUARICOLCA",
    "C": 3762,
    "D": "120700",
    "E": "Huaricolca"
},
{
    "A": 120704,
    "B": "HUASAHUASI",
    "C": 2827,
    "D": "120700",
    "E": "Huasahuasi"
},
{
    "A": 120705,
    "B": "LA UNION",
    "C": 3505,
    "D": "120700",
    "E": "La Union"
},
{
    "A": 120706,
    "B": "PALCA",
    "C": 2742,
    "D": "120700",
    "E": "Palca"
},
{
    "A": 120707,
    "B": "PALCAMAYO",
    "C": 3338,
    "D": "120700",
    "E": "Palcamayo"
},
{
    "A": 120708,
    "B": "SAN PEDRO DE CAJAS",
    "C": 4024,
    "D": "120700",
    "E": "San Pedro De Cajas"
},
{
    "A": 120709,
    "B": "TAPO",
    "C": 3127,
    "D": "120700",
    "E": "Tapo"
},
{
    "A": 120801,
    "B": "LA OROYA",
    "C": 3725,
    "D": "120800",
    "E": "La Oroya"
},
{
    "A": 120802,
    "B": "CHACAPALPA",
    "C": 3752,
    "D": "120800",
    "E": "Chacapalpa"
},
{
    "A": 120803,
    "B": "HUAY-HUAY",
    "C": 3969,
    "D": "120800",
    "E": "Huay-Huay"
},
{
    "A": 120804,
    "B": "MARCAPOMACOCHA",
    "C": 4412,
    "D": "120800",
    "E": "Marcapomacocha"
},
{
    "A": 120805,
    "B": "MOROCOCHA",
    "C": 4524,
    "D": "120800",
    "E": "Morococha"
},
{
    "A": 120806,
    "B": "PACCHA",
    "C": 3987,
    "D": "120800",
    "E": "Paccha"
},
{
    "A": 120807,
    "B": "SANTA BARBARA DE CARHUACAYAN",
    "C": 4126,
    "D": "120800",
    "E": "Santa Barbara De Carhuacayan"
},
{
    "A": 120808,
    "B": "SANTA ROSA DE SACCO",
    "C": 3814,
    "D": "120800",
    "E": "Santa Rosa De Sacco"
},
{
    "A": 120809,
    "B": "SUITUCANCHA",
    "C": 4259,
    "D": "120800",
    "E": "Suitucancha"
},
{
    "A": 120810,
    "B": "YAULI",
    "C": 4140,
    "D": "120800",
    "E": "Yauli"
},
{
    "A": 120901,
    "B": "CHUPACA",
    "C": 3281,
    "D": "120900",
    "E": "Chupaca"
},
{
    "A": 120902,
    "B": "AHUAC",
    "C": 3295,
    "D": "120900",
    "E": "Ahuac"
},
{
    "A": 120903,
    "B": "CHONGOS BAJO",
    "C": 3272,
    "D": "120900",
    "E": "Chongos Bajo"
},
{
    "A": 120904,
    "B": "HUACHAC",
    "C": 3361,
    "D": "120900",
    "E": "Huachac"
},
{
    "A": 120905,
    "B": "HUAMANCACA CHICO",
    "C": 3193,
    "D": "120900",
    "E": "Huamancaca Chico"
},
{
    "A": 120906,
    "B": "SAN JUAN DE ISCOS",
    "C": 3256,
    "D": "120900",
    "E": "San Juan De Iscos"
},
{
    "A": 120907,
    "B": "SAN JUAN DE JARPA",
    "C": 3737,
    "D": "120900",
    "E": "San Juan De Jarpa"
},
{
    "A": 120908,
    "B": "TRES DE DICIEMBRE",
    "C": 3213,
    "D": "120900",
    "E": "Tres De Diciembre"
},
{
    "A": 120909,
    "B": "YANACANCHA",
    "C": 3824,
    "D": "120900",
    "E": "Yanacancha"
},
{
    "A": 130101,
    "B": "TRUJILLO",
    "C": 34,
    "D": "130100",
    "E": "Trujillo"
},
{
    "A": 130102,
    "B": "EL PORVENIR",
    "C": 104,
    "D": "130100",
    "E": "El Porvenir"
},
{
    "A": 130103,
    "B": "FLORENCIA DE MORA",
    "C": 102,
    "D": "130100",
    "E": "Florencia De Mora"
},
{
    "A": 130104,
    "B": "HUANCHACO",
    "C": 7,
    "D": "130100",
    "E": "Huanchaco"
},
{
    "A": 130105,
    "B": "LA ESPERANZA",
    "C": 87,
    "D": "130100",
    "E": "La Esperanza"
},
{
    "A": 130106,
    "B": "LAREDO",
    "C": 95,
    "D": "130100",
    "E": "Laredo"
},
{
    "A": 130107,
    "B": "MOCHE",
    "C": 10,
    "D": "130100",
    "E": "Moche"
},
{
    "A": 130108,
    "B": "POROTO",
    "C": 633,
    "D": "130100",
    "E": "Poroto"
},
{
    "A": 130109,
    "B": "SALAVERRY",
    "C": 6,
    "D": "130100",
    "E": "Salaverry"
},
{
    "A": 130110,
    "B": "SIMBAL",
    "C": 574,
    "D": "130100",
    "E": "Simbal"
},
{
    "A": 130111,
    "B": "VICTOR LARCO HERRERA",
    "C": 8,
    "D": "130100",
    "E": "Victor Larco Herrera"
},
{
    "A": 130201,
    "B": "ASCOPE",
    "C": 238,
    "D": "130200",
    "E": "Ascope"
},
{
    "A": 130202,
    "B": "CHICAMA",
    "C": 129,
    "D": "130200",
    "E": "Chicama"
},
{
    "A": 130203,
    "B": "CHOCOPE",
    "C": 106,
    "D": "130200",
    "E": "Chocope"
},
{
    "A": 130204,
    "B": "MAGDALENA DE CAO",
    "C": 28,
    "D": "130200",
    "E": "Magdalena De Cao"
},
{
    "A": 130205,
    "B": "PAIJAN",
    "C": 94,
    "D": "130200",
    "E": "Paijan"
},
{
    "A": 130206,
    "B": "RAZURI",
    "C": 10,
    "D": "130200",
    "E": "Razuri"
},
{
    "A": 130207,
    "B": "SANTIAGO DE CAO",
    "C": 18,
    "D": "130200",
    "E": "Santiago De Cao"
},
{
    "A": 130208,
    "B": "CASA GRANDE",
    "C": 145,
    "D": "130200",
    "E": "Casa Grande"
},
{
    "A": 130301,
    "B": "BOLIVAR",
    "C": 3098,
    "D": "130300",
    "E": "Bolivar"
},
{
    "A": 130302,
    "B": "BAMBAMARCA",
    "C": 3470,
    "D": "130300",
    "E": "Bambamarca"
},
{
    "A": 130303,
    "B": "CONDORMARCA",
    "C": 2999,
    "D": "130300",
    "E": "Condormarca"
},
{
    "A": 130304,
    "B": "LONGOTEA",
    "C": 2545,
    "D": "130300",
    "E": "Longotea"
},
{
    "A": 130305,
    "B": "UCHUMARCA",
    "C": 3062,
    "D": "130300",
    "E": "Uchumarca"
},
{
    "A": 130306,
    "B": "UCUNCHA",
    "C": 2492,
    "D": "130300",
    "E": "Ucuncha"
},
{
    "A": 130401,
    "B": "CHEPEN",
    "C": 135,
    "D": "130400",
    "E": "Chepen"
},
{
    "A": 130402,
    "B": "PACANGA",
    "C": 89,
    "D": "130400",
    "E": "Pacanga"
},
{
    "A": 130403,
    "B": "PUEBLO NUEVO",
    "C": 75,
    "D": "130400",
    "E": "Pueblo Nuevo"
},
{
    "A": 130501,
    "B": "JULCAN",
    "C": 3412,
    "D": "130500",
    "E": "Julcan"
},
{
    "A": 130502,
    "B": "CALAMARCA",
    "C": 3314,
    "D": "130500",
    "E": "Calamarca"
},
{
    "A": 130503,
    "B": "CARABAMBA",
    "C": 3335,
    "D": "130500",
    "E": "Carabamba"
},
{
    "A": 130504,
    "B": "HUASO",
    "C": 3113,
    "D": "130500",
    "E": "Huaso"
},
{
    "A": 130601,
    "B": "OTUZCO",
    "C": 2660,
    "D": "130600",
    "E": "Otuzco"
},
{
    "A": 130602,
    "B": "AGALLPAMPA",
    "C": 3119,
    "D": "130600",
    "E": "Agallpampa"
},
{
    "A": 130604,
    "B": "CHARAT",
    "C": 2285,
    "D": "130600",
    "E": "Charat"
},
{
    "A": 130605,
    "B": "HUARANCHAL",
    "C": 2110,
    "D": "130600",
    "E": "Huaranchal"
},
{
    "A": 130606,
    "B": "LA CUESTA",
    "C": 1879,
    "D": "130600",
    "E": "La Cuesta"
},
{
    "A": 130608,
    "B": "MACHE",
    "C": 3301,
    "D": "130600",
    "E": "Mache"
},
{
    "A": 130610,
    "B": "PARANDAY",
    "C": 3156,
    "D": "130600",
    "E": "Paranday"
},
{
    "A": 130611,
    "B": "SALPO",
    "C": 3433,
    "D": "130600",
    "E": "Salpo"
},
{
    "A": 130613,
    "B": "SINSICAP",
    "C": 2280,
    "D": "130600",
    "E": "Sinsicap"
},
{
    "A": 130614,
    "B": "USQUIL",
    "C": 2987,
    "D": "130600",
    "E": "Usquil"
},
{
    "A": 130701,
    "B": "SAN PEDRO DE LLOC",
    "C": 48,
    "D": "130700",
    "E": "San Pedro De Lloc"
},
{
    "A": 130702,
    "B": "GUADALUPE",
    "C": 92,
    "D": "130700",
    "E": "Guadalupe"
},
{
    "A": 130703,
    "B": "JEQUETEPEQUE",
    "C": 24,
    "D": "130700",
    "E": "Jequetepeque"
},
{
    "A": 130704,
    "B": "PACASMAYO",
    "C": 7,
    "D": "130700",
    "E": "Pacasmayo"
},
{
    "A": 130705,
    "B": "SAN JOSE",
    "C": 106,
    "D": "130700",
    "E": "San Jose"
},
{
    "A": 130801,
    "B": "TAYABAMBA",
    "C": 3290,
    "D": "130800",
    "E": "Tayabamba"
},
{
    "A": 130802,
    "B": "BULDIBUYO",
    "C": 3201,
    "D": "130800",
    "E": "Buldibuyo"
},
{
    "A": 130803,
    "B": "CHILLIA",
    "C": 3120,
    "D": "130800",
    "E": "Chillia"
},
{
    "A": 130804,
    "B": "HUANCASPATA",
    "C": 3361,
    "D": "130800",
    "E": "Huancaspata"
},
{
    "A": 130805,
    "B": "HUAYLILLAS",
    "C": 2359,
    "D": "130800",
    "E": "Huaylillas"
},
{
    "A": 130806,
    "B": "HUAYO",
    "C": 2280,
    "D": "130800",
    "E": "Huayo"
},
{
    "A": 130807,
    "B": "ONGON",
    "C": 1245,
    "D": "130800",
    "E": "Ongon"
},
{
    "A": 130808,
    "B": "PARCOY",
    "C": 3182,
    "D": "130800",
    "E": "Parcoy"
},
{
    "A": 130809,
    "B": "PATAZ",
    "C": 2638,
    "D": "130800",
    "E": "Pataz"
},
{
    "A": 130810,
    "B": "PIAS",
    "C": 2650,
    "D": "130800",
    "E": "Pias"
},
{
    "A": 130811,
    "B": "SANTIAGO DE CHALLAS",
    "C": 3226,
    "D": "130800",
    "E": "Santiago De Challas"
},
{
    "A": 130812,
    "B": "TAURIJA",
    "C": 3089,
    "D": "130800",
    "E": "Taurija"
},
{
    "A": 130813,
    "B": "URPAY",
    "C": 2690,
    "D": "130800",
    "E": "Urpay"
},
{
    "A": 130901,
    "B": "HUAMACHUCO",
    "C": 3185,
    "D": "130900",
    "E": "Huamachuco"
},
{
    "A": 130902,
    "B": "CHUGAY",
    "C": 3378,
    "D": "130900",
    "E": "Chugay"
},
{
    "A": 130903,
    "B": "COCHORCO",
    "C": 2578,
    "D": "130900",
    "E": "Cochorco"
},
{
    "A": 130904,
    "B": "CURGOS",
    "C": 3231,
    "D": "130900",
    "E": "Curgos"
},
{
    "A": 130905,
    "B": "MARCABAL",
    "C": 2942,
    "D": "130900",
    "E": "Marcabal"
},
{
    "A": 130906,
    "B": "SANAGORAN",
    "C": 2672,
    "D": "130900",
    "E": "Sanagoran"
},
{
    "A": 130907,
    "B": "SARIN",
    "C": 2819,
    "D": "130900",
    "E": "Sarin"
},
{
    "A": 130908,
    "B": "SARTIMBAMBA",
    "C": 2718,
    "D": "130900",
    "E": "Sartimbamba"
},
{
    "A": 131001,
    "B": "SANTIAGO DE CHUCO",
    "C": 3127,
    "D": "131000",
    "E": "Santiago De Chuco"
},
{
    "A": 131002,
    "B": "ANGASMARCA",
    "C": 2870,
    "D": "131000",
    "E": "Angasmarca"
},
{
    "A": 131003,
    "B": "CACHICADAN",
    "C": 2918,
    "D": "131000",
    "E": "Cachicadan"
},
{
    "A": 131004,
    "B": "MOLLEBAMBA",
    "C": 3060,
    "D": "131000",
    "E": "Mollebamba"
},
{
    "A": 131005,
    "B": "MOLLEPATA",
    "C": 2755,
    "D": "131000",
    "E": "Mollepata"
},
{
    "A": 131006,
    "B": "QUIRUVILCA",
    "C": 3979,
    "D": "131000",
    "E": "Quiruvilca"
},
{
    "A": 131007,
    "B": "SANTA CRUZ DE CHUCA",
    "C": 2930,
    "D": "131000",
    "E": "Santa Cruz De Chuca"
},
{
    "A": 131008,
    "B": "SITABAMBA",
    "C": 3113,
    "D": "131000",
    "E": "Sitabamba"
},
{
    "A": 131101,
    "B": "CASCAS",
    "C": 1279,
    "D": "131100",
    "E": "Cascas"
},
{
    "A": 131102,
    "B": "LUCMA",
    "C": 2167,
    "D": "131100",
    "E": "Lucma"
},
{
    "A": 131103,
    "B": "MARMOT",
    "C": 2200,
    "D": "131100",
    "E": "Marmot"
},
{
    "A": 131104,
    "B": "SAYAPULLO",
    "C": 2381,
    "D": "131100",
    "E": "Sayapullo"
},
{
    "A": 131201,
    "B": "VIRU",
    "C": 76,
    "D": "131200",
    "E": "Viru"
},
{
    "A": 131202,
    "B": "CHAO",
    "C": 75,
    "D": "131200",
    "E": "Chao"
},
{
    "A": 131203,
    "B": "GUADALUPITO",
    "C": 26,
    "D": "131200",
    "E": "Guadalupito"
},
{
    "A": 140101,
    "B": "CHICLAYO",
    "C": 34,
    "D": "140100",
    "E": "Chiclayo"
},
{
    "A": 140102,
    "B": "CHONGOYAPE",
    "C": 216,
    "D": "140100",
    "E": "Chongoyape"
},
{
    "A": 140103,
    "B": "ETEN",
    "C": 6,
    "D": "140100",
    "E": "Eten"
},
{
    "A": 140104,
    "B": "ETEN PUERTO",
    "C": 5,
    "D": "140100",
    "E": "Eten Puerto"
},
{
    "A": 140105,
    "B": "JOSE LEONARDO ORTIZ",
    "C": 31,
    "D": "140100",
    "E": "Jose Leonardo Ortiz"
},
{
    "A": 140106,
    "B": "LA VICTORIA",
    "C": 28,
    "D": "140100",
    "E": "La Victoria"
},
{
    "A": 140107,
    "B": "LAGUNAS",
    "C": 34,
    "D": "140100",
    "E": "Lagunas"
},
{
    "A": 140108,
    "B": "MONSEFU",
    "C": 13,
    "D": "140100",
    "E": "Monsefu"
},
{
    "A": 140109,
    "B": "NUEVA ARICA",
    "C": 175,
    "D": "140100",
    "E": "Nueva Arica"
},
{
    "A": 140110,
    "B": "OYOTUN",
    "C": 220,
    "D": "140100",
    "E": "Oyotun"
},
{
    "A": 140111,
    "B": "PICSI",
    "C": 44,
    "D": "140100",
    "E": "Picsi"
},
{
    "A": 140112,
    "B": "PIMENTEL",
    "C": 9,
    "D": "140100",
    "E": "Pimentel"
},
{
    "A": 140113,
    "B": "REQUE",
    "C": 24,
    "D": "140100",
    "E": "Reque"
},
{
    "A": 140114,
    "B": "SANTA ROSA",
    "C": 4,
    "D": "140100",
    "E": "Santa Rosa"
},
{
    "A": 140115,
    "B": "SAÑA",
    "C": 58,
    "D": "140100",
    "E": "Saña"
},
{
    "A": 140116,
    "B": "CAYALTI",
    "C": 75,
    "D": "140100",
    "E": "Cayalti"
},
{
    "A": 140117,
    "B": "PATAPO",
    "C": 88,
    "D": "140100",
    "E": "Patapo"
},
{
    "A": 140118,
    "B": "POMALCA",
    "C": 48,
    "D": "140100",
    "E": "Pomalca"
},
{
    "A": 140119,
    "B": "PUCALA",
    "C": 88,
    "D": "140100",
    "E": "Pucala"
},
{
    "A": 140120,
    "B": "TUMAN",
    "C": 59,
    "D": "140100",
    "E": "Tuman"
},
{
    "A": 140201,
    "B": "FERREÑAFE",
    "C": 42,
    "D": "140200",
    "E": "Ferreñafe"
},
{
    "A": 140202,
    "B": "CAÑARIS",
    "C": 2416,
    "D": "140200",
    "E": "Cañaris"
},
{
    "A": 140203,
    "B": "INCAHUASI",
    "C": 3032,
    "D": "140200",
    "E": "Incahuasi"
},
{
    "A": 140204,
    "B": "MANUEL ANTONIO MESONES MURO",
    "C": 65,
    "D": "140200",
    "E": "Manuel Antonio Mesones Muro"
},
{
    "A": 140205,
    "B": "PITIPO",
    "C": 50,
    "D": "140200",
    "E": "Pitipo"
},
{
    "A": 140206,
    "B": "PUEBLO NUEVO",
    "C": 39,
    "D": "140200",
    "E": "Pueblo Nuevo"
},
{
    "A": 140301,
    "B": "LAMBAYEQUE",
    "C": 20,
    "D": "140300",
    "E": "Lambayeque"
},
{
    "A": 140302,
    "B": "CHOCHOPE",
    "C": 200,
    "D": "140300",
    "E": "Chochope"
},
{
    "A": 140303,
    "B": "ILLIMO",
    "C": 53,
    "D": "140300",
    "E": "Illimo"
},
{
    "A": 140304,
    "B": "JAYANCA",
    "C": 66,
    "D": "140300",
    "E": "Jayanca"
},
{
    "A": 140305,
    "B": "MOCHUMI",
    "C": 39,
    "D": "140300",
    "E": "Mochumi"
},
{
    "A": 140306,
    "B": "MORROPE",
    "C": 21,
    "D": "140300",
    "E": "Morrope"
},
{
    "A": 140307,
    "B": "MOTUPE",
    "C": 132,
    "D": "140300",
    "E": "Motupe"
},
{
    "A": 140308,
    "B": "OLMOS",
    "C": 174,
    "D": "140300",
    "E": "Olmos"
},
{
    "A": 140309,
    "B": "PACORA",
    "C": 57,
    "D": "140300",
    "E": "Pacora"
},
{
    "A": 140310,
    "B": "SALAS",
    "C": 166,
    "D": "140300",
    "E": "Salas"
},
{
    "A": 140311,
    "B": "SAN JOSE",
    "C": 10,
    "D": "140300",
    "E": "San Jose"
},
{
    "A": 140312,
    "B": "TUCUME",
    "C": 45,
    "D": "140300",
    "E": "Tucume"
},
{
    "A": 150101,
    "B": "LIMA",
    "C": 161,
    "D": "150100",
    "E": "Lima"
},
{
    "A": 150102,
    "B": "ANCON",
    "C": 12,
    "D": "150100",
    "E": "Ancon"
},
{
    "A": 150103,
    "B": "ATE",
    "C": 349,
    "D": "150100",
    "E": "Ate"
},
{
    "A": 150104,
    "B": "BARRANCO",
    "C": 5,
    "D": "150100",
    "E": "Barranco"
},
{
    "A": 150105,
    "B": "BREÑA",
    "C": 120,
    "D": "150100",
    "E": "Breña"
},
{
    "A": 150106,
    "B": "CARABAYLLO",
    "C": 221,
    "D": "150100",
    "E": "Carabayllo"
},
{
    "A": 150107,
    "B": "CHACLACAYO",
    "C": 676,
    "D": "150100",
    "E": "Chaclacayo"
},
{
    "A": 150108,
    "B": "CHORRILLOS",
    "C": 46,
    "D": "150100",
    "E": "Chorrillos"
},
{
    "A": 150109,
    "B": "CIENEGUILLA",
    "C": 267,
    "D": "150100",
    "E": "Cieneguilla"
},
{
    "A": 150110,
    "B": "COMAS",
    "C": 101,
    "D": "150100",
    "E": "Comas"
},
{
    "A": 150111,
    "B": "EL AGUSTINO",
    "C": 210,
    "D": "150100",
    "E": "El Agustino"
},
{
    "A": 150112,
    "B": "INDEPENDENCIA",
    "C": 85,
    "D": "150100",
    "E": "Independencia"
},
{
    "A": 150113,
    "B": "JESUS MARIA",
    "C": 121,
    "D": "150100",
    "E": "Jesus Maria"
},
{
    "A": 150114,
    "B": "LA MOLINA",
    "C": 255,
    "D": "150100",
    "E": "La Molina"
},
{
    "A": 150115,
    "B": "LA VICTORIA",
    "C": 142,
    "D": "150100",
    "E": "La Victoria"
},
{
    "A": 150116,
    "B": "LINCE",
    "C": 126,
    "D": "150100",
    "E": "Lince"
},
{
    "A": 150117,
    "B": "LOS OLIVOS",
    "C": 63,
    "D": "150100",
    "E": "Los Olivos"
},
{
    "A": 150118,
    "B": "LURIGANCHO",
    "C": 911,
    "D": "150100",
    "E": "Lurigancho"
},
{
    "A": 150119,
    "B": "LURIN",
    "C": 10,
    "D": "150100",
    "E": "Lurin"
},
{
    "A": 150120,
    "B": "MAGDALENA DEL MAR",
    "C": 27,
    "D": "150100",
    "E": "Magdalena Del Mar"
},
{
    "A": 150121,
    "B": "PUEBLO LIBRE",
    "C": 93,
    "D": "150100",
    "E": "Pueblo Libre"
},
{
    "A": 150122,
    "B": "MIRAFLORES",
    "C": 69,
    "D": "150100",
    "E": "Miraflores"
},
{
    "A": 150123,
    "B": "PACHACAMAC",
    "C": 73,
    "D": "150100",
    "E": "Pachacamac"
},
{
    "A": 150124,
    "B": "PUCUSANA",
    "C": 13,
    "D": "150100",
    "E": "Pucusana"
},
{
    "A": 150125,
    "B": "PUENTE PIEDRA",
    "C": 200,
    "D": "150100",
    "E": "Puente Piedra"
},
{
    "A": 150126,
    "B": "PUNTA HERMOSA",
    "C": 16,
    "D": "150100",
    "E": "Punta Hermosa"
},
{
    "A": 150127,
    "B": "PUNTA NEGRA",
    "C": 19,
    "D": "150100",
    "E": "Punta Negra"
},
{
    "A": 150128,
    "B": "RIMAC",
    "C": 127,
    "D": "150100",
    "E": "Rimac"
},
{
    "A": 150129,
    "B": "SAN BARTOLO",
    "C": 24,
    "D": "150100",
    "E": "San Bartolo"
},
{
    "A": 150130,
    "B": "SAN BORJA",
    "C": 143,
    "D": "150100",
    "E": "San Borja"
},
{
    "A": 150131,
    "B": "SAN ISIDRO",
    "C": 108,
    "D": "150100",
    "E": "San Isidro"
},
{
    "A": 150132,
    "B": "SAN JUAN DE LURIGANCHO",
    "C": 205,
    "D": "150100",
    "E": "San Juan De Lurigancho"
},
{
    "A": 150133,
    "B": "SAN JUAN DE MIRAFLORES",
    "C": 108,
    "D": "150100",
    "E": "San Juan De Miraflores"
},
{
    "A": 150134,
    "B": "SAN LUIS",
    "C": 189,
    "D": "150100",
    "E": "San Luis"
},
{
    "A": 150135,
    "B": "SAN MARTIN DE PORRES",
    "C": 123,
    "D": "150100",
    "E": "San Martin De Porres"
},
{
    "A": 150136,
    "B": "SAN MIGUEL",
    "C": 45,
    "D": "150100",
    "E": "San Miguel"
},
{
    "A": 150137,
    "B": "SANTA ANITA",
    "C": 240,
    "D": "150100",
    "E": "Santa Anita"
},
{
    "A": 150138,
    "B": "SANTA MARIA DEL MAR",
    "C": 28,
    "D": "150100",
    "E": "Santa Maria Del Mar"
},
{
    "A": 150139,
    "B": "SANTA ROSA",
    "C": 7,
    "D": "150100",
    "E": "Santa Rosa"
},
{
    "A": 150140,
    "B": "SANTIAGO DE SURCO",
    "C": 72,
    "D": "150100",
    "E": "Santiago De Surco"
},
{
    "A": 150141,
    "B": "SURQUILLO",
    "C": 122,
    "D": "150100",
    "E": "Surquillo"
},
{
    "A": 150142,
    "B": "VILLA EL SALVADOR",
    "C": 143,
    "D": "150100",
    "E": "Villa El Salvador"
},
{
    "A": 150143,
    "B": "VILLA MARIA DEL TRIUNFO",
    "C": 163,
    "D": "150100",
    "E": "Villa Maria Del Triunfo"
},
{
    "A": 150201,
    "B": "BARRANCA",
    "C": 51,
    "D": "150200",
    "E": "Barranca"
},
{
    "A": 150202,
    "B": "PARAMONGA",
    "C": 19,
    "D": "150200",
    "E": "Paramonga"
},
{
    "A": 150203,
    "B": "PATIVILCA",
    "C": 88,
    "D": "150200",
    "E": "Pativilca"
},
{
    "A": 150204,
    "B": "SUPE",
    "C": 51,
    "D": "150200",
    "E": "Supe"
},
{
    "A": 150205,
    "B": "SUPE PUERTO",
    "C": 4,
    "D": "150200",
    "E": "Supe Puerto"
},
{
    "A": 150301,
    "B": "CAJATAMBO",
    "C": 3382,
    "D": "150300",
    "E": "Cajatambo"
},
{
    "A": 150302,
    "B": "COPA",
    "C": 3418,
    "D": "150300",
    "E": "Copa"
},
{
    "A": 150303,
    "B": "GORGOR",
    "C": 3035,
    "D": "150300",
    "E": "Gorgor"
},
{
    "A": 150304,
    "B": "HUANCAPON",
    "C": 3159,
    "D": "150300",
    "E": "Huancapon"
},
{
    "A": 150305,
    "B": "MANAS",
    "C": 2423,
    "D": "150300",
    "E": "Manas"
},
{
    "A": 150401,
    "B": "CANTA",
    "C": 2833,
    "D": "150400",
    "E": "Canta"
},
{
    "A": 150402,
    "B": "ARAHUAY",
    "C": 2462,
    "D": "150400",
    "E": "Arahuay"
},
{
    "A": 150403,
    "B": "HUAMANTANGA",
    "C": 3384,
    "D": "150400",
    "E": "Huamantanga"
},
{
    "A": 150404,
    "B": "HUAROS",
    "C": 3591,
    "D": "150400",
    "E": "Huaros"
},
{
    "A": 150405,
    "B": "LACHAQUI",
    "C": 3754,
    "D": "150400",
    "E": "Lachaqui"
},
{
    "A": 150406,
    "B": "SAN BUENAVENTURA",
    "C": 2696,
    "D": "150400",
    "E": "San Buenaventura"
},
{
    "A": 150407,
    "B": "SANTA ROSA DE QUIVES",
    "C": 919,
    "D": "150400",
    "E": "Santa Rosa De Quives"
},
{
    "A": 150501,
    "B": "SAN VICENTE DE CAÑETE",
    "C": 28,
    "D": "150500",
    "E": "San Vicente De Cañete"
},
{
    "A": 150502,
    "B": "ASIA",
    "C": 46,
    "D": "150500",
    "E": "Asia"
},
{
    "A": 150503,
    "B": "CALANGO",
    "C": 317,
    "D": "150500",
    "E": "Calango"
},
{
    "A": 150504,
    "B": "CERRO AZUL",
    "C": 7,
    "D": "150500",
    "E": "Cerro Azul"
},
{
    "A": 150505,
    "B": "CHILCA",
    "C": 17,
    "D": "150500",
    "E": "Chilca"
},
{
    "A": 150506,
    "B": "COAYLLO",
    "C": 294,
    "D": "150500",
    "E": "Coayllo"
},
{
    "A": 150507,
    "B": "IMPERIAL",
    "C": 91,
    "D": "150500",
    "E": "Imperial"
},
{
    "A": 150508,
    "B": "LUNAHUANA",
    "C": 484,
    "D": "150500",
    "E": "Lunahuana"
},
{
    "A": 150509,
    "B": "MALA",
    "C": 41,
    "D": "150500",
    "E": "Mala"
},
{
    "A": 150510,
    "B": "NUEVO IMPERIAL",
    "C": 154,
    "D": "150500",
    "E": "Nuevo Imperial"
},
{
    "A": 150511,
    "B": "PACARAN",
    "C": 708,
    "D": "150500",
    "E": "Pacaran"
},
{
    "A": 150512,
    "B": "QUILMANA",
    "C": 161,
    "D": "150500",
    "E": "Quilmana"
},
{
    "A": 150513,
    "B": "SAN ANTONIO",
    "C": 45,
    "D": "150500",
    "E": "San Antonio"
},
{
    "A": 150514,
    "B": "SAN LUIS",
    "C": 26,
    "D": "150500",
    "E": "San Luis"
},
{
    "A": 150515,
    "B": "SANTA CRUZ DE FLORES",
    "C": 91,
    "D": "150500",
    "E": "Santa Cruz De Flores"
},
{
    "A": 150516,
    "B": "ZUÑIGA",
    "C": 819,
    "D": "150500",
    "E": "Zuñiga"
},
{
    "A": 150601,
    "B": "HUARAL",
    "C": 186,
    "D": "150600",
    "E": "Huaral"
},
{
    "A": 150602,
    "B": "ATAVILLOS ALTO",
    "C": 3245,
    "D": "150600",
    "E": "Atavillos Alto"
},
{
    "A": 150603,
    "B": "ATAVILLOS BAJO",
    "C": 1869,
    "D": "150600",
    "E": "Atavillos Bajo"
},
{
    "A": 150604,
    "B": "AUCALLAMA",
    "C": 153,
    "D": "150600",
    "E": "Aucallama"
},
{
    "A": 150605,
    "B": "CHANCAY",
    "C": 44,
    "D": "150600",
    "E": "Chancay"
},
{
    "A": 150606,
    "B": "IHUARI",
    "C": 2826,
    "D": "150600",
    "E": "Ihuari"
},
{
    "A": 150607,
    "B": "LAMPIAN",
    "C": 2460,
    "D": "150600",
    "E": "Lampian"
},
{
    "A": 150608,
    "B": "PACARAOS",
    "C": 3396,
    "D": "150600",
    "E": "Pacaraos"
},
{
    "A": 150609,
    "B": "SAN MIGUEL DE ACOS",
    "C": 1594,
    "D": "150600",
    "E": "San Miguel De Acos"
},
{
    "A": 150610,
    "B": "SANTA CRUZ DE ANDAMARCA",
    "C": 3522,
    "D": "150600",
    "E": "Santa Cruz De Andamarca"
},
{
    "A": 150611,
    "B": "SUMBILCA",
    "C": 3367,
    "D": "150600",
    "E": "Sumbilca"
},
{
    "A": 150612,
    "B": "VEINTISIETE DE NOVIEMBRE",
    "C": 2626,
    "D": "150600",
    "E": "Veintisiete De Noviembre"
},
{
    "A": 150701,
    "B": "MATUCANA",
    "C": 2380,
    "D": "150700",
    "E": "Matucana"
},
{
    "A": 150702,
    "B": "ANTIOQUIA",
    "C": 1526,
    "D": "150700",
    "E": "Antioquia"
},
{
    "A": 150703,
    "B": "CALLAHUANCA",
    "C": 1750,
    "D": "150700",
    "E": "Callahuanca"
},
{
    "A": 150704,
    "B": "CARAMPOMA",
    "C": 3476,
    "D": "150700",
    "E": "Carampoma"
},
{
    "A": 150705,
    "B": "CHICLA",
    "C": 3825,
    "D": "150700",
    "E": "Chicla"
},
{
    "A": 150706,
    "B": "CUENCA",
    "C": 2673,
    "D": "150700",
    "E": "Cuenca"
},
{
    "A": 150707,
    "B": "HUACHUPAMPA",
    "C": 2865,
    "D": "150700",
    "E": "Huachupampa"
},
{
    "A": 150708,
    "B": "HUANZA",
    "C": 3352,
    "D": "150700",
    "E": "Huanza"
},
{
    "A": 150709,
    "B": "HUAROCHIRI",
    "C": 3144,
    "D": "150700",
    "E": "Huarochiri"
},
{
    "A": 150710,
    "B": "LAHUAYTAMBO",
    "C": 3270,
    "D": "150700",
    "E": "Lahuaytambo"
},
{
    "A": 150711,
    "B": "LANGA",
    "C": 2789,
    "D": "150700",
    "E": "Langa"
},
{
    "A": 150712,
    "B": "LARAOS",
    "C": 3660,
    "D": "150700",
    "E": "Laraos"
},
{
    "A": 150713,
    "B": "MARIATANA",
    "C": 3533,
    "D": "150700",
    "E": "Mariatana"
},
{
    "A": 150714,
    "B": "RICARDO PALMA",
    "C": 953,
    "D": "150700",
    "E": "Ricardo Palma"
},
{
    "A": 150715,
    "B": "SAN ANDRES DE TUPICOCHA",
    "C": 3303,
    "D": "150700",
    "E": "San Andres De Tupicocha"
},
{
    "A": 150716,
    "B": "SAN ANTONIO",
    "C": 3457,
    "D": "150700",
    "E": "San Antonio"
},
{
    "A": 150717,
    "B": "SAN BARTOLOME",
    "C": 1587,
    "D": "150700",
    "E": "San Bartolome"
},
{
    "A": 150718,
    "B": "SAN DAMIAN",
    "C": 3182,
    "D": "150700",
    "E": "San Damian"
},
{
    "A": 150719,
    "B": "SAN JUAN DE IRIS",
    "C": 3413,
    "D": "150700",
    "E": "San Juan De Iris"
},
{
    "A": 150720,
    "B": "SAN JUAN DE TANTARANCHE",
    "C": 3448,
    "D": "150700",
    "E": "San Juan De Tantaranche"
},
{
    "A": 150721,
    "B": "SAN LORENZO DE QUINTI",
    "C": 2649,
    "D": "150700",
    "E": "San Lorenzo De Quinti"
},
{
    "A": 150722,
    "B": "SAN MATEO",
    "C": 3273,
    "D": "150700",
    "E": "San Mateo"
},
{
    "A": 150723,
    "B": "SAN MATEO DE OTAO",
    "C": 2259,
    "D": "150700",
    "E": "San Mateo De Otao"
},
{
    "A": 150724,
    "B": "SAN PEDRO DE CASTA",
    "C": 3021,
    "D": "150700",
    "E": "San Pedro De Casta"
},
{
    "A": 150725,
    "B": "SAN PEDRO DE HUANCAYRE",
    "C": 3048,
    "D": "150700",
    "E": "San Pedro De Huancayre"
},
{
    "A": 150726,
    "B": "SANGALLAYA",
    "C": 2733,
    "D": "150700",
    "E": "Sangallaya"
},
{
    "A": 150727,
    "B": "SANTA CRUZ DE COCACHACRA",
    "C": 1406,
    "D": "150700",
    "E": "Santa Cruz De Cocachacra"
},
{
    "A": 150728,
    "B": "SANTA EULALIA",
    "C": 1029,
    "D": "150700",
    "E": "Santa Eulalia"
},
{
    "A": 150729,
    "B": "SANTIAGO DE ANCHUCAYA",
    "C": 3336,
    "D": "150700",
    "E": "Santiago De Anchucaya"
},
{
    "A": 150730,
    "B": "SANTIAGO DE TUNA",
    "C": 2850,
    "D": "150700",
    "E": "Santiago De Tuna"
},
{
    "A": 150731,
    "B": "SANTO DOMINGO DE LOS OLLEROS",
    "C": 2830,
    "D": "150700",
    "E": "Santo Domingo De Los Olleros"
},
{
    "A": 150732,
    "B": "SURCO",
    "C": 2008,
    "D": "150700",
    "E": "Surco"
},
{
    "A": 150801,
    "B": "HUACHO",
    "C": 37,
    "D": "150800",
    "E": "Huacho"
},
{
    "A": 150802,
    "B": "AMBAR",
    "C": 2081,
    "D": "150800",
    "E": "Ambar"
},
{
    "A": 150803,
    "B": "CALETA DE CARQUIN",
    "C": 12,
    "D": "150800",
    "E": "Caleta De Carquin"
},
{
    "A": 150804,
    "B": "CHECRAS",
    "C": 3705,
    "D": "150800",
    "E": "Checras"
},
{
    "A": 150805,
    "B": "HUALMAY",
    "C": 41,
    "D": "150800",
    "E": "Hualmay"
},
{
    "A": 150806,
    "B": "HUAURA",
    "C": 71,
    "D": "150800",
    "E": "Huaura"
},
{
    "A": 150807,
    "B": "LEONCIO PRADO",
    "C": 3297,
    "D": "150800",
    "E": "Leoncio Prado"
},
{
    "A": 150808,
    "B": "PACCHO",
    "C": 3237,
    "D": "150800",
    "E": "Paccho"
},
{
    "A": 150809,
    "B": "SANTA LEONOR",
    "C": 3541,
    "D": "150800",
    "E": "Santa Leonor"
},
{
    "A": 150810,
    "B": "SANTA MARIA",
    "C": 75,
    "D": "150800",
    "E": "Santa Maria"
},
{
    "A": 150811,
    "B": "SAYAN",
    "C": 668,
    "D": "150800",
    "E": "Sayan"
},
{
    "A": 150812,
    "B": "VEGUETA",
    "C": 24,
    "D": "150800",
    "E": "Vegueta"
},
{
    "A": 150901,
    "B": "OYON",
    "C": 3619,
    "D": "150900",
    "E": "Oyon"
},
{
    "A": 150902,
    "B": "ANDAJES",
    "C": 3492,
    "D": "150900",
    "E": "Andajes"
},
{
    "A": 150903,
    "B": "CAUJUL",
    "C": 3169,
    "D": "150900",
    "E": "Caujul"
},
{
    "A": 150904,
    "B": "COCHAMARCA",
    "C": 3417,
    "D": "150900",
    "E": "Cochamarca"
},
{
    "A": 150905,
    "B": "NAVAN",
    "C": 3051,
    "D": "150900",
    "E": "Navan"
},
{
    "A": 150906,
    "B": "PACHANGARA",
    "C": 2258,
    "D": "150900",
    "E": "Pachangara"
},
{
    "A": 151001,
    "B": "YAUYOS",
    "C": 2900,
    "D": "151000",
    "E": "Yauyos"
},
{
    "A": 151002,
    "B": "ALIS",
    "C": 3249,
    "D": "151000",
    "E": "Alis"
},
{
    "A": 151003,
    "B": "ALLAUCA",
    "C": 3151,
    "D": "151000",
    "E": "Allauca"
},
{
    "A": 151004,
    "B": "AYAVIRI",
    "C": 3239,
    "D": "151000",
    "E": "Ayaviri"
},
{
    "A": 151005,
    "B": "AZANGARO",
    "C": 3401,
    "D": "151000",
    "E": "Azangaro"
},
{
    "A": 151006,
    "B": "CACRA",
    "C": 2800,
    "D": "151000",
    "E": "Cacra"
},
{
    "A": 151007,
    "B": "CARANIA",
    "C": 3828,
    "D": "151000",
    "E": "Carania"
},
{
    "A": 151008,
    "B": "CATAHUASI",
    "C": 1179,
    "D": "151000",
    "E": "Catahuasi"
},
{
    "A": 151009,
    "B": "CHOCOS",
    "C": 2688,
    "D": "151000",
    "E": "Chocos"
},
{
    "A": 151010,
    "B": "COCHAS",
    "C": 2770,
    "D": "151000",
    "E": "Cochas"
},
{
    "A": 151011,
    "B": "COLONIA",
    "C": 3366,
    "D": "151000",
    "E": "Colonia"
},
{
    "A": 151012,
    "B": "HONGOS",
    "C": 3182,
    "D": "151000",
    "E": "Hongos"
},
{
    "A": 151013,
    "B": "HUAMPARA",
    "C": 2479,
    "D": "151000",
    "E": "Huampara"
},
{
    "A": 151014,
    "B": "HUANCAYA",
    "C": 3586,
    "D": "151000",
    "E": "Huancaya"
},
{
    "A": 151015,
    "B": "HUANGASCAR",
    "C": 2519,
    "D": "151000",
    "E": "Huangascar"
},
{
    "A": 151016,
    "B": "HUANTAN",
    "C": 3298,
    "D": "151000",
    "E": "Huantan"
},
{
    "A": 151017,
    "B": "HUAÑEC",
    "C": 3195,
    "D": "151000",
    "E": "Huañec"
},
{
    "A": 151018,
    "B": "LARAOS",
    "C": 3484,
    "D": "151000",
    "E": "Laraos"
},
{
    "A": 151019,
    "B": "LINCHA",
    "C": 3482,
    "D": "151000",
    "E": "Lincha"
},
{
    "A": 151020,
    "B": "MADEAN",
    "C": 3275,
    "D": "151000",
    "E": "Madean"
},
{
    "A": 151021,
    "B": "MIRAFLORES",
    "C": 3668,
    "D": "151000",
    "E": "Miraflores"
},
{
    "A": 151022,
    "B": "OMAS",
    "C": 1549,
    "D": "151000",
    "E": "Omas"
},
{
    "A": 151023,
    "B": "PUTINZA",
    "C": 1960,
    "D": "151000",
    "E": "Putinza"
},
{
    "A": 151024,
    "B": "QUINCHES",
    "C": 2963,
    "D": "151000",
    "E": "Quinches"
},
{
    "A": 151025,
    "B": "QUINOCAY",
    "C": 2651,
    "D": "151000",
    "E": "Quinocay"
},
{
    "A": 151026,
    "B": "SAN JOAQUIN",
    "C": 2938,
    "D": "151000",
    "E": "San Joaquin"
},
{
    "A": 151027,
    "B": "SAN PEDRO DE PILAS",
    "C": 2653,
    "D": "151000",
    "E": "San Pedro De Pilas"
},
{
    "A": 151028,
    "B": "TANTA",
    "C": 4268,
    "D": "151000",
    "E": "Tanta"
},
{
    "A": 151029,
    "B": "TAURIPAMPA",
    "C": 3504,
    "D": "151000",
    "E": "Tauripampa"
},
{
    "A": 151030,
    "B": "TOMAS",
    "C": 3700,
    "D": "151000",
    "E": "Tomas"
},
{
    "A": 151031,
    "B": "TUPE",
    "C": 2832,
    "D": "151000",
    "E": "Tupe"
},
{
    "A": 151032,
    "B": "VIÑAC",
    "C": 3297,
    "D": "151000",
    "E": "Viñac"
},
{
    "A": 151033,
    "B": "VITIS",
    "C": 3594,
    "D": "151000",
    "E": "Vitis"
},
{
    "A": 160101,
    "B": "IQUITOS",
    "C": 91,
    "D": "160100",
    "E": "Iquitos"
},
{
    "A": 160102,
    "B": "ALTO NANAY",
    "C": 101,
    "D": "160100",
    "E": "Alto Nanay"
},
{
    "A": 160103,
    "B": "FERNANDO LORES",
    "C": 101,
    "D": "160100",
    "E": "Fernando Lores"
},
{
    "A": 160104,
    "B": "INDIANA",
    "C": 90,
    "D": "160100",
    "E": "Indiana"
},
{
    "A": 160105,
    "B": "LAS AMAZONAS",
    "C": 92,
    "D": "160100",
    "E": "Las Amazonas"
},
{
    "A": 160106,
    "B": "MAZAN",
    "C": 103,
    "D": "160100",
    "E": "Mazan"
},
{
    "A": 160107,
    "B": "NAPO",
    "C": 139,
    "D": "160100",
    "E": "Napo"
},
{
    "A": 160108,
    "B": "PUNCHANA",
    "C": 97,
    "D": "160100",
    "E": "Punchana"
},
{
    "A": 160110,
    "B": "TORRES CAUSANA",
    "C": 195,
    "D": "160100",
    "E": "Torres Causana"
},
{
    "A": 160112,
    "B": "BELEN",
    "C": 86,
    "D": "160100",
    "E": "Belen"
},
{
    "A": 160113,
    "B": "SAN JUAN BAUTISTA",
    "C": 96,
    "D": "160100",
    "E": "San Juan Bautista"
},
{
    "A": 160201,
    "B": "YURIMAGUAS",
    "C": 148,
    "D": "160200",
    "E": "Yurimaguas"
},
{
    "A": 160202,
    "B": "BALSAPUERTO",
    "C": 205,
    "D": "160200",
    "E": "Balsapuerto"
},
{
    "A": 160205,
    "B": "JEBEROS",
    "C": 154,
    "D": "160200",
    "E": "Jeberos"
},
{
    "A": 160206,
    "B": "LAGUNAS",
    "C": 125,
    "D": "160200",
    "E": "Lagunas"
},
{
    "A": 160210,
    "B": "SANTA CRUZ",
    "C": 121,
    "D": "160200",
    "E": "Santa Cruz"
},
{
    "A": 160211,
    "B": "TENIENTE CESAR LOPEZ ROJAS",
    "C": 141,
    "D": "160200",
    "E": "Teniente Cesar Lopez Rojas"
},
{
    "A": 160301,
    "B": "NAUTA",
    "C": 98,
    "D": "160300",
    "E": "Nauta"
},
{
    "A": 160302,
    "B": "PARINARI",
    "C": 119,
    "D": "160300",
    "E": "Parinari"
},
{
    "A": 160303,
    "B": "TIGRE",
    "C": 127,
    "D": "160300",
    "E": "Tigre"
},
{
    "A": 160304,
    "B": "TROMPETEROS",
    "C": 124,
    "D": "160300",
    "E": "Trompeteros"
},
{
    "A": 160305,
    "B": "URARINAS",
    "C": 116,
    "D": "160300",
    "E": "Urarinas"
},
{
    "A": 160401,
    "B": "RAMON CASTILLA",
    "C": 74,
    "D": "160400",
    "E": "Ramon Castilla"
},
{
    "A": 160402,
    "B": "PEBAS",
    "C": 99,
    "D": "160400",
    "E": "Pebas"
},
{
    "A": 160403,
    "B": "YAVARI",
    "C": 70,
    "D": "160400",
    "E": "Yavari"
},
{
    "A": 160404,
    "B": "SAN PABLO",
    "C": 74,
    "D": "160400",
    "E": "San Pablo"
},
{
    "A": 160501,
    "B": "REQUENA",
    "C": 95,
    "D": "160500",
    "E": "Requena"
},
{
    "A": 160502,
    "B": "ALTO TAPICHE",
    "C": 115,
    "D": "160500",
    "E": "Alto Tapiche"
},
{
    "A": 160503,
    "B": "CAPELO",
    "C": 105,
    "D": "160500",
    "E": "Capelo"
},
{
    "A": 160504,
    "B": "EMILIO SAN MARTIN",
    "C": 107,
    "D": "160500",
    "E": "Emilio San Martin"
},
{
    "A": 160505,
    "B": "MAQUIA",
    "C": 100,
    "D": "160500",
    "E": "Maquia"
},
{
    "A": 160506,
    "B": "PUINAHUA",
    "C": 106,
    "D": "160500",
    "E": "Puinahua"
},
{
    "A": 160507,
    "B": "SAQUENA",
    "C": 95,
    "D": "160500",
    "E": "Saquena"
},
{
    "A": 160508,
    "B": "SOPLIN",
    "C": 121,
    "D": "160500",
    "E": "Soplin"
},
{
    "A": 160509,
    "B": "TAPICHE",
    "C": 106,
    "D": "160500",
    "E": "Tapiche"
},
{
    "A": 160510,
    "B": "JENARO HERRERA",
    "C": 91,
    "D": "160500",
    "E": "Jenaro Herrera"
},
{
    "A": 160511,
    "B": "YAQUERANA",
    "C": 104,
    "D": "160500",
    "E": "Yaquerana"
},
{
    "A": 160601,
    "B": "CONTAMANA",
    "C": 134,
    "D": "160600",
    "E": "Contamana"
},
{
    "A": 160602,
    "B": "INAHUAYA",
    "C": 150,
    "D": "160600",
    "E": "Inahuaya"
},
{
    "A": 160603,
    "B": "PADRE MARQUEZ",
    "C": 140,
    "D": "160600",
    "E": "Padre Marquez"
},
{
    "A": 160604,
    "B": "PAMPA HERMOSA",
    "C": 132,
    "D": "160600",
    "E": "Pampa Hermosa"
},
{
    "A": 160605,
    "B": "SARAYACU",
    "C": 126,
    "D": "160600",
    "E": "Sarayacu"
},
{
    "A": 160606,
    "B": "VARGAS GUERRA",
    "C": 146,
    "D": "160600",
    "E": "Vargas Guerra"
},
{
    "A": 160701,
    "B": "BARRANCA",
    "C": 133,
    "D": "160700",
    "E": "Barranca"
},
{
    "A": 160702,
    "B": "CAHUAPANAS",
    "C": 165,
    "D": "160700",
    "E": "Cahuapanas"
},
{
    "A": 160703,
    "B": "MANSERICHE",
    "C": 145,
    "D": "160700",
    "E": "Manseriche"
},
{
    "A": 160704,
    "B": "MORONA",
    "C": 149,
    "D": "160700",
    "E": "Morona"
},
{
    "A": 160705,
    "B": "PASTAZA",
    "C": 145,
    "D": "160700",
    "E": "Pastaza"
},
{
    "A": 160706,
    "B": "ANDOAS",
    "C": 165,
    "D": "160700",
    "E": "Andoas"
},
{
    "A": 160801,
    "B": "PUTUMAYO",
    "C": 106,
    "D": "160800",
    "E": "Putumayo"
},
{
    "A": 160802,
    "B": "ROSA PANDURO",
    "C": 161,
    "D": "160800",
    "E": "Rosa Panduro"
},
{
    "A": 160803,
    "B": "TENIENTE MANUEL CLAVERO",
    "C": 199,
    "D": "160800",
    "E": "Teniente Manuel Clavero"
},
{
    "A": 160804,
    "B": "YAGUAS",
    "C": 107,
    "D": "160800",
    "E": "Yaguas"
},
{
    "A": 170101,
    "B": "TAMBOPATA",
    "C": 205,
    "D": "170100",
    "E": "Tambopata"
},
{
    "A": 170102,
    "B": "INAMBARI",
    "C": 353,
    "D": "170100",
    "E": "Inambari"
},
{
    "A": 170103,
    "B": "LAS PIEDRAS",
    "C": 248,
    "D": "170100",
    "E": "Las Piedras"
},
{
    "A": 170104,
    "B": "LABERINTO",
    "C": 197,
    "D": "170100",
    "E": "Laberinto"
},
{
    "A": 170201,
    "B": "MANU",
    "C": 527,
    "D": "170200",
    "E": "Manu"
},
{
    "A": 170202,
    "B": "FITZCARRALD",
    "C": 306,
    "D": "170200",
    "E": "Fitzcarrald"
},
{
    "A": 170203,
    "B": "MADRE DE DIOS",
    "C": 240,
    "D": "170200",
    "E": "Madre De Dios"
},
{
    "A": 170204,
    "B": "HUEPETUHE",
    "C": 417,
    "D": "170200",
    "E": "Huepetuhe"
},
{
    "A": 170301,
    "B": "IÑAPARI",
    "C": 245,
    "D": "170300",
    "E": "Iñapari"
},
{
    "A": 170302,
    "B": "IBERIA",
    "C": 270,
    "D": "170300",
    "E": "Iberia"
},
{
    "A": 170303,
    "B": "TAHUAMANU",
    "C": 259,
    "D": "170300",
    "E": "Tahuamanu"
},
{
    "A": 180101,
    "B": "MOQUEGUA",
    "C": 1417,
    "D": "180100",
    "E": "Moquegua"
},
{
    "A": 180102,
    "B": "CARUMAS",
    "C": 3043,
    "D": "180100",
    "E": "Carumas"
},
{
    "A": 180103,
    "B": "CUCHUMBAYA",
    "C": 3131,
    "D": "180100",
    "E": "Cuchumbaya"
},
{
    "A": 180104,
    "B": "SAMEGUA",
    "C": 1558,
    "D": "180100",
    "E": "Samegua"
},
{
    "A": 180105,
    "B": "SAN CRISTOBAL",
    "C": 3458,
    "D": "180100",
    "E": "San Cristobal"
},
{
    "A": 180106,
    "B": "TORATA",
    "C": 2195,
    "D": "180100",
    "E": "Torata"
},
{
    "A": 180201,
    "B": "OMATE",
    "C": 2160,
    "D": "180200",
    "E": "Omate"
},
{
    "A": 180202,
    "B": "CHOJATA",
    "C": 3620,
    "D": "180200",
    "E": "Chojata"
},
{
    "A": 180203,
    "B": "COALAQUE",
    "C": 2297,
    "D": "180200",
    "E": "Coalaque"
},
{
    "A": 180204,
    "B": "ICHUÑA",
    "C": 3774,
    "D": "180200",
    "E": "Ichuña"
},
{
    "A": 180205,
    "B": "LA CAPILLA",
    "C": 1800,
    "D": "180200",
    "E": "La Capilla"
},
{
    "A": 180206,
    "B": "LLOQUE",
    "C": 3312,
    "D": "180200",
    "E": "Lloque"
},
{
    "A": 180207,
    "B": "MATALAQUE",
    "C": 2635,
    "D": "180200",
    "E": "Matalaque"
},
{
    "A": 180208,
    "B": "PUQUINA",
    "C": 3134,
    "D": "180200",
    "E": "Puquina"
},
{
    "A": 180209,
    "B": "QUINISTAQUILLAS",
    "C": 1815,
    "D": "180200",
    "E": "Quinistaquillas"
},
{
    "A": 180210,
    "B": "UBINAS",
    "C": 3397,
    "D": "180200",
    "E": "Ubinas"
},
{
    "A": 180211,
    "B": "YUNGA",
    "C": 3665,
    "D": "180200",
    "E": "Yunga"
},
{
    "A": 180301,
    "B": "ILO",
    "C": 13,
    "D": "180300",
    "E": "Ilo"
},
{
    "A": 180302,
    "B": "EL ALGARROBAL",
    "C": 92,
    "D": "180300",
    "E": "El Algarrobal"
},
{
    "A": 180303,
    "B": "PACOCHA",
    "C": 23,
    "D": "180300",
    "E": "Pacocha"
},
{
    "A": 190101,
    "B": "CHAUPIMARCA",
    "C": 4342,
    "D": "190100",
    "E": "Chaupimarca"
},
{
    "A": 190102,
    "B": "HUACHON",
    "C": 3407,
    "D": "190100",
    "E": "Huachon"
},
{
    "A": 190103,
    "B": "HUARIACA",
    "C": 2958,
    "D": "190100",
    "E": "Huariaca"
},
{
    "A": 190104,
    "B": "HUAYLLAY",
    "C": 4348,
    "D": "190100",
    "E": "Huayllay"
},
{
    "A": 190105,
    "B": "NINACACA",
    "C": 4141,
    "D": "190100",
    "E": "Ninacaca"
},
{
    "A": 190106,
    "B": "PALLANCHACRA",
    "C": 3132,
    "D": "190100",
    "E": "Pallanchacra"
},
{
    "A": 190107,
    "B": "PAUCARTAMBO",
    "C": 2930,
    "D": "190100",
    "E": "Paucartambo"
},
{
    "A": 190108,
    "B": "SAN FRANCISCO DE ASIS DE YARUSYACAN",
    "C": 3814,
    "D": "190100",
    "E": "San Francisco De Asis De Yarusyacan"
},
{
    "A": 190109,
    "B": "SIMON BOLIVAR",
    "C": 4191,
    "D": "190100",
    "E": "Simon Bolivar"
},
{
    "A": 190110,
    "B": "TICLACAYAN",
    "C": 3531,
    "D": "190100",
    "E": "Ticlacayan"
},
{
    "A": 190111,
    "B": "TINYAHUARCO",
    "C": 4270,
    "D": "190100",
    "E": "Tinyahuarco"
},
{
    "A": 190112,
    "B": "VICCO",
    "C": 4104,
    "D": "190100",
    "E": "Vicco"
},
{
    "A": 190113,
    "B": "YANACANCHA",
    "C": 4334,
    "D": "190100",
    "E": "Yanacancha"
},
{
    "A": 190201,
    "B": "YANAHUANCA",
    "C": 3199,
    "D": "190200",
    "E": "Yanahuanca"
},
{
    "A": 190202,
    "B": "CHACAYAN",
    "C": 3338,
    "D": "190200",
    "E": "Chacayan"
},
{
    "A": 190203,
    "B": "GOYLLARISQUIZGA",
    "C": 4183,
    "D": "190200",
    "E": "Goyllarisquizga"
},
{
    "A": 190204,
    "B": "PAUCAR",
    "C": 3357,
    "D": "190200",
    "E": "Paucar"
},
{
    "A": 190205,
    "B": "SAN PEDRO DE PILLAO",
    "C": 3678,
    "D": "190200",
    "E": "San Pedro De Pillao"
},
{
    "A": 190206,
    "B": "SANTA ANA DE TUSI",
    "C": 3803,
    "D": "190200",
    "E": "Santa Ana De Tusi"
},
{
    "A": 190207,
    "B": "TAPUC",
    "C": 3678,
    "D": "190200",
    "E": "Tapuc"
},
{
    "A": 190208,
    "B": "VILCABAMBA",
    "C": 3530,
    "D": "190200",
    "E": "Vilcabamba"
},
{
    "A": 190301,
    "B": "OXAPAMPA",
    "C": 1806,
    "D": "190300",
    "E": "Oxapampa"
},
{
    "A": 190302,
    "B": "CHONTABAMBA",
    "C": 1865,
    "D": "190300",
    "E": "Chontabamba"
},
{
    "A": 190303,
    "B": "HUANCABAMBA",
    "C": 1666,
    "D": "190300",
    "E": "Huancabamba"
},
{
    "A": 190304,
    "B": "PALCAZU",
    "C": 297,
    "D": "190300",
    "E": "Palcazu"
},
{
    "A": 190305,
    "B": "POZUZO",
    "C": 971,
    "D": "190300",
    "E": "Pozuzo"
},
{
    "A": 190306,
    "B": "PUERTO BERMUDEZ",
    "C": 258,
    "D": "190300",
    "E": "Puerto Bermudez"
},
{
    "A": 190307,
    "B": "VILLA RICA",
    "C": 1495,
    "D": "190300",
    "E": "Villa Rica"
},
{
    "A": 190308,
    "B": "CONSTITUCION",
    "C": 250,
    "D": "190300",
    "E": "Constitucion"
},
{
    "A": 200101,
    "B": "PIURA",
    "C": 36,
    "D": "200100",
    "E": "Piura"
},
{
    "A": 200104,
    "B": "CASTILLA",
    "C": 32,
    "D": "200100",
    "E": "Castilla"
},
{
    "A": 200105,
    "B": "CATACAOS",
    "C": 28,
    "D": "200100",
    "E": "Catacaos"
},
{
    "A": 200107,
    "B": "CURA MORI",
    "C": 28,
    "D": "200100",
    "E": "Cura Mori"
},
{
    "A": 200108,
    "B": "EL TALLAN",
    "C": 15,
    "D": "200100",
    "E": "El Tallan"
},
{
    "A": 200109,
    "B": "LA ARENA",
    "C": 22,
    "D": "200100",
    "E": "La Arena"
},
{
    "A": 200110,
    "B": "LA UNION",
    "C": 16,
    "D": "200100",
    "E": "La Union"
},
{
    "A": 200111,
    "B": "LAS LOMAS",
    "C": 240,
    "D": "200100",
    "E": "Las Lomas"
},
{
    "A": 200114,
    "B": "TAMBO GRANDE",
    "C": 72,
    "D": "200100",
    "E": "Tambo Grande"
},
{
    "A": 200115,
    "B": "VEINTISEIS DE OCTUBRE",
    "C": 36,
    "D": "200100",
    "E": "Veintiseis De Octubre"
},
{
    "A": 200201,
    "B": "AYABACA",
    "C": 2748,
    "D": "200200",
    "E": "Ayabaca"
},
{
    "A": 200202,
    "B": "FRIAS",
    "C": 1755,
    "D": "200200",
    "E": "Frias"
},
{
    "A": 200203,
    "B": "JILILI",
    "C": 1263,
    "D": "200200",
    "E": "Jilili"
},
{
    "A": 200204,
    "B": "LAGUNAS",
    "C": 2185,
    "D": "200200",
    "E": "Lagunas"
},
{
    "A": 200205,
    "B": "MONTERO",
    "C": 1108,
    "D": "200200",
    "E": "Montero"
},
{
    "A": 200206,
    "B": "PACAIPAMPA",
    "C": 1941,
    "D": "200200",
    "E": "Pacaipampa"
},
{
    "A": 200207,
    "B": "PAIMAS",
    "C": 574,
    "D": "200200",
    "E": "Paimas"
},
{
    "A": 200208,
    "B": "SAPILLICA",
    "C": 1428,
    "D": "200200",
    "E": "Sapillica"
},
{
    "A": 200209,
    "B": "SICCHEZ",
    "C": 1363,
    "D": "200200",
    "E": "Sicchez"
},
{
    "A": 200210,
    "B": "SUYO",
    "C": 408,
    "D": "200200",
    "E": "Suyo"
},
{
    "A": 200301,
    "B": "HUANCABAMBA",
    "C": 1933,
    "D": "200300",
    "E": "Huancabamba"
},
{
    "A": 200302,
    "B": "CANCHAQUE",
    "C": 1135,
    "D": "200300",
    "E": "Canchaque"
},
{
    "A": 200303,
    "B": "EL CARMEN DE LA FRONTERA",
    "C": 2449,
    "D": "200300",
    "E": "El Carmen De La Frontera"
},
{
    "A": 200304,
    "B": "HUARMACA",
    "C": 2163,
    "D": "200300",
    "E": "Huarmaca"
},
{
    "A": 200305,
    "B": "LALAQUIZ",
    "C": 960,
    "D": "200300",
    "E": "Lalaquiz"
},
{
    "A": 200306,
    "B": "SAN MIGUEL DE EL FAIQUE",
    "C": 1242,
    "D": "200300",
    "E": "San Miguel De El Faique"
},
{
    "A": 200307,
    "B": "SONDOR",
    "C": 2004,
    "D": "200300",
    "E": "Sondor"
},
{
    "A": 200308,
    "B": "SONDORILLO",
    "C": 1905,
    "D": "200300",
    "E": "Sondorillo"
},
{
    "A": 200401,
    "B": "CHULUCANAS",
    "C": 92,
    "D": "200400",
    "E": "Chulucanas"
},
{
    "A": 200402,
    "B": "BUENOS AIRES",
    "C": 146,
    "D": "200400",
    "E": "Buenos Aires"
},
{
    "A": 200403,
    "B": "CHALACO",
    "C": 2230,
    "D": "200400",
    "E": "Chalaco"
},
{
    "A": 200404,
    "B": "LA MATANZA",
    "C": 112,
    "D": "200400",
    "E": "La Matanza"
},
{
    "A": 200405,
    "B": "MORROPON",
    "C": 130,
    "D": "200400",
    "E": "Morropon"
},
{
    "A": 200406,
    "B": "SALITRAL",
    "C": 159,
    "D": "200400",
    "E": "Salitral"
},
{
    "A": 200407,
    "B": "SAN JUAN DE BIGOTE",
    "C": 184,
    "D": "200400",
    "E": "San Juan De Bigote"
},
{
    "A": 200408,
    "B": "SANTA CATALINA DE MOSSA",
    "C": 796,
    "D": "200400",
    "E": "Santa Catalina De Mossa"
},
{
    "A": 200409,
    "B": "SANTO DOMINGO",
    "C": 1480,
    "D": "200400",
    "E": "Santo Domingo"
},
{
    "A": 200410,
    "B": "YAMANGO",
    "C": 1138,
    "D": "200400",
    "E": "Yamango"
},
{
    "A": 200501,
    "B": "PAITA",
    "C": 36,
    "D": "200500",
    "E": "Paita"
},
{
    "A": 200502,
    "B": "AMOTAPE",
    "C": 15,
    "D": "200500",
    "E": "Amotape"
},
{
    "A": 200503,
    "B": "ARENAL",
    "C": 39,
    "D": "200500",
    "E": "Arenal"
},
{
    "A": 200504,
    "B": "COLAN",
    "C": 11,
    "D": "200500",
    "E": "Colan"
},
{
    "A": 200505,
    "B": "LA HUACA",
    "C": 24,
    "D": "200500",
    "E": "La Huaca"
},
{
    "A": 200506,
    "B": "TAMARINDO",
    "C": 24,
    "D": "200500",
    "E": "Tamarindo"
},
{
    "A": 200507,
    "B": "VICHAYAL",
    "C": 16,
    "D": "200500",
    "E": "Vichayal"
},
{
    "A": 200601,
    "B": "SULLANA",
    "C": 64,
    "D": "200600",
    "E": "Sullana"
},
{
    "A": 200602,
    "B": "BELLAVISTA",
    "C": 58,
    "D": "200600",
    "E": "Bellavista"
},
{
    "A": 200603,
    "B": "IGNACIO ESCUDERO",
    "C": 39,
    "D": "200600",
    "E": "Ignacio Escudero"
},
{
    "A": 200604,
    "B": "LANCONES",
    "C": 104,
    "D": "200600",
    "E": "Lancones"
},
{
    "A": 200605,
    "B": "MARCAVELICA",
    "C": 53,
    "D": "200600",
    "E": "Marcavelica"
},
{
    "A": 200606,
    "B": "MIGUEL CHECA",
    "C": 45,
    "D": "200600",
    "E": "Miguel Checa"
},
{
    "A": 200607,
    "B": "QUERECOTILLO",
    "C": 54,
    "D": "200600",
    "E": "Querecotillo"
},
{
    "A": 200608,
    "B": "SALITRAL",
    "C": 52,
    "D": "200600",
    "E": "Salitral"
},
{
    "A": 200701,
    "B": "PARIÑAS",
    "C": 5,
    "D": "200700",
    "E": "Pariñas"
},
{
    "A": 200702,
    "B": "EL ALTO",
    "C": 273,
    "D": "200700",
    "E": "El Alto"
},
{
    "A": 200703,
    "B": "LA BREA",
    "C": 3,
    "D": "200700",
    "E": "La Brea"
},
{
    "A": 200704,
    "B": "LOBITOS",
    "C": 7,
    "D": "200700",
    "E": "Lobitos"
},
{
    "A": 200705,
    "B": "LOS ORGANOS",
    "C": 15,
    "D": "200700",
    "E": "Los Organos"
},
{
    "A": 200706,
    "B": "MANCORA",
    "C": 23,
    "D": "200700",
    "E": "Mancora"
},
{
    "A": 200801,
    "B": "SECHURA",
    "C": 15,
    "D": "200800",
    "E": "Sechura"
},
{
    "A": 200802,
    "B": "BELLAVISTA DE LA UNION",
    "C": 13,
    "D": "200800",
    "E": "Bellavista De La Union"
},
{
    "A": 200803,
    "B": "BERNAL",
    "C": 16,
    "D": "200800",
    "E": "Bernal"
},
{
    "A": 200804,
    "B": "CRISTO NOS VALGA",
    "C": 13,
    "D": "200800",
    "E": "Cristo Nos Valga"
},
{
    "A": 200805,
    "B": "VICE",
    "C": 16,
    "D": "200800",
    "E": "Vice"
},
{
    "A": 200806,
    "B": "RINCONADA LLICUAR",
    "C": 12,
    "D": "200800",
    "E": "Rinconada Llicuar"
},
{
    "A": 210101,
    "B": "PUNO",
    "C": 3848,
    "D": "210100",
    "E": "Puno"
},
{
    "A": 210102,
    "B": "ACORA",
    "C": 3847,
    "D": "210100",
    "E": "Acora"
},
{
    "A": 210103,
    "B": "AMANTANI",
    "C": 3854,
    "D": "210100",
    "E": "Amantani"
},
{
    "A": 210104,
    "B": "ATUNCOLLA",
    "C": 3831,
    "D": "210100",
    "E": "Atuncolla"
},
{
    "A": 210105,
    "B": "CAPACHICA",
    "C": 3863,
    "D": "210100",
    "E": "Capachica"
},
{
    "A": 210106,
    "B": "CHUCUITO",
    "C": 3875,
    "D": "210100",
    "E": "Chucuito"
},
{
    "A": 210107,
    "B": "COATA",
    "C": 3821,
    "D": "210100",
    "E": "Coata"
},
{
    "A": 210108,
    "B": "HUATA",
    "C": 3842,
    "D": "210100",
    "E": "Huata"
},
{
    "A": 210109,
    "B": "MAÑAZO",
    "C": 3935,
    "D": "210100",
    "E": "Mañazo"
},
{
    "A": 210110,
    "B": "PAUCARCOLLA",
    "C": 3845,
    "D": "210100",
    "E": "Paucarcolla"
},
{
    "A": 210111,
    "B": "PICHACANI",
    "C": 3957,
    "D": "210100",
    "E": "Pichacani"
},
{
    "A": 210112,
    "B": "PLATERIA",
    "C": 3826,
    "D": "210100",
    "E": "Plateria"
},
{
    "A": 210113,
    "B": "SAN ANTONIO",
    "C": 4329,
    "D": "210100",
    "E": "San Antonio"
},
{
    "A": 210114,
    "B": "TIQUILLACA",
    "C": 3885,
    "D": "210100",
    "E": "Tiquillaca"
},
{
    "A": 210115,
    "B": "VILQUE",
    "C": 3873,
    "D": "210100",
    "E": "Vilque"
},
{
    "A": 210201,
    "B": "AZANGARO",
    "C": 3865,
    "D": "210200",
    "E": "Azangaro"
},
{
    "A": 210202,
    "B": "ACHAYA",
    "C": 3846,
    "D": "210200",
    "E": "Achaya"
},
{
    "A": 210203,
    "B": "ARAPA",
    "C": 3829,
    "D": "210200",
    "E": "Arapa"
},
{
    "A": 210204,
    "B": "ASILLO",
    "C": 3913,
    "D": "210200",
    "E": "Asillo"
},
{
    "A": 210205,
    "B": "CAMINACA",
    "C": 3835,
    "D": "210200",
    "E": "Caminaca"
},
{
    "A": 210206,
    "B": "CHUPA",
    "C": 3823,
    "D": "210200",
    "E": "Chupa"
},
{
    "A": 210207,
    "B": "JOSE DOMINGO CHOQUEHUANCA",
    "C": 3888,
    "D": "210200",
    "E": "Jose Domingo Choquehuanca"
},
{
    "A": 210208,
    "B": "MUÑANI",
    "C": 3916,
    "D": "210200",
    "E": "Muñani"
},
{
    "A": 210209,
    "B": "POTONI",
    "C": 4172,
    "D": "210200",
    "E": "Potoni"
},
{
    "A": 210210,
    "B": "SAMAN",
    "C": 3829,
    "D": "210200",
    "E": "Saman"
},
{
    "A": 210211,
    "B": "SAN ANTON",
    "C": 3971,
    "D": "210200",
    "E": "San Anton"
},
{
    "A": 210212,
    "B": "SAN JOSE",
    "C": 4082,
    "D": "210200",
    "E": "San Jose"
},
{
    "A": 210213,
    "B": "SAN JUAN DE SALINAS",
    "C": 3841,
    "D": "210200",
    "E": "San Juan De Salinas"
},
{
    "A": 210214,
    "B": "SANTIAGO DE PUPUJA",
    "C": 3926,
    "D": "210200",
    "E": "Santiago De Pupuja"
},
{
    "A": 210215,
    "B": "TIRAPATA",
    "C": 3886,
    "D": "210200",
    "E": "Tirapata"
},
{
    "A": 210301,
    "B": "MACUSANI",
    "C": 4321,
    "D": "210300",
    "E": "Macusani"
},
{
    "A": 210302,
    "B": "AJOYANI",
    "C": 4255,
    "D": "210300",
    "E": "Ajoyani"
},
{
    "A": 210303,
    "B": "AYAPATA",
    "C": 3501,
    "D": "210300",
    "E": "Ayapata"
},
{
    "A": 210304,
    "B": "COASA",
    "C": 3745,
    "D": "210300",
    "E": "Coasa"
},
{
    "A": 210305,
    "B": "CORANI",
    "C": 4017,
    "D": "210300",
    "E": "Corani"
},
{
    "A": 210306,
    "B": "CRUCERO",
    "C": 4131,
    "D": "210300",
    "E": "Crucero"
},
{
    "A": 210307,
    "B": "ITUATA",
    "C": 3915,
    "D": "210300",
    "E": "Ituata"
},
{
    "A": 210308,
    "B": "OLLACHEA",
    "C": 2774,
    "D": "210300",
    "E": "Ollachea"
},
{
    "A": 210309,
    "B": "SAN GABAN",
    "C": 610,
    "D": "210300",
    "E": "San Gaban"
},
{
    "A": 210310,
    "B": "USICAYOS",
    "C": 3750,
    "D": "210300",
    "E": "Usicayos"
},
{
    "A": 210401,
    "B": "JULI",
    "C": 3868,
    "D": "210400",
    "E": "Juli"
},
{
    "A": 210402,
    "B": "DESAGUADERO",
    "C": 3832,
    "D": "210400",
    "E": "Desaguadero"
},
{
    "A": 210403,
    "B": "HUACULLANI",
    "C": 3937,
    "D": "210400",
    "E": "Huacullani"
},
{
    "A": 210404,
    "B": "KELLUYO",
    "C": 3859,
    "D": "210400",
    "E": "Kelluyo"
},
{
    "A": 210405,
    "B": "PISACOMA",
    "C": 3972,
    "D": "210400",
    "E": "Pisacoma"
},
{
    "A": 210406,
    "B": "POMATA",
    "C": 3876,
    "D": "210400",
    "E": "Pomata"
},
{
    "A": 210407,
    "B": "ZEPITA",
    "C": 3831,
    "D": "210400",
    "E": "Zepita"
},
{
    "A": 210501,
    "B": "ILAVE",
    "C": 3862,
    "D": "210500",
    "E": "Ilave"
},
{
    "A": 210502,
    "B": "CAPAZO",
    "C": 4398,
    "D": "210500",
    "E": "Capazo"
},
{
    "A": 210503,
    "B": "PILCUYO",
    "C": 3833,
    "D": "210500",
    "E": "Pilcuyo"
},
{
    "A": 210504,
    "B": "SANTA ROSA",
    "C": 3977,
    "D": "210500",
    "E": "Santa Rosa"
},
{
    "A": 210505,
    "B": "CONDURIRI",
    "C": 3962,
    "D": "210500",
    "E": "Conduriri"
},
{
    "A": 210601,
    "B": "HUANCANE",
    "C": 3848,
    "D": "210600",
    "E": "Huancane"
},
{
    "A": 210602,
    "B": "COJATA",
    "C": 4364,
    "D": "210600",
    "E": "Cojata"
},
{
    "A": 210603,
    "B": "HUATASANI",
    "C": 3852,
    "D": "210600",
    "E": "Huatasani"
},
{
    "A": 210604,
    "B": "INCHUPALLA",
    "C": 3915,
    "D": "210600",
    "E": "Inchupalla"
},
{
    "A": 210605,
    "B": "PUSI",
    "C": 3838,
    "D": "210600",
    "E": "Pusi"
},
{
    "A": 210606,
    "B": "ROSASPATA",
    "C": 3879,
    "D": "210600",
    "E": "Rosaspata"
},
{
    "A": 210607,
    "B": "TARACO",
    "C": 3829,
    "D": "210600",
    "E": "Taraco"
},
{
    "A": 210608,
    "B": "VILQUE CHICO",
    "C": 3839,
    "D": "210600",
    "E": "Vilque Chico"
},
{
    "A": 210701,
    "B": "LAMPA",
    "C": 3873,
    "D": "210700",
    "E": "Lampa"
},
{
    "A": 210702,
    "B": "CABANILLA",
    "C": 3882,
    "D": "210700",
    "E": "Cabanilla"
},
{
    "A": 210703,
    "B": "CALAPUJA",
    "C": 3841,
    "D": "210700",
    "E": "Calapuja"
},
{
    "A": 210704,
    "B": "NICASIO",
    "C": 3856,
    "D": "210700",
    "E": "Nicasio"
},
{
    "A": 210705,
    "B": "OCUVIRI",
    "C": 4258,
    "D": "210700",
    "E": "Ocuviri"
},
{
    "A": 210706,
    "B": "PALCA",
    "C": 4068,
    "D": "210700",
    "E": "Palca"
},
{
    "A": 210707,
    "B": "PARATIA",
    "C": 4371,
    "D": "210700",
    "E": "Paratia"
},
{
    "A": 210708,
    "B": "PUCARA",
    "C": 3887,
    "D": "210700",
    "E": "Pucara"
},
{
    "A": 210709,
    "B": "SANTA LUCIA",
    "C": 4045,
    "D": "210700",
    "E": "Santa Lucia"
},
{
    "A": 210710,
    "B": "VILAVILA",
    "C": 4312,
    "D": "210700",
    "E": "Vilavila"
},
{
    "A": 210801,
    "B": "AYAVIRI",
    "C": 3918,
    "D": "210800",
    "E": "Ayaviri"
},
{
    "A": 210802,
    "B": "ANTAUTA",
    "C": 4200,
    "D": "210800",
    "E": "Antauta"
},
{
    "A": 210803,
    "B": "CUPI",
    "C": 3995,
    "D": "210800",
    "E": "Cupi"
},
{
    "A": 210804,
    "B": "LLALLI",
    "C": 4016,
    "D": "210800",
    "E": "Llalli"
},
{
    "A": 210805,
    "B": "MACARI",
    "C": 3969,
    "D": "210800",
    "E": "Macari"
},
{
    "A": 210806,
    "B": "NUÑOA",
    "C": 4023,
    "D": "210800",
    "E": "Nuñoa"
},
{
    "A": 210807,
    "B": "ORURILLO",
    "C": 3898,
    "D": "210800",
    "E": "Orurillo"
},
{
    "A": 210808,
    "B": "SANTA ROSA",
    "C": 4000,
    "D": "210800",
    "E": "Santa Rosa"
},
{
    "A": 210809,
    "B": "UMACHIRI",
    "C": 3921,
    "D": "210800",
    "E": "Umachiri"
},
{
    "A": 210901,
    "B": "MOHO",
    "C": 3889,
    "D": "210900",
    "E": "Moho"
},
{
    "A": 210902,
    "B": "CONIMA",
    "C": 3848,
    "D": "210900",
    "E": "Conima"
},
{
    "A": 210903,
    "B": "HUAYRAPATA",
    "C": 3896,
    "D": "210900",
    "E": "Huayrapata"
},
{
    "A": 210904,
    "B": "TILALI",
    "C": 3825,
    "D": "210900",
    "E": "Tilali"
},
{
    "A": 211001,
    "B": "PUTINA",
    "C": 3861,
    "D": "211000",
    "E": "Putina"
},
{
    "A": 211002,
    "B": "ANANEA",
    "C": 4660,
    "D": "211000",
    "E": "Ananea"
},
{
    "A": 211003,
    "B": "PEDRO VILCA APAZA",
    "C": 3856,
    "D": "211000",
    "E": "Pedro Vilca Apaza"
},
{
    "A": 211004,
    "B": "QUILCAPUNCU",
    "C": 3905,
    "D": "211000",
    "E": "Quilcapuncu"
},
{
    "A": 211005,
    "B": "SINA",
    "C": 3229,
    "D": "211000",
    "E": "Sina"
},
{
    "A": 211101,
    "B": "JULIACA",
    "C": 3832,
    "D": "211100",
    "E": "Juliaca"
},
{
    "A": 211102,
    "B": "CABANA",
    "C": 3901,
    "D": "211100",
    "E": "Cabana"
},
{
    "A": 211103,
    "B": "CABANILLAS",
    "C": 3887,
    "D": "211100",
    "E": "Cabanillas"
},
{
    "A": 211104,
    "B": "CARACOTO",
    "C": 3830,
    "D": "211100",
    "E": "Caracoto"
},
{
    "A": 211201,
    "B": "SANDIA",
    "C": 2249,
    "D": "211200",
    "E": "Sandia"
},
{
    "A": 211202,
    "B": "CUYOCUYO",
    "C": 3542,
    "D": "211200",
    "E": "Cuyocuyo"
},
{
    "A": 211203,
    "B": "LIMBANI",
    "C": 3300,
    "D": "211200",
    "E": "Limbani"
},
{
    "A": 211204,
    "B": "PATAMBUCO",
    "C": 3473,
    "D": "211200",
    "E": "Patambuco"
},
{
    "A": 211205,
    "B": "PHARA",
    "C": 3469,
    "D": "211200",
    "E": "Phara"
},
{
    "A": 211206,
    "B": "QUIACA",
    "C": 2978,
    "D": "211200",
    "E": "Quiaca"
},
{
    "A": 211207,
    "B": "SAN JUAN DEL ORO",
    "C": 1310,
    "D": "211200",
    "E": "San Juan Del Oro"
},
{
    "A": 211208,
    "B": "YANAHUAYA",
    "C": 1666,
    "D": "211200",
    "E": "Yanahuaya"
},
{
    "A": 211209,
    "B": "ALTO INAMBARI",
    "C": 1359,
    "D": "211200",
    "E": "Alto Inambari"
},
{
    "A": 211210,
    "B": "SAN PEDRO DE PUTINA PUNCO",
    "C": 947,
    "D": "211200",
    "E": "San Pedro De Putina Punco"
},
{
    "A": 211301,
    "B": "YUNGUYO",
    "C": 3839,
    "D": "211300",
    "E": "Yunguyo"
},
{
    "A": 211302,
    "B": "ANAPIA",
    "C": 3856,
    "D": "211300",
    "E": "Anapia"
},
{
    "A": 211303,
    "B": "COPANI",
    "C": 3854,
    "D": "211300",
    "E": "Copani"
},
{
    "A": 211304,
    "B": "CUTURAPI",
    "C": 3855,
    "D": "211300",
    "E": "Cuturapi"
},
{
    "A": 211305,
    "B": "OLLARAYA",
    "C": 3862,
    "D": "211300",
    "E": "Ollaraya"
},
{
    "A": 211306,
    "B": "TINICACHI",
    "C": 3852,
    "D": "211300",
    "E": "Tinicachi"
},
{
    "A": 211307,
    "B": "UNICACHI",
    "C": 3841,
    "D": "211300",
    "E": "Unicachi"
},
{
    "A": 220101,
    "B": "MOYOBAMBA",
    "C": 878,
    "D": "220100",
    "E": "Moyobamba"
},
{
    "A": 220102,
    "B": "CALZADA",
    "C": 848,
    "D": "220100",
    "E": "Calzada"
},
{
    "A": 220103,
    "B": "HABANA",
    "C": 842,
    "D": "220100",
    "E": "Habana"
},
{
    "A": 220104,
    "B": "JEPELACIO",
    "C": 1056,
    "D": "220100",
    "E": "Jepelacio"
},
{
    "A": 220105,
    "B": "SORITOR",
    "C": 884,
    "D": "220100",
    "E": "Soritor"
},
{
    "A": 220106,
    "B": "YANTALO",
    "C": 843,
    "D": "220100",
    "E": "Yantalo"
},
{
    "A": 220201,
    "B": "BELLAVISTA",
    "C": 285,
    "D": "220200",
    "E": "Bellavista"
},
{
    "A": 220202,
    "B": "ALTO BIAVO",
    "C": 288,
    "D": "220200",
    "E": "Alto Biavo"
},
{
    "A": 220203,
    "B": "BAJO BIAVO",
    "C": 241,
    "D": "220200",
    "E": "Bajo Biavo"
},
{
    "A": 220204,
    "B": "HUALLAGA",
    "C": 262,
    "D": "220200",
    "E": "Huallaga"
},
{
    "A": 220205,
    "B": "SAN PABLO",
    "C": 270,
    "D": "220200",
    "E": "San Pablo"
},
{
    "A": 220206,
    "B": "SAN RAFAEL",
    "C": 238,
    "D": "220200",
    "E": "San Rafael"
},
{
    "A": 220301,
    "B": "SAN JOSE DE SISA",
    "C": 346,
    "D": "220300",
    "E": "San Jose De Sisa"
},
{
    "A": 220302,
    "B": "AGUA BLANCA",
    "C": 308,
    "D": "220300",
    "E": "Agua Blanca"
},
{
    "A": 220303,
    "B": "SAN MARTIN",
    "C": 417,
    "D": "220300",
    "E": "San Martin"
},
{
    "A": 220304,
    "B": "SANTA ROSA",
    "C": 282,
    "D": "220300",
    "E": "Santa Rosa"
},
{
    "A": 220305,
    "B": "SHATOJA",
    "C": 397,
    "D": "220300",
    "E": "Shatoja"
},
{
    "A": 220401,
    "B": "SAPOSOA",
    "C": 303,
    "D": "220400",
    "E": "Saposoa"
},
{
    "A": 220402,
    "B": "ALTO SAPOSOA",
    "C": 412,
    "D": "220400",
    "E": "Alto Saposoa"
},
{
    "A": 220403,
    "B": "EL ESLABON",
    "C": 288,
    "D": "220400",
    "E": "El Eslabon"
},
{
    "A": 220404,
    "B": "PISCOYACU",
    "C": 298,
    "D": "220400",
    "E": "Piscoyacu"
},
{
    "A": 220405,
    "B": "SACANCHE",
    "C": 276,
    "D": "220400",
    "E": "Sacanche"
},
{
    "A": 220406,
    "B": "TINGO DE SAPOSOA",
    "C": 266,
    "D": "220400",
    "E": "Tingo De Saposoa"
},
{
    "A": 220501,
    "B": "LAMAS",
    "C": 791,
    "D": "220500",
    "E": "Lamas"
},
{
    "A": 220502,
    "B": "ALONSO DE ALVARADO",
    "C": 1103,
    "D": "220500",
    "E": "Alonso De Alvarado"
},
{
    "A": 220503,
    "B": "BARRANQUITA",
    "C": 158,
    "D": "220500",
    "E": "Barranquita"
},
{
    "A": 220504,
    "B": "CAYNARACHI",
    "C": 189,
    "D": "220500",
    "E": "Caynarachi"
},
{
    "A": 220505,
    "B": "CUÑUMBUQUI",
    "C": 233,
    "D": "220500",
    "E": "Cuñumbuqui"
},
{
    "A": 220506,
    "B": "PINTO RECODO",
    "C": 290,
    "D": "220500",
    "E": "Pinto Recodo"
},
{
    "A": 220507,
    "B": "RUMISAPA",
    "C": 329,
    "D": "220500",
    "E": "Rumisapa"
},
{
    "A": 220508,
    "B": "SAN ROQUE DE CUMBAZA",
    "C": 599,
    "D": "220500",
    "E": "San Roque De Cumbaza"
},
{
    "A": 220509,
    "B": "SHANAO",
    "C": 276,
    "D": "220500",
    "E": "Shanao"
},
{
    "A": 220510,
    "B": "TABALOSOS",
    "C": 559,
    "D": "220500",
    "E": "Tabalosos"
},
{
    "A": 220511,
    "B": "ZAPATERO",
    "C": 289,
    "D": "220500",
    "E": "Zapatero"
},
{
    "A": 220601,
    "B": "JUANJUI",
    "C": 282,
    "D": "220600",
    "E": "Juanjui"
},
{
    "A": 220602,
    "B": "CAMPANILLA",
    "C": 315,
    "D": "220600",
    "E": "Campanilla"
},
{
    "A": 220603,
    "B": "HUICUNGO",
    "C": 307,
    "D": "220600",
    "E": "Huicungo"
},
{
    "A": 220604,
    "B": "PACHIZA",
    "C": 288,
    "D": "220600",
    "E": "Pachiza"
},
{
    "A": 220605,
    "B": "PAJARILLO",
    "C": 271,
    "D": "220600",
    "E": "Pajarillo"
},
{
    "A": 220701,
    "B": "PICOTA",
    "C": 223,
    "D": "220700",
    "E": "Picota"
},
{
    "A": 220702,
    "B": "BUENOS AIRES",
    "C": 209,
    "D": "220700",
    "E": "Buenos Aires"
},
{
    "A": 220703,
    "B": "CASPISAPA",
    "C": 229,
    "D": "220700",
    "E": "Caspisapa"
},
{
    "A": 220704,
    "B": "PILLUANA",
    "C": 208,
    "D": "220700",
    "E": "Pilluana"
},
{
    "A": 220705,
    "B": "PUCACACA",
    "C": 219,
    "D": "220700",
    "E": "Pucacaca"
},
{
    "A": 220706,
    "B": "SAN CRISTOBAL",
    "C": 231,
    "D": "220700",
    "E": "San Cristobal"
},
{
    "A": 220707,
    "B": "SAN HILARION",
    "C": 230,
    "D": "220700",
    "E": "San Hilarion"
},
{
    "A": 220708,
    "B": "SHAMBOYACU",
    "C": 240,
    "D": "220700",
    "E": "Shamboyacu"
},
{
    "A": 220709,
    "B": "TINGO DE PONASA",
    "C": 240,
    "D": "220700",
    "E": "Tingo De Ponasa"
},
{
    "A": 220710,
    "B": "TRES UNIDOS",
    "C": 244,
    "D": "220700",
    "E": "Tres Unidos"
},
{
    "A": 220801,
    "B": "RIOJA",
    "C": 841,
    "D": "220800",
    "E": "Rioja"
},
{
    "A": 220802,
    "B": "AWAJUN",
    "C": 878,
    "D": "220800",
    "E": "Awajun"
},
{
    "A": 220803,
    "B": "ELIAS SOPLIN VARGAS",
    "C": 831,
    "D": "220800",
    "E": "Elias Soplin Vargas"
},
{
    "A": 220804,
    "B": "NUEVA CAJAMARCA",
    "C": 869,
    "D": "220800",
    "E": "Nueva Cajamarca"
},
{
    "A": 220805,
    "B": "PARDO MIGUEL",
    "C": 954,
    "D": "220800",
    "E": "Pardo Miguel"
},
{
    "A": 220806,
    "B": "POSIC",
    "C": 824,
    "D": "220800",
    "E": "Posic"
},
{
    "A": 220807,
    "B": "SAN FERNANDO",
    "C": 825,
    "D": "220800",
    "E": "San Fernando"
},
{
    "A": 220808,
    "B": "YORONGOS",
    "C": 868,
    "D": "220800",
    "E": "Yorongos"
},
{
    "A": 220809,
    "B": "YURACYACU",
    "C": 815,
    "D": "220800",
    "E": "Yuracyacu"
},
{
    "A": 220901,
    "B": "TARAPOTO",
    "C": 280,
    "D": "220900",
    "E": "Tarapoto"
},
{
    "A": 220902,
    "B": "ALBERTO LEVEAU",
    "C": 173,
    "D": "220900",
    "E": "Alberto Leveau"
},
{
    "A": 220903,
    "B": "CACATACHI",
    "C": 206,
    "D": "220900",
    "E": "Cacatachi"
},
{
    "A": 220904,
    "B": "CHAZUTA",
    "C": 296,
    "D": "220900",
    "E": "Chazuta"
},
{
    "A": 220905,
    "B": "CHIPURANA",
    "C": 181,
    "D": "220900",
    "E": "Chipurana"
},
{
    "A": 220906,
    "B": "EL PORVENIR",
    "C": 150,
    "D": "220900",
    "E": "El Porvenir"
},
{
    "A": 220907,
    "B": "HUIMBAYOC",
    "C": 140,
    "D": "220900",
    "E": "Huimbayoc"
},
{
    "A": 220908,
    "B": "JUAN GUERRA",
    "C": 205,
    "D": "220900",
    "E": "Juan Guerra"
},
{
    "A": 220909,
    "B": "LA BANDA DE SHILCAYO",
    "C": 294,
    "D": "220900",
    "E": "La Banda De Shilcayo"
},
{
    "A": 220910,
    "B": "MORALES",
    "C": 282,
    "D": "220900",
    "E": "Morales"
},
{
    "A": 220911,
    "B": "PAPAPLAYA",
    "C": 149,
    "D": "220900",
    "E": "Papaplaya"
},
{
    "A": 220912,
    "B": "SAN ANTONIO",
    "C": 402,
    "D": "220900",
    "E": "San Antonio"
},
{
    "A": 220913,
    "B": "SAUCE",
    "C": 614,
    "D": "220900",
    "E": "Sauce"
},
{
    "A": 220914,
    "B": "SHAPAJA",
    "C": 207,
    "D": "220900",
    "E": "Shapaja"
},
{
    "A": 221001,
    "B": "TOCACHE",
    "C": 502,
    "D": "221000",
    "E": "Tocache"
},
{
    "A": 221002,
    "B": "NUEVO PROGRESO",
    "C": 505,
    "D": "221000",
    "E": "Nuevo Progreso"
},
{
    "A": 221003,
    "B": "POLVORA",
    "C": 533,
    "D": "221000",
    "E": "Polvora"
},
{
    "A": 221004,
    "B": "SHUNTE",
    "C": 1144,
    "D": "221000",
    "E": "Shunte"
},
{
    "A": 221005,
    "B": "UCHIZA",
    "C": 545,
    "D": "221000",
    "E": "Uchiza"
},
{
    "A": 230101,
    "B": "TACNA",
    "C": 585,
    "D": "230100",
    "E": "Tacna"
},
{
    "A": 230102,
    "B": "ALTO DE LA ALIANZA",
    "C": 620,
    "D": "230100",
    "E": "Alto De La Alianza"
},
{
    "A": 230103,
    "B": "CALANA",
    "C": 904,
    "D": "230100",
    "E": "Calana"
},
{
    "A": 230104,
    "B": "CIUDAD NUEVA",
    "C": 669,
    "D": "230100",
    "E": "Ciudad Nueva"
},
{
    "A": 230105,
    "B": "INCLAN",
    "C": 516,
    "D": "230100",
    "E": "Inclan"
},
{
    "A": 230106,
    "B": "PACHIA",
    "C": 1100,
    "D": "230100",
    "E": "Pachia"
},
{
    "A": 230107,
    "B": "PALCA",
    "C": 3021,
    "D": "230100",
    "E": "Palca"
},
{
    "A": 230108,
    "B": "POCOLLAY",
    "C": 678,
    "D": "230100",
    "E": "Pocollay"
},
{
    "A": 230109,
    "B": "SAMA",
    "C": 404,
    "D": "230100",
    "E": "Sama"
},
{
    "A": 230110,
    "B": "CORONEL GREGORIO ALBARRACIN LANCHIPA",
    "C": 520,
    "D": "230100",
    "E": "Coronel Gregorio Albarracin Lanchipa"
},
{
    "A": 230201,
    "B": "CANDARAVE",
    "C": 3460,
    "D": "230200",
    "E": "Candarave"
},
{
    "A": 230202,
    "B": "CAIRANI",
    "C": 3394,
    "D": "230200",
    "E": "Cairani"
},
{
    "A": 230203,
    "B": "CAMILACA",
    "C": 3518,
    "D": "230200",
    "E": "Camilaca"
},
{
    "A": 230204,
    "B": "CURIBAYA",
    "C": 2401,
    "D": "230200",
    "E": "Curibaya"
},
{
    "A": 230205,
    "B": "HUANUARA",
    "C": 3223,
    "D": "230200",
    "E": "Huanuara"
},
{
    "A": 230206,
    "B": "QUILAHUANI",
    "C": 3218,
    "D": "230200",
    "E": "Quilahuani"
},
{
    "A": 230301,
    "B": "LOCUMBA",
    "C": 596,
    "D": "230300",
    "E": "Locumba"
},
{
    "A": 230302,
    "B": "ILABAYA",
    "C": 1384,
    "D": "230300",
    "E": "Ilabaya"
},
{
    "A": 230303,
    "B": "ITE",
    "C": 174,
    "D": "230300",
    "E": "Ite"
},
{
    "A": 230401,
    "B": "TARATA",
    "C": 3084,
    "D": "230400",
    "E": "Tarata"
},
{
    "A": 230402,
    "B": "HEROES ALBARRACIN",
    "C": 2347,
    "D": "230400",
    "E": "Heroes Albarracin"
},
{
    "A": 230403,
    "B": "ESTIQUE",
    "C": 3168,
    "D": "230400",
    "E": "Estique"
},
{
    "A": 230404,
    "B": "ESTIQUE-PAMPA",
    "C": 3066,
    "D": "230400",
    "E": "Estique-Pampa"
},
{
    "A": 230405,
    "B": "SITAJARA",
    "C": 3156,
    "D": "230400",
    "E": "Sitajara"
},
{
    "A": 230406,
    "B": "SUSAPAYA",
    "C": 3399,
    "D": "230400",
    "E": "Susapaya"
},
{
    "A": 230407,
    "B": "TARUCACHI",
    "C": 3094,
    "D": "230400",
    "E": "Tarucachi"
},
{
    "A": 230408,
    "B": "TICACO",
    "C": 3255,
    "D": "230400",
    "E": "Ticaco"
},
{
    "A": 240101,
    "B": "TUMBES",
    "C": 23,
    "D": "240100",
    "E": "Tumbes"
},
{
    "A": 240102,
    "B": "CORRALES",
    "C": 15,
    "D": "240100",
    "E": "Corrales"
},
{
    "A": 240103,
    "B": "LA CRUZ",
    "C": 8,
    "D": "240100",
    "E": "La Cruz"
},
{
    "A": 240104,
    "B": "PAMPAS DE HOSPITAL",
    "C": 23,
    "D": "240100",
    "E": "Pampas De Hospital"
},
{
    "A": 240105,
    "B": "SAN JACINTO",
    "C": 36,
    "D": "240100",
    "E": "San Jacinto"
},
{
    "A": 240106,
    "B": "SAN JUAN DE LA VIRGEN",
    "C": 15,
    "D": "240100",
    "E": "San Juan De La Virgen"
},
{
    "A": 240201,
    "B": "ZORRITOS",
    "C": 5,
    "D": "240200",
    "E": "Zorritos"
},
{
    "A": 240202,
    "B": "CASITAS",
    "C": 129,
    "D": "240200",
    "E": "Casitas"
},
{
    "A": 240203,
    "B": "CANOAS DE PUNTA SAL",
    "C": 6,
    "D": "240200",
    "E": "Canoas De Punta Sal"
},
{
    "A": 240301,
    "B": "ZARUMILLA",
    "C": 14,
    "D": "240300",
    "E": "Zarumilla"
},
{
    "A": 240302,
    "B": "AGUAS VERDES",
    "C": 8,
    "D": "240300",
    "E": "Aguas Verdes"
},
{
    "A": 240303,
    "B": "MATAPALO",
    "C": 65,
    "D": "240300",
    "E": "Matapalo"
},
{
    "A": 240304,
    "B": "PAPAYAL",
    "C": 43,
    "D": "240300",
    "E": "Papayal"
},
{
    "A": 250101,
    "B": "CALLERIA",
    "C": 157,
    "D": "250100",
    "E": "Calleria"
},
{
    "A": 250103,
    "B": "IPARIA",
    "C": 169,
    "D": "250100",
    "E": "Iparia"
},
{
    "A": 250104,
    "B": "MASISEA",
    "C": 156,
    "D": "250100",
    "E": "Masisea"
},
{
    "A": 250105,
    "B": "YARINACOCHA",
    "C": 153,
    "D": "250100",
    "E": "Yarinacocha"
},
{
    "A": 250106,
    "B": "NUEVA REQUENA",
    "C": 153,
    "D": "250100",
    "E": "Nueva Requena"
},
{
    "A": 250107,
    "B": "MANANTAY",
    "C": 150,
    "D": "250100",
    "E": "Manantay"
},
{
    "A": 250201,
    "B": "RAYMONDI",
    "C": 228,
    "D": "250200",
    "E": "Raymondi"
},
{
    "A": 250202,
    "B": "SEPAHUA",
    "C": 280,
    "D": "250200",
    "E": "Sepahua"
},
{
    "A": 250203,
    "B": "TAHUANIA",
    "C": 200,
    "D": "250200",
    "E": "Tahuania"
},
{
    "A": 250204,
    "B": "YURUA",
    "C": 249,
    "D": "250200",
    "E": "Yurua"
},
{
    "A": 250301,
    "B": "PADRE ABAD",
    "C": 300,
    "D": "250300",
    "E": "Padre Abad"
},
{
    "A": 250302,
    "B": "IRAZOLA",
    "C": 220,
    "D": "250300",
    "E": "Irazola"
},
{
    "A": 250303,
    "B": "CURIMANA",
    "C": 172,
    "D": "250300",
    "E": "Curimana"
},
{
    "A": "250102",
    "B": "CAMPOVERDE",
    "C": 193,
    "D": "250100",
    "E": "Campoverde"
},
{
    "A": "250401",
    "B": "PURUS",
    "C": 232,
    "D": "250400",
    "E": "Purus"
},
];


function castDistrict(districts) {
  const d = districts.map((e) => ({
      id : e.A,
      name : e.E,
      provinceId: e.D,
    }));
  return d;
}

const provinces = [{ id: '010100', 'name': 'Chachapoyas', region_id: '010000', 'created_at': null, 'updated_at': null },
  { 'id': '010200', 'name': 'Bagua', region_id: '010000', created_at: null, 'updated_at': null }, { 'id': '010300', 'name': 'Bongar\u00e1', region_id: '010000', 'created_at': null, 'updated_at': null }, { 'id': '010400', name: 'Condorcanqui', 'region_id': '010000', created_at: null, 'updated_at': null }, { id: '010500', name: 'Luya', 'region_id': '010000', 'created_at': null, updated_at: null }, { 'id': '010600', 'name': 'Rodr\u00edguez de Mendoza', region_id: '010000', created_at: null, 'updated_at': null }, { id: '010700', 'name': 'Utcubamba', region_id: '010000', created_at: null, updated_at: null }, { 'id': '020100', name: 'Huaraz', 'region_id': '020000', created_at: null, updated_at: null }, { 'id': '020200', name: 'Aija', 'region_id': '020000', created_at: null, 'updated_at': null }, { id: '020300', 'name': 'Antonio Raymondi', region_id: '020000', 'created_at': null, 'updated_at': null }, { id: '020400', 'name': 'Asunci\u00f3n', region_id: '020000', created_at: null, 'updated_at': null }, { id: '020500', name: 'Bolognesi', region_id: '020000', 'created_at': null, 'updated_at': null }, { id: '020600', 'name': 'Carhuaz', 'region_id': '020000', created_at: null, updated_at: null }, { id: '020700', 'name': 'Carlos Ferm\u00edn Fitzcarrald', region_id: '020000', 'created_at': null, 'updated_at': null }, { id: '020800', name: 'Casma', 'region_id': '020000', created_at: null, 'updated_at': null }, { id: '020900', 'name': 'Corongo', 'region_id': '020000', 'created_at': null, 'updated_at': null }, { id: '021000', 'name': 'Huari', region_id: '020000', 'created_at': null, updated_at: null }, { id: '021100', 'name': 'Huarmey', 'region_id': '020000', created_at: null, updated_at: null }, { 'id': '021200', name: 'Huaylas', 'region_id': '020000', 'created_at': null, 'updated_at': null }, { 'id': '021300', name: 'Mariscal Luzuriaga', 'region_id': '020000', 'created_at': null, updated_at: null }, { id: '021400', name: 'Ocros', 'region_id': '020000', 'created_at': null, 'updated_at': null }, { id: '021500', name: 'Pallasca', region_id: '020000', created_at: null, 'updated_at': null }, { id: '021600', 'name': 'Pomabamba', region_id: '020000', 'created_at': null, updated_at: null }, { 'id': '021700', 'name': 'Recuay', 'region_id': '020000', created_at: null, 'updated_at': null }, { id: '021800', 'name': 'Santa', region_id: '020000', 'created_at': null, 'updated_at': null }, { id: '021900', name: 'Sihuas', 'region_id': '020000', created_at: null, 'updated_at': null }, { id: '022000', 'name': 'Yungay', region_id: '020000', 'created_at': null, updated_at: null }, { 'id': '030100', 'name': 'Abancay', region_id: '030000', 'created_at': null, 'updated_at': null }, { id: '030200', 'name': 'Andahuaylas', 'region_id': '030000', created_at: null, updated_at: null }, { 'id': '030300', 'name': 'Antabamba', 'region_id': '030000', 'created_at': null, 'updated_at': null }, { 'id': '030400', 'name': 'Aymaraes', 'region_id': '030000', 'created_at': null, 'updated_at': null }, { id: '030500', 'name': 'Cotabambas', region_id: '030000', created_at: null, 'updated_at': null }, { id: '030600', 'name': 'Chincheros', 'region_id': '030000', 'created_at': null, 'updated_at': null }, { id: '030700', 'name': 'Grau', region_id: '030000', created_at: null, updated_at: null }, { 'id': '040100', name: 'Arequipa', 'region_id': '040000', created_at: null, updated_at: null }, { id: '040200', name: 'Caman\u00e1', region_id: '040000', created_at: null, 'updated_at': null }, { 'id': '040300', name: 'Caravel\u00ed', 'region_id': '040000', created_at: null, updated_at: null }, { 'id': '040400', name: 'Castilla', region_id: '040000', 'created_at': null, updated_at: null }, { 'id': '040500', name: 'Caylloma', region_id: '040000', 'created_at': null, 'updated_at': null }, { 'id': '040600', name: 'Condesuyos', 'region_id': '040000', 'created_at': null, 'updated_at': null }, { id: '040700', 'name': 'Islay', 'region_id': '040000', 'created_at': null, updated_at: null }, { 'id': '040800', name: 'La Uni\u00f2n', region_id: '040000', 'created_at': null, 'updated_at': null }, { 'id': '050100', name: 'Huamanga', 'region_id': '050000', 'created_at': null, 'updated_at': null }, { id: '050200', name: 'Cangallo', 'region_id': '050000', created_at: null, 'updated_at': null }, { 'id': '050300', 'name': 'Huanca Sancos', region_id: '050000', created_at: null, 'updated_at': null }, { 'id': '050400', name: 'Huanta', 'region_id': '050000', created_at: null, 'updated_at': null }, { id: '050500', 'name': 'La Mar', 'region_id': '050000', created_at: null, updated_at: null }, { 'id': '050600', name: 'Lucanas', region_id: '050000', created_at: null, 'updated_at': null }, { 'id': '050700', 'name': 'Parinacochas', region_id: '050000', created_at: null, updated_at: null }, { id: '050800', name: 'P\u00e0ucar del Sara Sara', 'region_id': '050000', 'created_at': null, updated_at: null }, { id: '050900', name: 'Sucre', 'region_id': '050000', 'created_at': null, updated_at: null }, { 'id': '051000', name: 'V\u00edctor Fajardo', region_id: '050000', created_at: null, updated_at: null }, { 'id': '051100', 'name': 'Vilcas Huam\u00e1n', region_id: '050000', 'created_at': null, updated_at: null }, { 'id': '060100', name: 'Cajamarca', region_id: '060000', created_at: null, 'updated_at': null }, { 'id': '060200', name: 'Cajabamba', 'region_id': '060000', created_at: null, updated_at: null }, { 'id': '060300', name: 'Celend\u00edn', region_id: '060000', created_at: null, updated_at: null }, { id: '060400', name: 'Chota', region_id: '060000', created_at: null, 'updated_at': null }, { id: '060500', name: 'Contumaz\u00e1', region_id: '060000', created_at: null, updated_at: null }, { id: '060600', 'name': 'Cutervo', region_id: '060000', created_at: null, 'updated_at': null }, { 'id': '060700', name: 'Hualgayoc', 'region_id': '060000', 'created_at': null, 'updated_at': null }, { id: '060800', name: 'Ja\u00e9n', 'region_id': '060000', 'created_at': null, 'updated_at': null }, { 'id': '060900', name: 'San Ignacio', 'region_id': '060000', 'created_at': null, updated_at: null }, { 'id': '061000', 'name': 'San Marcos', region_id: '060000', created_at: null, updated_at: null }, { id: '061100', name: 'San Miguel', region_id: '060000', 'created_at': null, 'updated_at': null }, { 'id': '061200', 'name': 'San Pablo', 'region_id': '060000', created_at: null, 'updated_at': null }, { 'id': '061300', name: 'Santa Cruz', 'region_id': '060000', created_at: null, updated_at: null }, { id: '070100', name: 'Prov. Const. del Callao', 'region_id': '070000', 'created_at': null, updated_at: null }, { 'id': '080100', 'name': 'Cusco', region_id: '080000', 'created_at': null, 'updated_at': null }, { id: '080200', name: 'Acomayo', 'region_id': '080000', created_at: null, 'updated_at': null }, { id: '080300', 'name': 'Anta', 'region_id': '080000', 'created_at': null, 'updated_at': null }, { 'id': '080400', name: 'Calca', 'region_id': '080000', 'created_at': null, updated_at: null }, { id: '080500', name: 'Canas', 'region_id': '080000', created_at: null, updated_at: null }, { 'id': '080600', name: 'Canchis', region_id: '080000', created_at: null, updated_at: null }, { 'id': '080700', name: 'Chumbivilcas', region_id: '080000', created_at: null, 'updated_at': null }, { 'id': '080800', 'name': 'Espinar', 'region_id': '080000', created_at: null, updated_at: null }, { 'id': '080900', 'name': 'La Convenci\u00f3n', 'region_id': '080000', created_at: null, updated_at: null }, { id: '081000', 'name': 'Paruro', region_id: '080000', 'created_at': null, updated_at: null }, { id: '081100', name: 'Paucartambo', 'region_id': '080000', 'created_at': null, 'updated_at': null }, { 'id': '081200', name: 'Quispicanchi', region_id: '080000', created_at: null, updated_at: null }, { 'id': '081300', name: 'Urubamba', 'region_id': '080000', 'created_at': null, 'updated_at': null }, { 'id': '090100', name: 'Huancavelica', 'region_id': '090000', 'created_at': null, updated_at: null }, { id: '090200', name: 'Acobamba', 'region_id': '090000', created_at: null, 'updated_at': null }, { 'id': '090300', 'name': 'Angaraes', region_id: '090000', created_at: null, 'updated_at': null }, { id: '090400', name: 'Castrovirreyna', region_id: '090000', 'created_at': null, 'updated_at': null }, { id: '090500', name: 'Churcampa', region_id: '090000', 'created_at': null, 'updated_at': null }, { 'id': '090600', 'name': 'Huaytar\u00e1', region_id: '090000', created_at: null, 'updated_at': null }, { id: '090700', name: 'Tayacaja', region_id: '090000', created_at: null, 'updated_at': null }, { 'id': '100100', name: 'Hu\u00e1nuco', region_id: '100000', 'created_at': null, 'updated_at': null }, { 'id': '100200', name: 'Ambo', 'region_id': '100000', created_at: null, updated_at: null }, { 'id': '100300', name: 'Dos de Mayo', region_id: '100000', created_at: null, 'updated_at': null }, { 'id': '100400', name: 'Huacaybamba', region_id: '100000', created_at: null, 'updated_at': null }, { id: '100500', name: 'Huamal\u00edes', 'region_id': '100000', 'created_at': null, 'updated_at': null }, { id: '100600', name: 'Leoncio Prado', region_id: '100000', created_at: null, updated_at: null }, { 'id': '100700', 'name': 'Mara\u00f1\u00f3n', 'region_id': '100000', created_at: null, updated_at: null }, { 'id': '100800', name: 'Pachitea', region_id: '100000', created_at: null, 'updated_at': null }, { 'id': '100900', 'name': 'Puerto Inca', 'region_id': '100000', 'created_at': null, updated_at: null }, { 'id': '101000', 'name': 'Lauricocha ', region_id: '100000', 'created_at': null, 'updated_at': null }, { id: '101100', name: 'Yarowilca ', 'region_id': '100000', 'created_at': null, updated_at: null }, { id: '110100', 'name': 'Ica ', 'region_id': '110000', 'created_at': null, updated_at: null }, { 'id': '110200', 'name': 'Chincha ', 'region_id': '110000', created_at: null, 'updated_at': null }, { id: '110300', 'name': 'Nasca ', 'region_id': '110000', 'created_at': null, updated_at: null }, { id: '110400', name: 'Palpa ', region_id: '110000', created_at: null, 'updated_at': null }, { id: '110500', name: 'Pisco ', 'region_id': '110000', 'created_at': null, updated_at: null }, { id: '120100', name: 'Huancayo ', region_id: '120000', 'created_at': null, 'updated_at': null }, { id: '120200', name: 'Concepci\u00f3n ', region_id: '120000', created_at: null, 'updated_at': null }, { id: '120300', name: 'Chanchamayo ', region_id: '120000', created_at: null, 'updated_at': null }, { id: '120400', 'name': 'Jauja ', region_id: '120000', created_at: null, 'updated_at': null }, { 'id': '120500', name: 'Jun\u00edn ', region_id: '120000', created_at: null, 'updated_at': null }, { 'id': '120600', name: 'Satipo ', 'region_id': '120000', 'created_at': null, updated_at: null }, { 'id': '120700', 'name': 'Tarma ', region_id: '120000', created_at: null, 'updated_at': null }, { 'id': '120800', name: 'Yauli ', region_id: '120000', 'created_at': null, 'updated_at': null }, { id: '120900', name: 'Chupaca ', 'region_id': '120000', 'created_at': null, updated_at: null }, { 'id': '130100', 'name': 'Trujillo ', region_id: '130000', 'created_at': null, updated_at: null }, { 'id': '130200', 'name': 'Ascope ', region_id: '130000', 'created_at': null, 'updated_at': null }, { 'id': '130300', 'name': 'Bol\u00edvar ', region_id: '130000', 'created_at': null, updated_at: null }, { 'id': '130400', 'name': 'Chep\u00e9n ', 'region_id': '130000', created_at: null, 'updated_at': null }, { id: '130500', 'name': 'Julc\u00e1n ', 'region_id': '130000', created_at: null, 'updated_at': null }, { 'id': '130600', 'name': 'Otuzco ', 'region_id': '130000', 'created_at': null, updated_at: null }, { 'id': '130700', 'name': 'Pacasmayo ', 'region_id': '130000', created_at: null, updated_at: null }, { 'id': '130800', name: 'Pataz ', region_id: '130000', created_at: null, 'updated_at': null }, { id: '130900', 'name': 'S\u00e1nchez Carri\u00f3n ', region_id: '130000', created_at: null, 'updated_at': null }, { id: '131000', name: 'Santiago de Chuco ', region_id: '130000', 'created_at': null, updated_at: null }, { id: '131100', 'name': 'Gran Chim\u00fa ', region_id: '130000', 'created_at': null, updated_at: null }, { 'id': '131200', 'name': 'Vir\u00fa ', 'region_id': '130000', 'created_at': null, updated_at: null }, { id: '140100', 'name': 'Chiclayo ', region_id: '140000', 'created_at': null, 'updated_at': null }, { 'id': '140200', 'name': 'Ferre\u00f1afe ', region_id: '140000', 'created_at': null, 'updated_at': null }, { id: '140300', name: 'Lambayeque ', 'region_id': '140000', 'created_at': null, 'updated_at': null }, { id: '150100', 'name': 'Lima ', 'region_id': '150000', 'created_at': null, updated_at: null }, { id: '150200', name: 'Barranca ', region_id: '150000', 'created_at': null, updated_at: null }, { 'id': '150300', name: 'Cajatambo ', region_id: '150000', 'created_at': null, 'updated_at': null }, { 'id': '150400', name: 'Canta ', 'region_id': '150000', created_at: null, updated_at: null }, { id: '150500', 'name': 'Ca\u00f1ete ', 'region_id': '150000', 'created_at': null, 'updated_at': null }, { 'id': '150600', 'name': 'Huaral ', region_id: '150000', 'created_at': null, 'updated_at': null }, { 'id': '150700', name: 'Huarochir\u00ed ', region_id: '150000', 'created_at': null, updated_at: null }, { 'id': '150800', 'name': 'Huaura ', region_id: '150000', 'created_at': null, 'updated_at': null }, { 'id': '150900', name: 'Oy\u00f3n ', region_id: '150000', 'created_at': null, updated_at: null }, { 'id': '151000', 'name': 'Yauyos ', 'region_id': '150000', 'created_at': null, updated_at: null }, { 'id': '160100', name: 'Maynas ', region_id: '160000', 'created_at': null, updated_at: null }, { id: '160200', 'name': 'Alto Amazonas ', 'region_id': '160000', 'created_at': null, updated_at: null }, { id: '160300', 'name': 'Loreto ', region_id: '160000', created_at: null, updated_at: null }, { 'id': '160400', name: 'Mariscal Ram\u00f3n Castilla ', 'region_id': '160000', created_at: null, 'updated_at': null }, { id: '160500', name: 'Requena ', 'region_id': '160000', 'created_at': null, updated_at: null }, { 'id': '160600', 'name': 'Ucayali ', region_id: '160000', created_at: null, updated_at: null }, { 'id': '160700', name: 'Datem del Mara\u00f1\u00f3n ', 'region_id': '160000', 'created_at': null, updated_at: null }, { id: '160800', name: 'Putumayo', region_id: '160000', created_at: null, 'updated_at': null }, { id: '170100', name: 'Tambopata ', 'region_id': '170000', created_at: null, updated_at: null }, { 'id': '170200', 'name': 'Manu ', 'region_id': '170000', created_at: null, updated_at: null }, { id: '170300', 'name': 'Tahuamanu ', 'region_id': '170000', 'created_at': null, 'updated_at': null }, { id: '180100', name: 'Mariscal Nieto ', 'region_id': '180000', created_at: null, updated_at: null }, { id: '180200', 'name': 'General S\u00e1nchez Cerro ', 'region_id': '180000', created_at: null, updated_at: null }, { 'id': '180300', 'name': 'Ilo ', region_id: '180000', created_at: null, updated_at: null }, { 'id': '190100', name: 'Pasco ', region_id: '190000', created_at: null, updated_at: null }, { 'id': '190200', 'name': 'Daniel Alcides Carri\u00f3n ', region_id: '190000', created_at: null, 'updated_at': null }, { 'id': '190300', 'name': 'Oxapampa ', 'region_id': '190000', created_at: null, 'updated_at': null }, { id: '200100', name: 'Piura ', region_id: '200000', created_at: null, updated_at: null }, { id: '200200', 'name': 'Ayabaca ', region_id: '200000', 'created_at': null, 'updated_at': null }, { id: '200300', 'name': 'Huancabamba ', region_id: '200000', 'created_at': null, 'updated_at': null }, { 'id': '200400', 'name': 'Morrop\u00f3n ', region_id: '200000', 'created_at': null, 'updated_at': null }, { 'id': '200500', 'name': 'Paita ', 'region_id': '200000', 'created_at': null, updated_at: null }, { 'id': '200600', 'name': 'Sullana ', region_id: '200000', 'created_at': null, updated_at: null }, { 'id': '200700', 'name': 'Talara ', region_id: '200000', 'created_at': null, updated_at: null }, { id: '200800', 'name': 'Sechura ', 'region_id': '200000', 'created_at': null, 'updated_at': null }, { 'id': '210100', name: 'Puno ', 'region_id': '210000', 'created_at': null, 'updated_at': null }, { 'id': '210200', 'name': 'Az\u00e1ngaro ', region_id: '210000', 'created_at': null, updated_at: null }, { 'id': '210300', 'name': 'Carabaya ', 'region_id': '210000', 'created_at': null, updated_at: null }, { 'id': '210400', name: 'Chucuito ', region_id: '210000', 'created_at': null, 'updated_at': null }, { 'id': '210500', 'name': 'El Collao ', region_id: '210000', 'created_at': null, updated_at: null }, { id: '210600', 'name': 'Huancan\u00e9 ', region_id: '210000', created_at: null, 'updated_at': null }, { id: '210700', 'name': 'Lampa ', 'region_id': '210000', 'created_at': null, updated_at: null }, { id: '210800', 'name': 'Melgar ', region_id: '210000', created_at: null, 'updated_at': null }, { 'id': '210900', name: 'Moho ', 'region_id': '210000', created_at: null, 'updated_at': null }, { id: '211000', 'name': 'San Antonio de Putina ', region_id: '210000', 'created_at': null, updated_at: null }, { 'id': '211100', 'name': 'San Rom\u00e1n ', 'region_id': '210000', 'created_at': null, updated_at: null }, { 'id': '211200', 'name': 'Sandia ', region_id: '210000', created_at: null, 'updated_at': null }, { id: '211300', 'name': 'Yunguyo ', 'region_id': '210000', created_at: null, 'updated_at': null }, { 'id': '220100', 'name': 'Moyobamba ', region_id: '220000', 'created_at': null, 'updated_at': null }, { 'id': '220200', name: 'Bellavista ', region_id: '220000', 'created_at': null, 'updated_at': null }, { id: '220300', 'name': 'El Dorado ', region_id: '220000', 'created_at': null, 'updated_at': null }, { 'id': '220400', name: 'Huallaga ', region_id: '220000', created_at: null, 'updated_at': null }, { id: '220500', 'name': 'Lamas ', region_id: '220000', created_at: null, 'updated_at': null }, { 'id': '220600', 'name': 'Mariscal C\u00e1ceres ', 'region_id': '220000', created_at: null, updated_at: null }, { id: '220700', name: 'Picota ', region_id: '220000', created_at: null, updated_at: null }, { 'id': '220800', 'name': 'Rioja ', region_id: '220000', 'created_at': null, updated_at: null }, { id: '220900', name: 'San Mart\u00edn ', region_id: '220000', created_at: null, updated_at: null }, { id: '221000', name: 'Tocache ', region_id: '220000', 'created_at': null, 'updated_at': null }, { id: '230100', 'name': 'Tacna ', 'region_id': '230000', 'created_at': null, updated_at: null }, { id: '230200', 'name': 'Candarave ', 'region_id': '230000', created_at: null, 'updated_at': null }, { id: '230300', 'name': 'Jorge Basadre ', region_id: '230000', created_at: null, 'updated_at': null }, { id: '230400', name: 'Tarata ', region_id: '230000', 'created_at': null, 'updated_at': null }, { 'id': '240100', name: 'Tumbes ', region_id: '240000', created_at: null, 'updated_at': null }, { 'id': '240200', 'name': 'Contralmirante Villar ', 'region_id': '240000', created_at: null, updated_at: null }, { id: '240300', 'name': 'Zarumilla ', 'region_id': '240000', created_at: null, updated_at: null }, { id: '250100', 'name': 'Coronel Portillo ', 'region_id': '250000', 'created_at': null, updated_at: null }, { 'id': '250200', name: 'Atalaya ', region_id: '250000', 'created_at': null, 'updated_at': null }, { 'id': '250300', 'name': 'Padre Abad ', region_id: '250000', 'created_at': null, updated_at: null }, { 'id': '250400', name: 'Pur\u00fas', 'region_id': '250000', created_at: null, updated_at: null }];

function castProvinces(provinces) {
  const p = provinces.map((e) => ({
      id : e.id,
      name : e.name,
      departmentId : e.region_id
    }));
  return p;
}

const departments =   [{ 'id': '010000', name: 'Amazonas', 'created_at': null, 'updated_at': null },
    { 'id': '020000', name: '\u00c1ncash', 'created_at': null, updated_at: null },
    { 'id': '030000', name: 'Apur\u00edmac', created_at: null, updated_at: null },
    { 'id': '040000', name: 'Arequipa', created_at: null, 'updated_at': null },
    { id: '050000', name: 'Ayacucho', created_at: null, updated_at: null },
    { 'id': '060000', 'name': 'Cajamarca', 'created_at': null, 'updated_at': null },
    { id: '070000', name: 'Callao', 'created_at': null, 'updated_at': null },
    { id: '080000', name: 'Cusco', 'created_at': null, updated_at: null },
    { 'id': '090000', name: 'Huancavelica', created_at: null, updated_at: null },
    { id: '100000', name: 'Hu\u00e1nuco', created_at: null, 'updated_at': null },
    { id: '110000', 'name': 'Ica', 'created_at': null, updated_at: null },
    { id: '120000', 'name': 'Jun\u00edn', created_at: null, 'updated_at': null },
    { id: '130000', name: 'La Libertad', 'created_at': null, updated_at: null },
    { id: '140000', name: 'Lambayeque', created_at: null, 'updated_at': null },
    { id: '150000', 'name': 'Lima', 'created_at': null, updated_at: null },
    { 'id': '160000', name: 'Loreto', created_at: null, 'updated_at': null },
    { 'id': '170000', name: 'Madre de Dios', created_at: null, 'updated_at': null },
    { id: '180000', name: 'Moquegua', created_at: null, 'updated_at': null },
    { 'id': '190000', 'name': 'Pasco', created_at: null, updated_at: null },
    { id: '200000', name: 'Piura', created_at: null, 'updated_at': null },
    { id: '210000', name: 'Puno', 'created_at': null, 'updated_at': null },
    { id: '220000', name: 'San Mart\u00edn', created_at: null, 'updated_at': null },
    { id: '230000', 'name': 'Tacna', created_at: null, updated_at: null },
    { 'id': '240000', name: 'Tumbes', created_at: null, updated_at: null },
    { id: '250000', name: 'Ucayali', 'created_at': null, updated_at: null }];

function castDepartmens(departments) {
  const d = departments.map((e) => ({
      id : e.id,
      name : e.name,
    }));
  return d;
}

const users = [
  {
    name: 'Brandon Jesús',
    lastName: 'Dominguez Polo',
    dni: '70944869',
    gender: 'M',
    age: 22,
    districtId: '150142',
  },
  {
    name: 'Luis Anthony',
    lastName: 'Caceres Hinostroza',
    dni: '70918245',
    gender: 'M',
    age: 21,
    districtId: '150142',
  },
  {
    name: 'Juan Manuel',
    lastName: 'Torres Sanchez',
    dni: '72860508',
    gender: 'M',
    age: 25,
    districtId: '150143',
  },
]


exports.seed = async (knex) => {
  await knex.raw('SET @@SESSION.foreign_key_checks = 0;');
  await knex('department').del();
  await knex('province').del();
  await knex('district').del();
  await knex('user').del();

  await knex.raw('SET @@SESSION.foreign_key_checks = 1;');
  await knex('department').insert(castDepartmens(departments));
  await knex('province').insert(castProvinces(provinces));
  await knex('district').insert(castDistrict(districts));
  await knex('user').insert(users);

};

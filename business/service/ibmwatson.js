
const AssistantV1 = require('ibm-watson/assistant/v1');
const { IamAuthenticator } = require('ibm-watson/auth');


const assistant = new AssistantV1({
    authenticator: new IamAuthenticator({ apikey: '9t5n87Gx4AVp7v8JplL-heBnLoGDN93gQCUS_CY6tX7v' }),
    url: 'https://api.us-east.assistant.watson.cloud.ibm.com',
    version: '2018-02-16'
  });


async function sendToIbmWatson (messageInput) {
    try {
        const data = await assistant.message({
            input: { text: `${messageInput}` },
            workspaceId: 'f59be4bd-5a3b-4826-9c1d-26d13d17b695',
          })

          return data;
    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    sendToIbmWatson,
};
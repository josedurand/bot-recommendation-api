const axios = require('axios');
const RENIEC_URI = `${process.env.RENIEC_URI}`;
const RENIEC_TOKEN = `${process.env.RENIEC_TOKEN}`;


async function validateToReniec(dni) {
    try {
        return await axios.get(`${RENIEC_URI}/${dni}`, {
            params: {
              token: RENIEC_TOKEN
            }
          });
    } catch(error) {
        return error;
    }
    
}


module.exports = {
    validateToReniec,
};
const options = require('../../knexfile');
const knex = require('knex')(options);
const { handleError } = require('../../utils/helpers/expressHelper');
const recommendationEntry = require('../entry/recommendation');
const axios = require('axios');
const RENIEC_URI = `${process.env.RENIEC_URI}`;
const RENIEC_TOKEN = `${process.env.RENIEC_TOKEN}`;

/*async function all(page) {
    var configuration = await knex('configuration').select('*').paginate(10, page, true);
    return configuration;
}

} */

async function validateToReniec(dni) {
    try {
        return await axios.get(`${RENIEC_URI}/${dni}`, {
            params: {
              token: RENIEC_TOKEN
            }
          });
    } catch(error) {
        return error;
    }
    
}

async function create(req, res) {
    const data = req.body;
    const validate = recommendationEntry.validateCreate(data);
    const matched = await validate.check();

    const { name, lastname, dni, gender, age, districtId } = req.body;

    if (!matched) {
        return res.status(422).json(validate.errors);
    }


    const result = validateToReniec(dni);


    const dni = req.body.dni;

    try {
        await knex('user').insert(req.body);
        return res.json({ message: 'Recommendation was created successfully!'});
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

async function update(req, res) {
    const { id } = req.params;
    const { type, description } = req.body;
    const data =  { type, description, id }
    const validate = recommendationEntry.validateUpdate(data);
    const matched = await validate.check();

    if (!matched) {
        return res.status(422).json(validate.errors);
    }
    try {
        const recommendation = await knex('recommendation').where(id);

        if (!recommendation) {
            return res.status(404).message({ message: 'This recommendation doesnt exists' });
        }

        await knex('recommendation')
            .update(req.body)
            .where(id);

        return res.json({ message: 'Recommendation was updated successfully!'});
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

async function destroy(req, res) {
    const { id } = req.params;
    const data = { id }
    const validate = recommendationEntry.validateDelete(data);
    const matched = await validate.check();

    if (!matched) {
        return res.status(422).json(validate.errors);
    }

    try {
        const recommendation = await knex('recommendation').where(id);

        if (!recommendation) {
            return res.status(404).message({ message: 'This recommendation doesnt exists' });
        }

        await knex('recommendation')
            .delete()
            .where(id);

        return res.json({ message: 'Recommendation was updated successfully!'});
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

async function getByType(req, res) {
    const data = req.body;
    const validate = recommendationEntry.validateGetByType(data);
    const matched = await validate.check();

    if (!matched) {
        return res.status(422).json(validate.errors);
    }
    
    try {
        const recommendations = await knex('recommendation').where('type', data.type);
        const recommendation = recommendations[Math.floor(Math.random() * recommendations.length)];
        return res.json(recommendation);
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

async function get(req, res) {
    try {
        const recommendations = await knex('recommendation');
        return res.json(recommendations);
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}


module.exports = {
    validateToReniec,
};
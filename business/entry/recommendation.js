const { Validator } = require('node-input-validator');

const validateCreate = (data) => {
    return new Validator(data, {
        type: `required|in:${process.env.VALID_RECOMMNEDATION_TYPE}`,
        description: 'required|maxLength:100'
    });
}

const validateUpdate = (data) => {
    return new Validator(data, {
        id: 'integer',
        type: `required|in:${process.env.VALID_RECOMMNEDATION_TYPE}`,
        description: 'required|maxLength:100'
    });
}

const validateGetByType = (data) => {
    return new Validator(data, {
        type: `required|in:${process.env.VALID_RECOMMNEDATION_TYPE}`,
    });
}


const validateDelete = (data) => {
    return new Validator(data, {
        id: 'integer',
    });
}

module.exports = {
    validateCreate,
    validateGetByType,
    validateUpdate,
    validateDelete
};

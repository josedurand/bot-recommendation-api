const options = require('../../knexfile');
const knex = require('knex')(options);    

async function all(page) {
    var configuration = await knex('configuration').select('*').paginate(10, page, true);
    return configuration;
}

async function create(data) {
    var configuration = await knex('configuration').insert(data);
    return configuration;
}

async function get(id) {
    var configuration = await knex('configuration').where(id);
    return configuration;
}

async function update(id,data) {
    var configuration = await knex('configuration').where(id).update(data);
    return configuration;
}

async function destroy(id) {
    var configuration = await knex('configuration').where(id).delete();
    return configuration;
}

module.exports = {
    all,
    create,
    get,
    update,
    destroy
};
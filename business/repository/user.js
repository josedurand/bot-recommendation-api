const options = require('../../knexfile');
const knex = require('knex')(options);
const { handleError } = require('../../utils/helpers/expressHelper');
const userService = require('../service/user');

async function create(req, res) {
    const { dni, name, lastName } = req.body;

    try {
        const reniecResult = await userService.validateToReniec(dni);
        if (reniecResult.status !== 200) {
            return res.status(400).json({ message: 'El dni proporcionado no pertenece a ninguna persona registrada.'});
        }

        const reniecFullName = `${reniecResult.data.nombres.trim()}${reniecResult.data.apellidoPaterno.trim()}${reniecResult.data.apellidoMaterno.trim()}`.replace(/ /g,'');
        const requestFullName = `${normalize(name).trim()}${normalize(lastName).trim()}`.replace(/ /g,'');

        console.log(requestFullName, reniecFullName)

        if (requestFullName.toUpperCase().trim() !== reniecFullName.toUpperCase().trim()) {
            return res.status(400).json({ message: 'El dni proporcionado no coincide con el nombre y apellidos.'})
        }

        const dbUser = await knex('user').where('dni', dni);
        if (dbUser.length !== 0) {
            return res.status(400).json({ message: 'Este dni esta siendo usado por otro usuario' })
        }

        await knex('user').insert(req.body);
        return res.json({ message: 'Usuario creado exitosamente'});
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

async function get(req, res) {
    const { dni } = req.params;

    try {
        const user = await knex.first('*').from('user').where('dni', dni);
        if (user.length === 0) {
            return res.status(400).json({ message: 'El dni proporcionado no pertenece a ningún usuario.' })
        }
        return res.json(user);
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}



async function update(req, res) {
    const { dni } = req.params;
    const { age, gender, districtId } = req.body;
    try {

        const user = await knex('user').where('dni', dni);

        if (user.length === 0) {
            return res.status(400).json({ message: 'El dni proporcionado no pertenece a ningún usuario.' })
        }

        await knex('user')
            .update({
                age,
                gender,
                districtId,
            })
            .where('dni', dni);

        return res.json({ message: 'Usuario actualizado correctamente'});
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

const normalize = (function() {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
        to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
        mapping = {};
   
    for(var i = 0, j = from.length; i < j; i++ )
        mapping[ from.charAt( i ) ] = to.charAt( i );
   
    return function( str ) {
        var ret = [];
        for( var i = 0, j = str.length; i < j; i++ ) {
            var c = str.charAt( i );
            if( mapping.hasOwnProperty( str.charAt( i ) ) )
                ret.push( mapping[ c ] );
            else
                ret.push( c );
        }      
        return ret.join( '' );
    }
   
})();

module.exports = {
    create,
    update,
    get,
};
const options = require('../../knexfile');
const knex = require('knex')(options);
const { handleError } = require('../../utils/helpers/expressHelper');
const userService = require('../service/user');
const jwt = require('../../middlewares/auth/jwt');


async function login(req, res) {
    const { fullName, firstLastName, secondLastName, dni } = req.body;
    try {
        const reniecResult = await userService.validateToReniec(dni);

        if (reniecResult.status !== 200) {
            return res.status(400).json({ message: 'El dni proporcionado no pertenece a ninguna persona registrada.'});
        }

        const reniecFullName = `${reniecResult.data.nombres.trim()}${reniecResult.data.apellidoPaterno.trim()}${reniecResult.data.apellidoMaterno.trim()}`;
        const requestFullName = `${normalize(fullName).trim()}${normalize(firstLastName).trim()}${normalize(secondLastName).trim()}`;

        console.log(reniecFullName, reniecFullName)

        if (requestFullName.toUpperCase().trim() !== reniecFullName.toUpperCase().trim()) {
            return res.status(400).json({ message: 'El dni proporcionado no coincide con el nombre y apellidos.'})
        }


        const dbUser = await knex('user').select('id', 'name', 'lastName', 'gender', 'age', 'dni').where('dni', dni);

        if(dbUser.length === 0) {
            return res.status(400).json({ message: 'El usuario no se encuentra registrado.'})
        }

        var token = jwt.createToken(dbUser[0]);
        return res.json({ message: 'Usuario logueado', user: dbUser[0], token: token });

    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

async function getDepartment(req, res) {
    try {
        const department = await knex('department');
        return res.json(department);
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

async function getProvince(req, res) {
    const { departmentId } = req.params;
    try {
        const provinces = await knex('province').where('departmentId', departmentId);
        return res.json(provinces);
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

async function getDistrict(req, res) {
    const { provinceId } = req.params;
    try {
        const provinces = await knex('district').where('provinceId', provinceId);
        return res.json(provinces);
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}


const normalize = (function() {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
        to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
        mapping = {};
   
    for(var i = 0, j = from.length; i < j; i++ )
        mapping[ from.charAt( i ) ] = to.charAt( i );
   
    return function( str ) {
        var ret = [];
        for( var i = 0, j = str.length; i < j; i++ ) {
            var c = str.charAt( i );
            if( mapping.hasOwnProperty( str.charAt( i ) ) )
                ret.push( mapping[ c ] );
            else
                ret.push( c );
        }      
        return ret.join( '' );
    }
   
})();


module.exports = {
    login,
    getDepartment,
    getProvince,
    getDistrict,
};
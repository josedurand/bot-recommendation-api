const options = require('../../knexfile');
const knex = require('knex')(options);
const { handleError } = require('../../utils/helpers/expressHelper');
const moment = require('moment')

async function create(req, res) {
    const { userId, content } = req.body;

    try {
        const user = await knex.first('*').from('user').where('id', userId);
        if (user.length === 0) {
            return res.status(400).json({ message: 'El dni proporcionado no pertenece a ningún usuario.' })
        }

        await knex('surveyResult').insert({
            userId,
            content,
            proccessDate: moment().format('YYYY-MM-DD hh:mm:ss')
        });

        return res.json({ message: 'Resultado de encuesta guradado correctamente.' });
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}

async function get(req, res) {
    const { userId } = req.params;

    try {
        const user = await knex.first('*').from('user').where('id', userId);
        if (!user) {
            return res.status(400).json({ message: 'El dni proporcionado no pertenece a ningún usuario.' })
        }
        let results = await knex('surveyResult')
            .where('userId', userId)
            .orderBy('proccessDate', 'DESC')
            .limit(10)

            console.log(results)

        results = results.map(result => {
            return {
                id: result.id,
                userId: result.userId,
                content: result.content,
                proccessDate: moment.utc(result.proccessDate).local().format('YYYY-MM-DD hh:mm:ss'),
            }
        })
        return res.json(results);
    } catch (error) {
        const errorMessage = handleError(error);
        return res.status(500).json({ message: 'Error in the request', error: errorMessage.error });
    }
}


module.exports = {
    create,
    get,
};